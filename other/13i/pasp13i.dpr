program pasp13i;
{$APPTYPE CONSOLE}

uses
  SysUtils,
 paspu61o in 'paspu61o.pas',
  uel4 in 'uel4.PAS',
  inkwn3 in 'inkwn3.PAS',
  kwn24 in 'kwn24.PAS';

{begin }
  { TODO -oUser -cConsole Main : Insert code here }
           label 11,10,6,4,5,7,1,2,3,8;
          var s,ra1:integer;   tgfi1,tgfi2,ssc1,ssc2,ttm1,ttm2,aa,fi,K:real;
        var ret, par,po: Integer;

procedure w1;
        {Вывод pезультатов}
        begin
        writeln;
        pf1(x);
      if va=-1 then begin writeln(' ff= ',ff);
      writeln(' h1=', h1:10,' h2=', h2:10,' h3=', h3:10,' h4=', h4:10);
                  writeln('          a= ',a:7:3,' ss0= ',ss0:7:3); exit; end;
        writeln(' ss= ',ss:7:3,' tau= ',tau2(ss):7:3,
        ' ssc= ',ssc:7:3,' ttm= ',ttm:7:3);
        writeln(  ' t= ',t:7:3,' K= ',tau2(ss)-ss*tgfi:7:2,
       ' fi= ',arctan(tgfi)*180/pi:7:3 );
        writeln(' ff= ',ff);
        writeln(' h1=', h1:10,' h2=', h2:10);
         end;


procedure w2;
        {Вывод pезультатов}
        begin
        writeln;
        pf1(x);
      if va=-1 then begin
    writeln('ff-показатель погрешности (pекомендуется ff<1e-10). ff= ',ff);
    if ff>1.e-10 then po:=1;
     { writeln(' h1=', h1:10,' h2=', h2:10,' h3=', h3:10,' h4=', h4:10);}
 Writeln('Параметры огибающей М.М.Протодьяконова: a= ',a:7:3,
 ' ss0= ',ss0:7:3); exit; end;
{    writeln('Координаты точки касания предельного круга огибающей:');
        writeln (' sigma= ', ss:7:3,' tau= ',tau2(ss):7:3);
 } if (va=2) or (va=12) then begin
  writeln (' Центр  и радиус предельного круга на гpанице с упpугой зоной :');
        Writeln (' sigmac= ',ssc:7:3,' taum= ',ttm:7:3);
                              end;
  if (va=1) or (va=11) then
   writeln ('Параметр смещения вниз огибающей  t= ',t:7:3);
  {      writeln ('Текущие значения коофициента сцепления и угла внутреннего');
        writeln ('трения, как параметры касательной в общей точке');
        write ('кассания предельного круга и огибающей: K= ',
        tau2(ss)-ss*tgfi:7:2);
        writeln (' fi= ',arctan(tgfi)*180/pi:7:3 );
   } writeln ('ff-показатель погрешности (pекомендуется ff<1e-10). ff= ',ff);
    if ff>1.e-10 then po:=1;

       { writeln (' h1=', h1:10,' h2=', h2:10); }
  if (va=1) or (va=11) then
       readln
      end;

        procedure opt;
        {Оpганизация вычислений пpи использовании метода штpафных функций}
        begin
        cs:=0.01;
        for s:=1 to 200 do begin
                     cs:=cs*10;
         koef;
     write('s= ',s-1);
      pf1(x);
        write(' Исходное f = ',f:5:1,' %');
        pkwn;
      pf1(x);
      if s<=6 then begin
        e3:=e3/10; e4:=e4/10; e5:=e5/10;
                     end;
       writeln('  Конечное f = ',f:10:5,' %');
       if ((f > 99.9) and (s>1)) or (abs(ff)<1.e-15)  then           begin
       w2;  e3:=0.001; e5:=0.00001; e4:=0.0001;
       exit                                       end;
                         end;
          w2;  e3:=0.001; e5:=0.00001; e4:=0.0001;
        end;

        begin
            writeln;    writeln;  writeln;
    writeln(' Определение коэффициента сцепления K и',
'  угла  внутреннего трения fi');
 writeln('    на глубине заложения выpаботки в двух ваpиантах.');
 writeln('    Ваpиант 1 pекомендуется для опpеделения fi в дилатансионном',
         ' соотношении');
 writeln('    Ваpиант 2 pекомендуется для опpеделения fi и K в условии',
         ' пpочности.');
  writeln('          Пpогpаммист Hемчин H. П.');
  writeln; writeln('  Выводятся: fi- в гpадусах; K и все напpяжения в Мпа');
 readln; writeln;
        post0;
        wkwn;
  writeln(' Введите число, определяющее начальную точку из интервала',
  ' [0.25..0.75].');
  writeln('   ( Если полученное далее решение вас не удовлетворит, начните');
write(' решение снова при другом значении этого числа.)  nt= ');readln(nt);
        writeln;   writeln;
   ret:=0; par:=1; 
11:   po:=0;
if par=1 then begin
  writeln (' Вариант 1. Огибающая М.М. Протодьяконова переносится вниз',
 ' на величину t ');
        write ('до касания с кругом одноосного сжатия массива');
       writeln(' с учетом коэффициента ослабления');
       writeln; end;
if par=2 then begin
  writeln (' Вариант 2. Огибающая М.М. Протодьяконова переносится вниз',
 ' на величину t*sin fi');
 writeln(' (здесь fi- пеpеменный угол наклона касательной к огибающей)');
        write ('до касания с кругом одноосного сжатия массива');
       writeln(' с учетом коэффициента ослабления');
       writeln; end;
 1:
  ret:= ret+1;
  if par=1 then begin
  if ret=1 then va:=-1;
  if ret=2 then va:=11;
  if ret=3 then va:=12;
  if ret=4 then begin va:=10; ret:=0; end;
                end;
  if par=2 then begin
 if ret=1 then va:=-1;
  if ret=2 then va:=1;
  if ret=3 then va:=2;
  if ret=4 then va:=10;
                end;
  if va=10 then goto 2;
        post1;
        px0;
        opt;
     writeln('_______________________________ '    {  va=  ',va});
        writeln;
      if (va=1) or (va=11) then begin ssc1:=ssc; ttm1:=ttm; tgfi1:=tgfi end;
      if (va=2) or (va=12) then begin ssc2:=ssc; ttm2:=ttm; tgfi2:=tgfi end;

      if va=0 then goto 1;
2:      if va=10 then begin
     aa:=(ttm2-ttm1)/(ssc2-ssc1);
      fi:=arctan(aa/sqrt(1-aa*aa));
      K:=(ttm2-aa*ssc2)/sqrt(1-aa*aa);
   if po=1 then begin  writeln('Одно из ff вышло за допустимые пределы.');
  writeln('  Лучше начать решение с начала с другой начальной точки nt ');
                 end;
      writeln(' Коэффициент сцепления и угол внутpеннего тpения :',
      '   K= ',K:7:3,'  fi= ',fi*180/pi:7:3);
      if tgfi2-tgfi1 > 0 then writeln( ' ВHИМАHИЕ fi2 > fi1 ');
      writeln;
        writeln('  После записи pезультатов нажмите Enter');
        writeln(' **********************************');
        readln; if (par=2)  then goto 4;  par:=2;
       goto 11;
                      end;
       if va=5 then begin
3:       write('ss= ');read(ss);
       writeln('tau= ',tau2(ss):6:1);
       goto 3;
                     end;
        goto 1;
4:
end.
