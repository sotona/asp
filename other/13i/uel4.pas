        unit uel4;
        {Возведение в степень; а с точностью до 1.e-8}
        interface
        function el(a:real;n:real):real;
        implementation
        function el(a:real;n:real):real;
        label 1;
        begin
     if (n<0) and (a<1.e-9) and (a>-1.e-35) then begin a:=a+1.e-8; goto 1 end;
     if (a<1.e-16) and (a>-1.e-35) and (n>1/2) then begin el:=0.; goto 1 ;end;
        if (abs(a)<1.e-35) and (n>=0) then begin el:=0.;goto 1; end;
         el:=exp(n*ln(a));
         1:end;
 {Число возводимое в степень не должно быть меньше 0 }
end.