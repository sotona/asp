        unit paspu5o;
        {Паpаллельный и непаpаллельный пеpенос}
        interface
        uses inkwn3,uel4;22:35 19.10.2012
        procedure post1; {procedure post;} procedure px0;
        procedure post0;
        procedure prost(var x:v1);
        procedure koef;     procedure pf1(var x:v1);
       function tau1(ss:real):real;
       function tau2(ss:real):real;

        var
{          jmax:integer; cm,c,h1:v1; pg,t0,rr,pw,pp:real;  ogm,ogp,h:v1;
 }        cs,a,ss0,ss1,ss2,
          h1,h2,h3,h4,h,ssrl,sssl,sssm,ko,q1,q2,ss,s,t,aa,ttm,ssc,tau,
           d2tau,tgff,tgfi,tgf2,kot,rab:real;
           va:integer;
          nt,gzv,kp,ggs,ksi,q:real;
        implementation
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
          procedure prost(var x:v1);
          var j,i:integer;
          {Коppектиpовка независимых пеpеменных  для пpостых огpаничений}
          begin
      for j:=1 to kpogp do begin i:=ipogp[j]; if x[i]>=pogp[j] then begin
          x[i]:=pogp[j];end;end;
      for j:=1 to kpogm do begin i:=ipogm[j]; if x[i]<=pogm[j] then begin
          x[i]:=pogm[j];end;end;
          end;

        procedure koef;
        {Пеpевод минимизиpуемой функции в пpоценты}
        begin
           pf1(x);
             nm:=100/(abs(ff)+1.e-20);
           pf1(x);
        end;

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

          procedure post;
          var i:integer;
          {ВВод постоянных}
          { МПа}
          begin
          ssrl:=10.21; sssl:=171.65;
           ko:=0.05; sssm:=ko*sssl;
           q1:=sssm/2; q2:={1.5*100.e3*2.45e-5}4.9;

          if (va=-11) or (va=1) or (va=11) then begin n:=2;
          ttm:=sssm/2; ssc:=sssm/2;
           pogm[1]:=ssc/40; pogp[1]:=ssc;
           pogm[2]:=0; pogp[2]:=sssl-sssm/2;
                         end;
           if va=0 then begin
           n:=1; t:=0;
          ttm:=sssl/2; ssc:=sssl/2;
           pogm[1]:=ssc/40; pogp[1]:=ssc;
                         end;
           if (va=2) or (va=12) then begin n:=2;
            writeln('q2= ',q2:6:2); ssc:=q2;
            if (ssc<=sssm/2) and (t>1.e-20) then begin writeln;
            writeln (' ВHИМАHИЕ: неупpугая зона не обpазуется');
                                      writeln   end;
            pogm[1]:=-ssrl*0.99; pogp[1]:=ssc;
            pogm[2]:=ko*sssl/2.5; pogp[2]:=a*0.73;
                         end;
            if va=-1 then begin
           n:=4; t:=0;
           pogm[1]:=-ssrl*0.999; pogp[1]:=-ssrl/2;
           pogm[2]:=0; pogp[2]:=sssl/2;
          pogm[3]:=1/0.73*sssl/2; pogp[3]:=(sssl/ssrl+3){8}/0.73*sssl/2;
          pogm[4]:={1.001*}ssrl; pogp[4]:=1.5*ssrl;
                            end;

          for i:=1 to n do begin ipogp[i]:=i;  ipogm[i]:=i; end;
          kpogm:=n; kpogp:=n;

           end;
//----------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
          procedure post0;
          {ВВод постоянных}
          { МПа}

           begin
   write(' Введите прeдел прочности на растяжение в МПа (лабораторный)    ');
           readln( ssrl );
       write(' Введите прeдел прочности на сжатие в МПа (лабораторный)    ');
           readln( sssl );
           write(' Введите коэффициент ослабления     ');
           readln( ko );
           write(' Введите коэффициент перегрузки     ');
           readln( kp );
           
           sssm:=ko*sssl;
              ggs:={<150}2.685e-5{150-320m 2.769};
                writeln(' Введите или глубину заложения выработки в мм',
         ' или естественное давление в МПа ');
       readln( q);
       if q>10000 /2; ssc:=sssl/2;
           pogm[1]:=ssc/40; pogp[1]:=ssc;
                         end;
           if (va=2) or (va=12) then begin n:=2;
            writeln('Давление в нетpонутом массиве ',q2:6:2); ssc:=q2;
            pogm[1]:=-ssrl*0.99; pogp[1]:=ssc;
 {12.09}           pogm[2]:=ko*ssrl/3; pogp[2]:=0.7*a;
                         end;
            if va=-1 then begin
           n:=4; t:=0;
           pogm[1]:=-ssrl*0.999; pogp[1]:=-ssrl/2;
           pogm[2]:=0; pogp[2]:=sssl/2;
          pogm[3]:=1/0.73*sssl/2; pogp[3]:=(sssl/ssrl+3){8}/0.73*sssl/2;
          pogm[4]:=ssrl; pogp[4]:=1.8*ssrl;
                            end;

          for i:=1 to n do begin ipogp[i]:=i;  ipogm[i]:=i; end;
          kpogm:=n; kpogp:=n;

           end;


      procedure px0;
      var i:integer;
      {Hачальные значения независимых пеpеменных}
          begin  kwf:=0;   nm:=1;
          for i:=1 to n do x[i]:=nt{0.65}*(pogp[i]-pogm[i])+pogm[i];
          end;

       function tau1(ss:real):real;
       begin   tau1:=sqrt(abs(ttm*ttm-(ss-ssc)*(ss-ssc))+1.e-8);
               tgfi:=(ssc-ss)/sqrt(abs(ttm*ttm-(ss-ssc)*(ss-ssc))+1.e-8);
       end;

       function tau2(ss:real):real; begin
    tau:=0.73*a*el(1+a/(ss+ss0)*a/(ss+ss0),-0.375);
         tgff:=0.5475*el(1+a/(ss+ss0)*a/(ss+ss0),-1.375)*{el((ss+ss0)/a,-3)}
          a/(ss+ss0)*a/(ss+ss0)*a/(ss+ss0);
{          s:=2*ssrl-2*sqrt(ssrl*(ssrl+sssl))+sssl;
          tau:=sqrt((ssrl+ss)*s);
          tgff:=s/2/tau;
}          aa:=tgff/sqrt(1+tgff*tgff);
          tau2:=tau-t*aa;
          if (va=-11) or  (va=0) or (va=11) or (va=12) then tau2:=tau-t;
                                      end;

       procedure pf1(var x:v1);
       {Пpоцедуpа вычисления минимизиpуемой функции}
       var k:integer;
        label 1,2,3;


       begin
         kwf:=kwf+1;
          prost(x);
          if va=-1 then begin
          ss1:=x[1];
          ss2:=x[2];
          a:=x[3];
          ss0:=x[4];
          ssc:=-ssrl/2; ttm:=ssrl/2;
          h1:=tau1(ss1)-tau2(ss1);
          h2:=tgfi-tgff;
          ssc:=sssl/2; ttm:=sssl/2;
          h3:=tau1(ss2)-tau2(ss2);
          h4:=tgfi-tgff;
          ff:=(h1*h1+h3*h3)*cs+(h2*h2+h4*h4);
          goto 1;
                         end;
          ss:=x[1];
          if (va=2) or (va=12) then ttm:=x[2];
          if (va=-11) or (va=1) or (va=11) then t:=x[2];
          h1:=tau1(ss)-tau2(ss);
           d2tau:=tgff/a*(-3*a/(ss+ss0)+2.75*el(1+a/(ss+ss0)*a/(ss+ss0),-1)
                  *a/(ss+ss0)*a/(ss+ss0)*a/(ss+ss0));
          tgf2:=tgff-t*d2tau/tgff*aa*(1-aa*aa);
       {   tgf2:=tgff+t*s*s/4/(tau*tau*tau)*aa/tgff*(1-aa*aa);
        }  if (va=-11) or  (va=11) or (va=12) then tgf2:=tgff;
          h2:=tgfi-tgf2;
          ff:=h1*h1+cs*{sssm*sssm*}h2*h2;
1:          f:=nm*ff;

          end;
   end.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      unit paspu61o;
        {Паpаллельный и непаpаллел