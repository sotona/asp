#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from collections import namedtuple
from math import log10, sin

from scipy.optimize import minimize
from pylab import *


Shaft = namedtuple('Shaft', 'S Em Em_p Em_ra SScm mu mu_p Ra Rb n a k b bb gamma')
Detail = namedtuple('Detail', 'r Er SScm SSr SSt U EEr EEt drSSr drU TT EEt_k EEt_h') # Describes Strain-Stress State

eps = 0.000001  # epsilon for eet
table_head = "БВР,    mu,        Ua,        SSR,     SST,     EET_k,     EET_h, закон Гука,   E_s,   E_hs"
print_mask = ", %8.4f, %8.4f, %8.4f, %8.6f, %8.6f, %s, %8.2f, %12.11f"
print_mask2 = ", U= %8.4f, SSR= %8.4f, SST= %8.4f, EET_k=%8.6f, EET_h=%8.6f, %s, E_s=%8.2f, E_h=%12.11f"

# Sb!=0
def funcIV_1D(x, sh, N, fdm):
    x[1] = 0
    d = fdm(x, sh, N)
    return (d.SSr[-1] + sh.S)**2

# Sb=0, Sa!=0
def funcII_1D_ssr0(x, sh, N, fdm):
    x[1] = sh.S
    d = fdm(x, sh, N)
    return (d.SSr[-1])**2

# X = [U0, SSr0]
def func_2d_lin(x, sh, N, fdm):
    S = sh.S
    d = fdm(x, sh, N)
    A = d.SSr[-1] + S
    B = d.SSt[0] - sh.bb * d.SSr[0] + d.SScm[0]
    return A**2 + B**2


# Квадратичная функция прочности
def func_2d_quad(x, sh, N, fdm):
    S = sh.h
    d = fdm(x, sh, N)
    A = d.SSr[-1] + S
    B = d.SSt[0] + d.SScm[0] - c1*d.SSr[0] - c2*d.SSr[0]**2
    return A**2 + B**2


# Упругая зона, явный МКР
def eFDM(x, sh, N):
    u0,ssr0 = x
    d = Detail([sh.Ra]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,1,1)
    dr = (sh.Rb-sh.Ra)/N
    S = sh.S
    d.U[0] = u0
    d.SSr[0] = ssr0
    d.r[N-1]=sh.Rb
    for i in range(0,N-1):
         d.r[i] = sh.Ra + dr*i
         d.Er[i] = ERA(sh.Em_p,sh.a,sh.n,d.r[i]/sh.Ra)
         d.EEt[i] =  d.U[i] / d.r[i]
         d.SSt[i] = d.EEt[i] * d.Er[i] - S + sh.mu_p * ( d.SSr[i] + S )
         d.EEr[i] = ( d.SSr[i] + S - sh.mu_p * ( d.SSt[i] + S ) ) / d.Er[i]
         d.drU[i] = d.EEr[i]
         d.U[i + 1] = d.U[i] + dr * d.drU[i]
         d.drSSr[i] = - ( d.SSr[i] - d.SSt[i] ) / d.r[i]
         d.SSr[i + 1] = d.SSr[i] + dr * d.drSSr[i]
         d.TT[i] = d.EEr[i] + d.EEt[i]
         d.SScm[i] = sh.SScm*(1.0 - sh.b * (d.r[i]/sh.Ra)**(-sh.k))
    return d

# Упругая зона, явный МКР, S=0
def eFDM_S0(x, sh, N):
    u0,ssr0 = x
    d = Detail([sh.Ra]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,1,1)
    dr = (sh.Rb-sh.Ra)/N
    d.U[0] = u0
    d.SSr[0] = ssr0
    S = 0
    for i in range(0,N-1):
         d.r[i] = sh.Ra + dr*i
         d.Er[i] = ERA(sh.Em_p,sh.a,sh.n,d.r[i]/sh.Ra)
         d.EEt[i] =  d.U[i] / d.r[i]
         d.SSt[i] = d.EEt[i] * d.Er[i] - S + sh.mu_p * ( d.SSr[i] + S )
         d.EEr[i] = ( d.SSr[i] + S - sh.mu_p * ( d.SSt[i] + S ) ) / d.Er[i]
         d.drU[i] = d.EEr[i]
         d.U[i + 1] = d.U[i] + dr * d.drU[i]
         d.drSSr[i] = ( - ( d.SSr[i] - d.SSt[i] ) ) / d.r[i]
         d.SSr[i + 1] = d.SSr[i] + dr * d.drSSr[i]
         d.TT[i] = d.EEr[i] + d.EEt[i]
    return d


def pFDM(sh, D, Rz, N, omega):
    """
    Дополняет массивы D данными из запредельной зоны,
    :param sh: Shaft
    :param D: elastic Details (arrays)
    :param Rz: radius of plastic region
    :param omega: угол [Rad]
    :return: D with plastic data
    """
    # D = Detail([sh.Ra]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,1,1)
    Iz = int(N*(Rz-sh.Ra)/sh.Rb)  # первая точка упругой области
    Rz = sh.Ra + (sh.Rb-sh.Ra)*N/Iz
    dr = (Rz-sh.Ra)/Iz

    thetta_l = 0.12
    thetta_z = D.EEr[Iz]+D.EEt[Iz]


    for i in range(Iz-1,0,-1):
        D.drSSr[i] = - (D.SSr[i]-D.SSt[i])/D.r[i] + sh.gamma*sin(omega);
        D.U[i-1]   = D.U[i]   - dr*D.drU[i]
        D.SSr[i-1] = D.SSr[i] - dr*D.drSSr[i]
        if (D.TT[i] >= thetta_l - 1e-10): D.drU[i-1] = -D.u[i-1]/D.r[i-1] + thetta_l;
        else: D.drU[i-1] = -D.U[i-1]/D.r[i-1] + thetta_z + \
                                    (1-m.c[1]-2*m.c[2]*D.SSr[i-1])*(D.U[i-1]/D.r[i-1]-eetz);
        D.EEr[i-1] = D.drU[i-1];
        D.EEt[i-1] =  D.U[i-1] / D.r[i-1];

        D.TT[i-1] = D.drU[i-1] + D.U[i-1]/D.r[i-1];
        if (D.TT[i-1]>thetta_l):
            D.TT[i-1] = thetta_l;
            if (rk):
                sh.rk=D.r[i-1]
                rk=0
        D.SSo[i-1] = m.SSm - m.T*(D.TT[i-1]-thetta_z)
        if D.SSo[i-1] < 0:
            D.SSo[i-1] = 0
            if rp:
                sh.rp = D.r[i-1]
                rp=0
        D.SSt[i-1]= - D.SSo[i-1] + m.c[1]*D.SSr[i-1]+m.c[2]*D.SSr[i-1]*D.SSr[i-1];



def bur(br, ssl, ra):
    M__ = 0.0
    m_ = 0.0
    if (ssl > 20.) and (ssl <= 40.): M__ = 1.8;  m_ = 0.85;
    elif (ssl > 40.) and (ssl <= 60.): M__ = 1.3;  m_ = 0.75;
    elif (ssl > 60.) and (ssl <= 80.): M__ = 1.0;  m_ = 0.70;
    elif (ssl > 80): M__ = 0.9;   m_ = 0.60;
    n = k = 1./log10( 1. + M__ / (ra / 1000.0 * (br/1000.0)**m_))
    b = 0.9
    a = 0.98**n
    return n,a,k,b


def U_I(S,E, mm, r):
    return -S/E * (1 - mm)*r


def U_bak(q, p, E, n, a, r):
    return 3 / 2 * (q - p) / E * (n + 2) / (n + 2 - 2 * a) / r


def SStII_bak(q, p, n, a, r):
    return -(p - q) * (n + 2 - 2 * a *(n+1)* r ** (-n)) / (n + 2 - 2 * a) / r ** 2

def SSrII_bak(q, p, n, a, r):
    return (p - q) * (n + 2 - 2 * a * r ** (-n)) / (n + 2 - 2 * a) / r ** 2


def EET(E, sst, ssr, S, mu):
    return 1.0 / E * (sst + S - mu * (ssr + S))


def EER(E, sst, ssr, S, mu):
    return 1 / E * (ssr + S - mu * (sst + S))

def E_plain(Em, mu):
    return Em / (1.0 - mu * mu)

def mu_plain(mu):
    return mu/(1.0-mu)

def ERA(E,a,n,r):
    return E * (1- a*r**(-n))


def print_all(ssr, sst, ua):
    pass


def calc_hook(ssr, sst, ua, ra, E, mu):
    eet_k = au/ra
    eet_h = 1/E * (sst - mu*ssr)
    return eet_k, eet_h


# calc II, I+II, III
def calc_all_fdm(shoring):
    return detail


# calc II, I+II
def calc_all_bak(shoring):
    return detail


def compare(a,b,eps):
    if a <= b+eps and a >= b-eps:
        return True
    else: return False


def Note(sh):
    return ("    БВР" if sh.n+sh.a+sh.k+sh.b > eps else "без БВР")+",mu=%.3f"%sh.mu


def calc_I(sh):
    global eps
    u = U_I(sh.S, sh.Em_p, sh.mu_p, sh.Ra)
    ssr = -sh.S
    sst = -sh.S
    eet_k = u/sh.Ra
    eet_h = - sh.S / sh.Em_p * (1 - sh.mu_p)
    print (("        ,mm  =%5.3f"+print_mask)%(sh.mu,u,-sh.S, -sh.S, eet_k, eet_h, '+' if compare(eet_k, eet_h, eps) else '-', 0, sh.Em))
    return u,ssr,sst,eet_k,eet_h,compare(eet_k, eet_h, eps)


def calc_I_II(sh, tri):
    # global eps
    note = Note(sh)
    u,ssr,sst, eet_k, eet_h,b,result = tri
    S = sh.S
    ssr-=S
    sst-=S
    uI = U_I(sh.S, sh.Em_p, sh.mu_p, sh.Ra)
    u = u + uI
    eet_k = u/sh.Ra
    E = sh.Em_p if sh.a==0 else result.Er[0]
    eet_h = EET(sh.Em_p if sh.a==0 else sh.Em_ra,sst,ssr,S=0,mu=sh.mu_p)
    print(note + print_mask%(u,  ssr,  sst, eet_k, eet_h, '+' if compare(eet_k, eet_h, eps) else '-', result.Er[0], E))
    return u,ssr,sst,eet_k,eet_h,compare(eet_k, eet_h, eps)


def calc_prog(sh,N, fdm, func):
    global eps
    note = Note(sh)
    u0,ssr0 = 0, 0 if func==funcIV_1D else sh.S
    res = minimize(func, [u0, ssr0], args=(sh, N, fdm))
    if res.status != 0:
        print("Optimization faild")
    u0 = res.x[0]
    f = func([u0,ssr0],sh, N, fdm)
    result = fdm([u0,ssr0],sh, N)
    eet_k = u0/sh.Ra
    E = sh.Em_p if sh.a==0 else result.Er[0]
    S = 0 if fdm!=eFDM and func!=funcIV_1D else sh.S
    eet_h = EET(E, result.SSt[0],ssr0,S,mu=sh.mu_p)
    print (note + (print_mask+", F= %f")%(u0,ssr0, result.SSt[0],eet_k, eet_h, '+' if compare(eet_k, eet_h, eps) else '-', result.Er[0], E, f))
    return u0,ssr0,result.SSt[0],eet_k,eet_h,compare(eet_k, eet_h, eps), result


def calc_bak(sh, N, S):
    global eps
    note = Note(sh)
    u0 = U_bak(-sh.S, 0, sh.Em, sh.n, sh.a, r=1)*sh.Ra
    ssr0 = SSrII_bak(-sh.S, 0, sh.n, sh.a, r=1)
    sst0 = SStII_bak(-sh.S, 0, sh.n, sh.a, r=1)
    eet_k = u0/sh.Ra
    E = sh.Em_p if sh.a==0 else sh.Em_ra
    eet_h = EET(E,sst0,ssr0,S=0, mu=1)
    print (note + print_mask%(u0, ssr0+S, sst0+S, eet_k, eet_h, '+' if compare(eet_k, eet_h, eps) else '-', 0, E))
    return u0,ssr0+S,sst0+S, eet_k, eet_h, compare(eet_k, eet_h, eps)

