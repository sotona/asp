__author__ = 'sotona'
import numpy as np
ssc_m = 17.41
S = 8.64

ssc1 = -S
ttm1 = 9.246

ssc2 = - ssc_m / 2
ttm2 = ssc_m/2

ssc3 = (ssc1 + ssc2) / 2
ttm3 = (ttm1 + ttm2) / 2

sst1 = ssc1 - ttm1
sst2 = ssc2 - ttm2
sst3 = ssc3 - ttm3

ssr1 = ssc1 + ttm1
ssr2 = ssc2 + ttm2
ssr3 = ssc3 + ttm3

print("ssc 1,2,3: %6.3f \t %6.3f \t %6.3f" % (ssc1, ssc2, ssc3))
print("ttm 1,2,3: %6.3f \t %6.3f \t %6.3f" % (ttm1, ttm2, ttm3))

print("ssr 1,2,3: %6.3f \t %6.3f \t %6.3f" % (ssr1, ssr2, ssr3))
print("sst 1,2,3: %6.3f \t %6.3f \t %6.3f" % (sst1, sst2, sst3))

a = np.array([1, ssr1, ssr1 * ssr1],
             [1, ssr2, ssr2 * ssr2],
             [1, ssr3, ssr3 * ssr3])
b = np.array([])






