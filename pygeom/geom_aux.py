__author__ = 'sotona'
import configparser
from math import sin, pi
from subprocess import Popen, PIPE
import re

from geom import Shaft, bur, ERA


def load_params(filename):
    config = configparser.ConfigParser()
    if len(config.read(filename)) == 0: return False
    S = float(config["general_params"]["S"])
    kp = float(config["plastic"]["kp"])
    Elab = float(config["general_params"]["Elab"])
    ko = float(config["general_params"]["ko"])
    kd = float(config["plastic"]["kd"])
    ssl_c = float(config["general_params"]["ssl_c"])
    ff = float(config["plastic"]["ff0"])
    mm = float(config["general_params"]["mu"])
    br = float(config["plastic"]["Br"])
    ra = float(config["general_params"]["ra"])
    gg = float(config["plastic"]["gg"])
    # rb = float(config["general_params"]["rb"]) * ra
    rb = 20 * ra
    n, a, k, b = bur(br,ssl_c, ra)
    Em = Elab * 0.75 * (1. + ko) / 2.
    ssm_c = ssl_c * ko * kd
    ff = sin(ff / 180 * pi)
    bb = (1 + ff) / (1 - ff)
    Em_p = Em / (1.0 - mm * mm)
    Er = ERA(Em_p, a, n, r=1.0)
    sh = Shaft(S * kp, Em, Em_p, Er, ssm_c, mm, mm / (1.0 - mm), ra, rb, n, a, k, b, bb, gamma=gg)
    return sh


def get_ssc_ttm(ss_t_lab, ss_cmp_lab, ko, kp, S, prog="./pasp13i-p", verbose=False):
    sscttm_reg = re.compile("sigmac=.*taum=.*\n")
    params = [prog, str(ss_t_lab), str(ss_cmp_lab), str(ko), str(kp), str(S)]
    p = Popen(params, stdout=PIPE, stdin=PIPE, stderr=PIPE)
    p.stdin.write(b'\r')
    p.stdin.write(b'9\r')
    p.stdin.write(b'0.5\r')
    stdout_data, _ = p.communicate()
    if verbose:
        print(stdout_data.decode())
        print("-"*32)
    result = sscttm_reg.findall(stdout_data.decode())
    ssc =  float(result[0][result[0].find('= ')+1:result[0].find(' t')])
    ttm1 = float(result[0][result[0].find('m= ')+2:-1])
    ttm2 = float(result[1][result[0].find('m= ')+2:-1])
    return ssc, ttm1, ttm2


# sh = load_params("asp/bin/params/Квершлаг_№1_гор_750.ini")


