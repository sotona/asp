#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from geom import *
from geom_aux import load_params


shaft1 = "../asp/bin/params/2й_гор_800.ini"
shaft2 = "../asp/bin/params/Квершлаг_№1_гор_750.ini"

shaft = shaft1

U_row = "Перемещения Ua (мм)  :   %8.4f  %8.4f  %8.4f мм"
SSt_row = "Напряжения SSTa (МПа):   %8.4f  %8.4f  %8.4f КПа"
SSr_row = "Напряжения SSRa (МПа):   %8.4f  %8.4f  %8.4f КПа"


# 32000 - ok
N = 1000

sh = sh__m = sh_bm2 = sh__m2 = sh_b_ = sh___ = load_params(shaft)
if not sh:
    print("Проблема при загрузке параметров выработки из: %s"%shaft)
    sys.exit()
mm = 0.25
sh_bm =  sh._replace(mu=mm, mu_p = mu_plain(mm), Em_p = E_plain(sh.Em*0.9, mm), Em_ra = ERA(E_plain(sh.Em, mm),sh.a,sh.n,1.0))
sh__m =  sh._replace(mu=mm, mu_p = mu_plain(mm), Em_p = E_plain(sh.Em, mm), Em_ra = ERA(E_plain(sh.Em, mm),sh.a,sh.n,1.0), a=0, n=0, k=0, b=0)
mm = 0.32
sh_bm2 = sh._replace(mu=mm, mu_p = mu_plain(mm), Em_p = E_plain(sh.Em, mm), Em_ra = ERA(E_plain(sh.Em, mm),sh.a,sh.n,1.0))
sh__m2 = sh._replace(mu=mm, mu_p = mu_plain(mm), Em_p = E_plain(sh.Em, mm), Em_ra = ERA(E_plain(sh.Em, mm),sh.a,sh.n,1.0), a=0, n=0, k=0, b=0)
mm = 0.5
sh_b_ =  sh._replace(mu=mm, mu_p = mu_plain(mm), Em_p = E_plain(sh.Em, mm), Em_ra = ERA(E_plain(sh.Em, mm),sh.a,sh.n,1.0))
sh___ =  sh._replace(mu=mm, mu_p = mu_plain(mm), Em_p = E_plain(sh.Em, mm), Em_ra = ERA(E_plain(sh.Em, mm),sh.a,sh.n,1.0), a=0, n=0, k=0, b=0)

stddataset = (sh_bm, sh__m, sh_bm2, sh__m2, sh_b_, sh___)
result = []
results = [("Значения I",                                     (sh__m, sh__m2, sh___), result),
           ("Программа (Значения II, Shook=0, Sb=0, Sa!=0)",  stddataset,             result),
           ("Программа (Значения II + I)",                    stddataset,             result),
           ("Программа (Значения III, Shook=0, Sb!=0, Sa=0)", stddataset,             result),
           ("Программа (конечные значения IV, изменённый закон гука, Shook!=0, Sb!=0, Sa=0)", stddataset, result),
           ("Баклашов (значения, II)", (sh_b_, sh___), result), ("Баклашов (конечные значения, I+II)", (sh_b_, sh___), result)]


S = sh_bm.S
print("Параметры:")
print(sh_bm)
print("dr = %8.4f мм"%((sh___.Rb-sh___.Ra)/N))
print("Коэффициент Пуассона, модуль Юнга для плоской деформации и для буровзрывных работ - переменные")
print("Значения величин приведены для контура выработки.")
print("EET_k - окружные деформации (из формулы Коши); EET_h - окружные деформации (из закона Гука).")
print("E_s - модуль упругости из системы; E_h - модуль упругости из закона Гука.")
print("'+' или '-' - закон Гука выполняется или невыполняется соответственно")
print("\n")
print("Значения I")

for param in results[0][1]:
    results[0][2].append(calc_I(param))

print("\n")
print("Программа (Значения II, Shook=0, Sb=0, Sa!=0)")
II_bm  = calc_prog(sh_bm,  N, eFDM_S0, funcII_1D_ssr0)
II__m  = calc_prog(sh__m,  N, eFDM_S0, funcII_1D_ssr0)
II_bm2 = calc_prog(sh_bm2, N, eFDM_S0, funcII_1D_ssr0)
II__m2 = calc_prog(sh__m2, N, eFDM_S0, funcII_1D_ssr0)
II_b_  = calc_prog(sh_b_,  N, eFDM_S0, funcII_1D_ssr0)
II___  = calc_prog(sh___,  N, eFDM_S0, funcII_1D_ssr0)

print("\n")
print("Программа (Значения II + I)")
Sum_bm  = calc_I_II(sh_bm, II_bm)
Sum__m  = calc_I_II(sh__m, II__m)
Sum_bm2 = calc_I_II(sh_bm2,II_bm2)
Sum__m2 = calc_I_II(sh__m2,II__m2)
Sum_b_  = calc_I_II(sh_b_, II_b_)
Sum___  = calc_I_II(sh___, II___)

print("\n")
print("Программа (Значения III, Shook=0, Sb!=0, Sa=0)")
III_bm  = calc_prog(sh_bm,  N, eFDM_S0, funcIV_1D)
III__m  = calc_prog(sh__m,  N, eFDM_S0, funcIV_1D)
III_bm2 = calc_prog(sh_bm2, N, eFDM_S0, funcIV_1D)
III__m2 = calc_prog(sh__m2, N, eFDM_S0, funcIV_1D)
III_b_  = calc_prog(sh_b_,  N, eFDM_S0, funcIV_1D)
III___  = calc_prog(sh___,  N, eFDM_S0, funcIV_1D)

print("\n")
print("Программа (конечные значения IV, изменённый закон гука, Shook!=0, Sb!=0, Sa=0)")
print(table_head)
IV_bm  = calc_prog(sh_bm,  N, eFDM, funcIV_1D)
IV__m  = calc_prog(sh__m,  N, eFDM, funcIV_1D)
IV_bm2 = calc_prog(sh_bm2, N, eFDM, funcIV_1D)
IV__m2 = calc_prog(sh__m2, N, eFDM, funcIV_1D)
IV_b_  = calc_prog(sh_b_,  N, eFDM, funcIV_1D)
IV___  = calc_prog(sh___,  N, eFDM, funcIV_1D)


print("\n")
print("Баклашов (значения, II)")
II_bak_b_ = calc_bak(sh_b_,  N, 0)
II_bak___ = calc_bak(sh___,  N, 0)

print("\n")
print("Баклашов (конечные значения, I+II)")
Sum_bak_b_ = calc_bak(sh_b_,  N, -S)
Sum_bak___ = calc_bak(sh___,  N, -S)

print("\n")
print ("Зависимость SST от коэффициента Пуассона (приведён без учёта плоской деформации) на этапе II. Без БВР.")
print ('  mu      SST [МПа]')
print ("%8.4f  %8.4f" % (sh__m.mu, II__m[2]))
print ("%8.4f  %8.4f" % (sh__m2.mu,II__m2[2]))
print ("%8.4f  %8.4f" % (sh___.mu, II___[2]))
print ("Макс. разница значений SST: %8.4f (КПа)" % (max ([abs(II__m[2] - II__m2[2]), abs(II__m[2] - II___[2]),
                                       abs(II__m2[2] - II___[2])]) * 1000))

print("\n")
print("Различия решений для этапа II")
print("с буровзрывными работами: Бак       prog       delta")
print(U_row %   (II_bak_b_[0], II_b_[0], abs(II_bak_b_[0] - II_b_[0])))
print(SSt_row % (II_bak_b_[2], II_b_[2], 1000*abs(II_bak_b_[2] - II_b_[2])))
print(SSr_row % (II_bak_b_[1], II_b_[1], 1000*abs(II_bak_b_[1] - II_b_[1])))
print("без буровзрывных работ")
print(U_row %   (II_bak___[0], II___[0], 1000*abs(II_bak___[0] - II___[0])))
print(SSt_row % (II_bak___[2], II___[2], 1000*abs(II_bak___[2] - II___[2])))
print(SSr_row % (II_bak___[1], II___[1], 1000*abs(II_bak___[1] - II___[1])))

print("\n")
print("Различия решений для этапa I+II")
print("с буровзрывными работами: Бак       prog       delta")
print(U_row %   (Sum_bak_b_[0], Sum_b_[0], abs(Sum_bak_b_[0] - Sum_b_[0])))
print(SSt_row % (Sum_bak_b_[2], Sum_b_[2], 1000*abs(Sum_bak_b_[2] - Sum_b_[2])))
print(SSr_row % (Sum_bak_b_[1], Sum_b_[1], 1000*abs(Sum_bak_b_[1] - Sum_b_[1])))
print("без буровзрывных работ")
print(U_row %   (Sum_bak___[0], Sum___[0], abs(Sum_bak___[0] - Sum___[0])))
print(SSt_row % (Sum_bak___[2], Sum___[2], 1000*abs(Sum_bak___[2] - Sum___[2])))
print(SSr_row % (Sum_bak___[1], Sum___[1], 1000*abs(Sum_bak___[1] - Sum___[1])))

print("\n")
print("Различия решений I+II и IV")
print("с буровзрывными работами: I+II      IV         delta")
print("mu = %8.4f"% sh_bm.mu)
print(U_row %   (Sum_bm[0], IV_bm[0], abs(Sum_bm[0] - IV_bm[0])))
print(SSt_row % (Sum_bm[2], IV_bm[2], 1000*abs(Sum_bm[2] - IV_bm[2])))
print(SSr_row % (Sum_bm[1], IV_bm[1], 1000*abs(Sum_bm[1] - IV_bm[1])))
print("без буровзрывных работ")
print(U_row %   (Sum__m[0], IV__m[0], abs(Sum___[0] - IV___[0])))
print(SSt_row % (Sum__m[2], IV__m[2], 1000*abs(Sum___[2] - IV___[2])))
print(SSr_row % (Sum__m[1], IV__m[1], 1000*abs(Sum___[1] - IV___[1])))
print("mu = %8.4f"% sh_bm.mu)
print("с буровзрывными работами: I+II      IV         delta")
print(U_row %   (Sum_bm2[0], IV_bm2[0], abs(Sum_bm2[0] - IV_bm2[0])))
print(SSt_row % (Sum_bm2[2], IV_bm2[2], 1000*abs(Sum_bm2[2] - IV_bm2[2])))
print(SSr_row % (Sum_bm2[1], IV_bm2[1], 1000*abs(Sum_bm2[1] - IV_bm2[1])))
print("без буровзрывных работ")
print(U_row %   (Sum__m2[0], IV_bm[0], abs(Sum__m2[0] - IV_bm[0])))
print(SSt_row % (Sum__m2[2], IV_bm[2], 1000*abs(Sum__m2[2] - IV_bm[2])))
print(SSr_row % (Sum__m2[1], IV_bm[1], 1000*abs(Sum__m2[1] - IV_bm[1])))
print("mu = %8.4f"% sh_bm.mu)
print("с буровзрывными работами: I+II      IV         delta")
print(U_row %   (Sum_b_[0], IV_b_[0], abs(Sum_b_[0] - IV_b_[0])))
print(SSt_row % (Sum_b_[2], IV_b_[2], 1000*abs(Sum_b_[2] - IV_b_[2])))
print(SSr_row % (Sum_b_[1], IV_b_[1], 1000*abs(Sum_b_[1] - IV_b_[1])))
print("без буровзрывных работ")
print(U_row %   (Sum___[0], IV___[0], abs(Sum___[0] - IV___[0])))
print(SSt_row % (Sum___[2], IV___[2], 1000*abs(Sum___[2] - IV___[2])))
print(SSr_row % (Sum___[1], IV___[1], 1000*abs(Sum___[1] - IV___[1])))

print("\n")
print("Влияние истории нагружения (На примере решений III и IV)")
print("mu = %8.4f"% sh_bm.mu)
print(U_row %   (III_bm[0], IV_bm[0], abs(III_bm[0] - IV_bm[0])))
print(SSt_row % (III_bm[2], IV_bm[2], 1000*abs(III_bm[2] - IV_bm[2])))
print(SSr_row % (III_bm[1], IV_bm[1], 1000*abs(III_bm[1] - IV_bm[1])))
print("без буровзрывных работ")
print(U_row %   (III__m[0], IV__m[0], abs(III___[0] - IV__m[0])))
print(SSt_row % (III__m[2], IV__m[2], 1000*abs(III___[2] - IV__m[2])))
print(SSr_row % (III__m[1], IV__m[1], 1000*abs(III___[1] - IV__m[1])))
print("mu = %8.4f"% sh_bm2.mu)
print(U_row %   (III_bm2[0], IV_bm2[0], abs(III_bm2[0] - IV_bm2[0])))
print(SSt_row % (III_bm2[2], IV_bm2[2], 1000*abs(III_bm2[2] - IV_bm2[2])))
print(SSr_row % (III_bm2[1], IV_bm2[1], 1000*abs(III_bm2[1] - IV_bm2[1])))
print("без буровзрывных работ")
print(U_row %   (III__m2[0], IV__m2[0], abs(III__m2[0] - IV__m2[0])))
print(SSt_row % (III__m2[2], IV__m2[2], 1000*abs(III__m2[2] - IV__m2[2])))
print(SSr_row % (III__m2[1], IV__m2[1], 1000*abs(III__m2[1] - IV__m2[1])))
