#!python3
# -*- coding: utf-8 -*-
import numpy
import scipy.linalg
from geom_aux import get_ssc_ttm

# # Квершлаг №1, гор.750
# ss_cmp_lab = 56.8
# ko = 0.14
# kd = 0.75
# S = 5.7
# kp = 1.8
# S= kp*S

# Квершлаг №1, гор.750 (edit)
ss_t_lab = 9.8
ss_cmp_lab = 56.8
ko = 0.14
kd = 0.75
ss_cmp = ss_cmp_lab*ko*kd
S = 6.7
kp = 1.8
S= kp*S 

# std
# ss_cmp_lab = 72
# ko = 0.2
# ss_cmp = ss_cmp_lab*ko
# S = 19.6
# kp = 1.8
# S= kp*S

ssc, ttm1, ttm2 = get_ssc_ttm(ss_t_lab, ss_cmp_lab, ko, kp, S, prog="../other/13i/pasp13i-p")
print(ssc, ttm1, ttm2)
ss_cmp = ss_cmp_lab*ko*kd
print("Предел прочности на сжатие массива ss_cmp: %.4f [МПа]"%ss_cmp)
print("Естественное давление в массиве (x Kп) S: %.4f [МПа]"%S)

ssc_s, ssc_cmp = -S, -ss_cmp/2
tm_s,  tm_cmp  =  8.5, ss_cmp/2

ssc_m =  (ssc_s+ssc_cmp)/2
tm_m =  5.903
print('')
print("σ_s = %2.4f, τ_s = %2.4f" % (ssc_s, tm_s))
print("σ_cmp = %2.4f, τ_cmp = %2.4f" % (ssc_cmp, tm_cmp))
print("σ_m = %2.4f, τ_m = %2.4f" % (ssc_m, tm_m))

ssr1, ssr2, ssr3 = ssc_s+tm_s, ssc_cmp+tm_cmp, ssc_m+tm_m
sst1, sst2, sst3 = ssc_s-tm_s, ssc_cmp-tm_cmp, ssc_m-tm_m
print('')
print("σ_r1 = %2.4f, σ_r2 = %2.4f, σ_r3 = %2.4f" % (ssr1, ssr2, ssr3))
print("σ_θ1 = %2.4f, σ_θ2 = %2.4f, σ_θ3 = %2.4f" % (sst1, sst2, sst3))

numpy.set_printoptions(precision=4, suppress=True)
A = numpy.matrix([
    [1, ssr1, ssr1 ** 2],
    [1, ssr2, ssr2 ** 2],
    [1, ssr3, ssr3 ** 2]])
B = numpy.matrix([[sst1], [sst2], [sst3]])
X = numpy.matrix([[0], [0], [0]])
print("\nСистема уравнений. Ax=B:")
print("A:")
print(A)
print("\nB:")
print(B)

try:
	X = scipy.linalg.solve(A, B)
except: print("X - произвольный")
else:
	print("\nX: c0, c1, c2")
	print(X)
