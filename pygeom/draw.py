__author__ = 'sotona'
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm

#d - Detail
def draw(d):
    pass



def draw_f(sh, N, func, fdm):
    dx, dy = 1.25, 1.25
    # generate 2 2d grids for the x & y bounds
    y, x = np.mgrid[slice(-30, 30 + dy, dy), slice(-200, 160 + dx, dx)]
    z = func([x,y], sh, N, fdm)
    z = z[:-1, :-1]
    z_min, z_max = -np.abs(z).max(), np.abs(z).max()

    plt.clf()
    plt.pcolor(x, y, z, cmap=cm.gray, vmin=z_min, vmax=z_max)
    plt.title('F(u0,ssr0)')
    # set the limits of the plot to the limits of the data
    plt.axis([x.min(), x.max(), y.min(), y.max()])
    plt.colorbar()
    plt.show()

