#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from geom_aux import load_params

__author__ = 'sotona'
from sys import exit
from scipy.optimize import minimize
import matplotlib.pyplot as plt
import numpy
from geom import *


def print_annotation(filename, sh, N):
    print("Выработка: %s"%filename)
    print("Естественное давление (с учётом коэф. перегрузки), S: %2.4f МПа"%sh.S)
    print("Ширина выработки, ra: %4.2f мм"%sh.Ra)
    print("Коэффициент Пуассона: %0.2f, %2.4f (для плоской деформации)"%(sh.mu, sh.mu_p))
    print("Радиус rb:  %1.2f ra"%(sh.Rb/sh.Ra))
    print("Число точек на радиусе: %i"%N)


def print_result(sh, res, d):
    """
    :param sh: Shaft
    :param res: result of minimize function from scipy.optimize
    :param d: Detail
    """
    print("Значение целевой функции: %e"%res.fun)
    print("r[ra]   u[мм]    SSr[MPa]    SSt[MPa]")
    print("%1.2f    %1.4f   %1.4f   %1.4f"%(d.r[0]/sh.Ra, d.U[0], d.SSr[0], d.SSt[0]))
    i=N//20
    print("%1.2f    %1.4f   %1.4f   %1.4f"%(d.r[i]/sh.Ra, d.U[i], d.SSr[i], d.SSt[i]))
    i=i*3
    print("%1.2f    %1.4f   %1.4f   %1.4f"%(d.r[i]/sh.Ra, d.U[i], d.SSr[i], d.SSt[i]))
    print()


def save_results(d, sh, plt_dir, data_filename, show=False):
    """
    :param show:
    :param d: Detail
    :param sh: Shaft
    :param plt_dir: dir for plots saving
    :param show: show plots?
    :return:
    """
    ssr = d.SSr[:-1]
    sst = d.SSt[:-1]
    u = d.U[:-1]
    r = [r/sh.Ra for r in d.r[:-1]]

    fig = plt.figure()
    fig.canvas.set_window_title('Напряжения. %s'%shaft_name)

    plt.plot(r,ssr,"b",label="радиальные напряжения")
    plt.plot(r,sst,"g",label="тангенциальные напряжения")
    plt.xlabel("r, [$r_a$]")
    plt.ylabel("Напряжение [МПа]")
    plt.axhline(-sh.S,ls ="dashed",color="red")
    plt.grid()
    plt.legend(loc='best')
    plt.title("радиальные и тангенциальные напряжения $\sigma_r, \sigma_{\\theta}$",fontsize=16)
    savefig(plt_dir+'/ssr,sst.png')
    if show: plt.show()

    fig = plt.figure()
    fig.canvas.set_window_title('Перемещения. %s'%shaft_name)

    plt.plot(r,u)
    plt.xlabel("r, [$r_a$]")
    plt.ylabel("Перемещения [мм]")
    plt.grid()
    plt.title("радиальные перемещения",fontsize=16)
    savefig(plt_dir+'/u.png')
    if show: plt.show()

    header = "r[r_a] r[mm]  Er[MPa] SScm[MPa]   u[mm]   SSr[MPa]    SSt[MPa]    EEr     EEt"
    data = numpy.asarray([r, d.r[:-1], d.Er[:-1], d.SScm[:-1], u, ssr, sst, d.EEr[:-1], d.EEt[:-1]])
    numpy.savetxt(data_filename,  numpy.transpose(data), delimiter=" ", fmt="%3.4f", header = header)


plt_dir = 'plots'
filename = "../asp/lbin/750_e.ini"
shaft_name = filename.split('/')[-1]
data_file = 'data.'+shaft_name+".csv"
show_plots = False # Показывать графики во время работы программы?
sh = load_params(filename)
N = 1000
if not sh: print("Eror opening %s"%filename); sys.exit()

print_annotation(filename, sh, N)

x0 = [0,0]
res = minimize(funcIV_1D, x0, args=(sh, N, eFDM))
d = eFDM(res.x, sh, N)

save_results(d,sh,plt_dir, data_file, show=False)

print_result(sh,res,d)
print("Графики сохранены в каталог %s, данные в файл %s"%(plt_dir, data_file))

# draw_f(sh,N,func_2d_lin,eFDM)