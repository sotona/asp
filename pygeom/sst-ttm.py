# -*- coding: utf-8 -*-
__author__ = 'sotona'
import matplotlib.pyplot as pl
import numpy as np
from scipy.stats.stats import pearsonr

from geom_aux import get_ssc_ttm


def plot(ss3, ss1, ss3_2, ss1_2, ssc, ttm1, ttm2, p):
    # n=100
    # dx = (ss3[-1]-ss3[0])/n
    # x = [ss3[0]-n/20*dx + dx*i for i in range(n+40)]
    # y1 = [p(xi) for xi in x]
    # y2 = [p(xi) for xi in x]
    # pl.autoscale(True, tight=False)
    # pl.grid(True)
    # pl.title("ss1 = f(ss3)")
    # pl.xlabel('ss3 = $\sigma_c + \\tau_m$')
    # pl.ylabel('ss1')
    # pl.plot(x,y1,'b--', label='P(5)')
    # pl.plot(x,y2,'g--', label='P(1)')
    # pl.plot(ss3,ss1,'ro', label='exp. data')
    # pl.legend(loc='best')
    # pl.show()


    pl.autoscale(True)
    pl.grid(True)
    pl.title("$\\tau_m$ = f($\sigma_c$)")
    pl.xlabel('$\sigma_c$ [МПа]')
    pl.ylabel('$\\tau_m$ [МПа]')
    pl.plot(ssc, ttm1, 'ro-', label='exp. data ($\\tau_{m1}$)')
    pl.plot(ssc, ttm2, 'go-', label='exp. data ($\\tau_{m2}$)')
    pl.legend(loc='best')
    pl.show()



#Камера ЦПП
ko = 0.11  # коэф. структурного ослабления
kd = 0.85  # коэф. длительной прочности
kp = 2.1  # коэф. перегрузки
ss_c = 120  #67.4  # предел прочности на сжатие [МПа]
ss_t = 6.3  # предел прочности на растяжение [МПа]
sigma_v = 16.3  # вертикальное напряжение [МПа]

min_error = 1/1000  # МПа

ss_cm = ss_c*kd*ko
sigma_v1 = 1.5 * ss_cm/2
sigma_vz = kp * sigma_v if sigma_v > sigma_v1 else sigma_v1
d = (sigma_vz - ss_cm/2)/5
ssc = [ss_cm/2 + d*i for i in range(1,6)]

ttm1, ttm2 = [], []
for c in ssc:
    s, t1, t2 = get_ssc_ttm(ss_t,ss_c,ko,kp,c,prog="../other/13i/pasp13i-p")
    ttm1.append(t1)
    ttm2.append(t2)
# ttm = [4.789, 6.481, 8.125, 9.756, 11.374]
ss1 = [s + t for s, t in zip(ssc, ttm1)]
ss3 = [s - t for s, t in zip(ssc, ttm1)]
ss1_2 = [s + t for s, t in zip(ssc, ttm2)]
ss3_2 = [s - t for s, t in zip(ssc, ttm2)]

print("Предел прочности на сжатие = %2.4f МПа"%ss_cm)
print("sigma_v  = %2.4f * %0.2f = %2.4f МПа" % (sigma_v, kp, sigma_v*kp))
print("sigma_v1 = %2.4f МПа" % sigma_v1)
print("sigma_v* = %2.4f МПа" % sigma_vz)
print("delta = %2.4f МПа" % d)
print()
print("SSc:  "+', '.join(map(lambda x: "%7.3f"%x, ssc)))
print("TTm1: "+', '.join(map(lambda x: "%7.3f"%x, ttm1)))
print("TTm2: "+', '.join(map(lambda x: "%7.3f"%x, ttm2)))
print("")
print("SS1:   "+', '.join(map(lambda x: "%7.3f"%x, ss1)))
print("SS3:   "+', '.join(map(lambda x: "%7.3f"%x, ss3)))
print("SS1_2: "+', '.join(map(lambda x: "%7.3f"%x, ss1_2)))
print("SS3_2: "+', '.join(map(lambda x: "%7.3f"%x, ss3_2)))
print("")
print("Коэффициент корреляции для ttm1 = f(ssc): %0.6f" % pearsonr(ssc, ttm1)[0])
print("Коэффициент корреляции для ttm2 = f(ssc): %0.6f" % pearsonr(ssc, ttm2)[0])
print("")
print("Коэффициенты полинома. Первый - свободный член")
print(" c[0]     c[1]      c[2]    c[3]     c[4]")
error = []
P = None
min_i = 3
for i in range(4,0,-1):
    c = np.polyfit(ss3,ss1, i)
    p = np.poly1d(c) # ss1 = f(ss3)
    print(', '.join(map(lambda x: "%7.4f"%x, p.c[::-1])))
    error += [abs(p(ss3[-1])-ss1[-1])]
    if error[-1]<min_error:
        min_i=i
        P = p
    print("Ошибка для ssc5: %3.5f КПа"%(error[-1]*1000))
print('')
print("Минимальная допустимая ошибка %7.4f (< %1.4f) КПа для полинома степени %i: "%(error[-min_i]*1000, 1000*min_error, min_i)+
      ', '.join(map(lambda x: "%7.4f"%x, P)))
plot(ss3, ss1, ss3_2, ss1_2, ssc, ttm1, ttm2, P)



#TODO
