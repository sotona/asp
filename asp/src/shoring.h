#ifndef SHORING_H
#define SHORING_H
#include <list>
#include <string>
#include <vector>
#include <QString>

/* file format:
 * width_min[m] width_max[m] name P[kH]
 */

struct shoring{
float w_min, w_max;
float P;
float density;  // 1/m^2
QString name;};

typedef std::vector<shoring> shoringArray;


shoringArray *load_shoring_data(QString filename);
shoringArray *calc_shorings(double width, double P, const shoringArray &sl);
QString make_shoring_report(const shoringArray& sshar);

#endif // SHORING_H
