#ifndef AUXILIARY_H
#define AUXILIARY_H

#include "calculation.h"
#include <../qwt/src/qwt_plot.h>
#include <../qwt/src/qwt_plot_grid.h>
#include <../qwt/src/qwt_plot_picker.h>
#include <../qwt/src/qwt_plot_magnifier.h>
#include <../qwt/src/qwt_plot_panner.h>
#include <../qwt/src/qwt_plot_marker.h>
#include <../qwt/src/qwt_symbol.h>
#include <../qwt/src/qwt_picker_machine.h>
#include <../qwt/src/qwt_legend.h>
#include <QImage>
#include <QTableWidget>
#include <QLibrary>

class PlotStaff{
public:
 QwtPlotGrid *grid;
 QwtPlotPicker *picker;
 QwtPlotMagnifier *mag;
 QwtPlotPanner *panner;
 QwtLegend *legend;

 PlotStaff();
 PlotStaff(QwtPlot *plot, QString bttm_ax_titl="", QString lft_ax_ttl="");
 ~PlotStaff();
 };


extern const char* el_report_fn;
extern const char* ep_report_fn;
extern const char* report_dir;
extern const char* el_data;
extern const char* el_data_b;
extern const char* el_data_cem;
//extern const char* ep_data;
extern const char* rzv_data[];


/// for PlotStaff array
namespace plot_ind {
const unsigned char u=0;
const unsigned char sst=1;
const unsigned char ssr=2;
const unsigned char fos=3;
const unsigned char p=4;
const unsigned char r=5;
const unsigned char k=6;}


void setup_grid(QwtPlotGrid* grid);
void setup_picker(QwtPlotPicker* p, QwtPlot *plot);


//-------------------------------------------
inline void add_marker(QwtPlotMarker** radius_markers, unsigned count, const double *val, double k, QwtPlot* p,
                            const OneDGrid& g, double Ra){
for(unsigned j=0;j<count;j++){
unsigned ind = g.indByVal((j+1)*Ra);
radius_markers[j]->setValue(QPointF(g.r(ind)/Ra,val[ind]/k));
radius_markers[j]->setLabel(QwtText(QString::number(val[ind]/k)));
radius_markers[j]->attach(p);}}


//-------------------------------------------
inline QwtPlotMarker* create_line_marker(const QString& label,QPen pen, Qt::Orientation orient = Qt::Horizontal,
                 QwtPlotMarker::LineStyle linestyle = QwtPlotMarker::HLine){
QwtPlotMarker *m = new QwtPlotMarker();
m->setLabel(label);
m->setLabelAlignment(Qt::AlignLeft | Qt::AlignBottom);
m->setLabelOrientation(orient);
m->setLineStyle(linestyle);
m->setLinePen(pen);
return m;}

/// insert some values into result table
inline void insert_to_table(QTableWidget* tw, double item, unsigned row, unsigned col, int  prec = 4){
tw->setItem(row, col, new QTableWidgetItem(QString::number(item, 'f', prec)));}

inline void insert_to_table(QTableWidget* tw, QString item, unsigned row, unsigned col){
tw->setItem(row, col, new QTableWidgetItem(item));}

QwtPlotMarker* create_dot_marker(const QColor &c);

QImage * make_hmap(QRectF r, float dx, float dy, const cm::foo f, Shaft& sh);


void calc_delta(QTableWidget* tab);

QString gen_RPdatafilename(const prob_param& pp);
bool calc_ff(Media& m, double S);
#endif // AUXILIARY_H
