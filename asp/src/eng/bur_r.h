﻿#ifndef BUR_R_H
#define BUR_R_H

#include <math.h>
#include "general.h"

/// @param br - distance between conouring holes []
/// @param ssl - lab's compression sigma [MPa]
/// r0 - radius of the shaft [mm]
void bur(double br, double ssl, double r0,geomech::DnBparam& dbp);
void no_bur(geomech::DnBparam &dbp);

inline void bur_options(geomech::DnBparam& dbp, double a, double n, double b, double k){
dbp.a=a; dbp.n=n; dbp.b=b; dbp.k=k;}

void cem_options(geomech::DnBparam &dbp, double rz);

#endif //  bur_rH
