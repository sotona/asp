#include <math.h>
#include <string.h>
#include "general.h"

namespace geomech{
//double x[ln+1], pogp[ln+1], pogm[ln+1]; //v1
//int n;
double ff;
//==========================================
OneDGrid::OneDGrid(){a=0; b=0; _d=0; _n=0; _r=NULL;}
OneDGrid::OneDGrid(const OneDGrid&g){
this->a=g.ra();
this->b=g.rb();
this->_n=g.n();
this->_d=g.dr();
_r = new double [g.n()];
memcpy(_r, g.r(), _n*sizeof(double));}
OneDGrid::OneDGrid(double a, double b, N_Type n){init(a,b,n);}
//-----------------------------------------
void OneDGrid::init(double a0, double b0, N_Type n0){
if (n0<=1) n0=2; 
a=a0; b=b0;_d=(b-a)/(n0-1);
if (n0!=_n){
    if(_r!=NULL)delete[] _r;
    _r=new double [n0];  _n=n0;}
for (N_Type i=0; i<n0; i++){
_r[i]= a + _d*i;}}

//-----------------------------------------
double OneDGrid::ra()const {return a;}
double OneDGrid::rb()const {return b;}
double OneDGrid::dr()const {return _d;}
OneDGrid::N_Type OneDGrid::n()const {return _n;}
double OneDGrid::r(N_Type i) const {if (i<_n) return _r[i]; else return _r[_n-1];}
double OneDGrid::rau(N_Type i) const {if (i<_n) return _r[i]/a; else return _r[_n-1]/a;}
const double *OneDGrid::r() const {return _r;}
OneDGrid::N_Type OneDGrid::indByVal(double val) const {
if (val<=rb() && val>=ra()) return (val-ra())/(rb()-ra())*n();
else return 0;
}
OneDGrid::~OneDGrid(){if (_r) delete[]_r;}

//==========================================
// kr - nodes count at radius, rt - nodes array
void  Media::set_consts(){
    aa = sin(ff1);
    aa0=sin(ff0);
    bb = ( 1. + aa )/( 1. - aa );   //beta in safety coefficient and pf1()
//    aa = sin(ff0);
    bb0 = ( 1. + aa0 )/( 1. - aa0 );   //beta in safety coefficient and pf1()
    ssm=ssl_c*ko*kd;
    Glab=Elab/2/(1+mm);
    Em=Elab*0.75*(1.+ko)/2.;
//    Em = Elab*0.75* (0.0168+0.773*ko + 0.119*ko*ko);
    mm_p = mm/(1.0-mm);           // plain deformation
    Em_p = Em/(1.0-mm*mm);          // plain deformation
    double tm=Elab*0.75*(1+ko)/2*(0.5-mm)/(0.5-0.2)*(2.5+ku)*aa0/sin(45.0/180*3.1415);
    /*/sin(45/180*3.1415);*///2.5
    T0=ssm*(1-aa)/2;
    T=tm/(bb-1);
    }
//==========================================
DataGrid::DataGrid(){
init();}

void DataGrid::init(){
this->size=0;
Er =    NULL;   Gr =    NULL;
u =     NULL;   ro =    NULL;
dru =   NULL;
drssr = NULL;   drdro = NULL;
sst =   NULL;   ssr   = NULL;
eet =   NULL;   eer =   NULL;
ss_c=   NULL;   res_k=  NULL;
sso =   NULL;
thetta = NULL;}

void DataGrid::alloc(unsigned n){
n+=1;
sso = new double[n];
res_k= new double[n];
thetta = new double[n];
Er = new double[n];    Gr = new double[n];
u = new double[n];     ro = new double[n];
dru = new double[n];
drssr = new double[n]; drdro = new double[n];
sst = new double[n];   ssr   = new double[n];
eet = new double[n];   eer = new double[n];
ss_c= new double[n];
}

void DataGrid::free(){
delete[] Er;
delete[] u;
delete[] Gr;
delete[] ro;
delete[] dru;
delete[] drdro;
delete[] drssr;
delete[] sst;
delete[] ssr;
delete[] eet;
delete[] eer;
delete[] ss_c;
delete[] thetta;
delete[] res_k;
delete[] sso;
}

DataGrid::DataGrid(OneDGrid::N_Type size){
init(); alloc(size); this->size=size;
clear();}

DataGrid::DataGrid(const DataGrid &dg){
init(); resize(dg.n());
memcpy( Er,     dg.Er,     size*sizeof(double));
memcpy( Gr,     dg.Gr,     size*sizeof(double));
memcpy( u,      dg.u,      size*sizeof(double));
memcpy( ro,     dg.ro,     size*sizeof(double));
memcpy( dru,    dg.dru,    size*sizeof(double));
memcpy( drdro,  dg.drdro,  size*sizeof(double));
memcpy( sst,    dg.sst,    size*sizeof(double));
memcpy( ssr,    dg.ssr,    size*sizeof(double));
memcpy( drssr,  dg.drssr,  size*sizeof(double));
memcpy( eet,    dg.eet,    size*sizeof(double));
memcpy( eer,    dg.eer,    size*sizeof(double));
memcpy( ss_c,   dg.ss_c,   size*sizeof(double));
memcpy( sso,    dg.sso,    size*sizeof(double));
memcpy( res_k,  dg.res_k,  size*sizeof(double));
memcpy( thetta, dg.thetta, size*sizeof(double));
}

OneDGrid::N_Type DataGrid::n() const{return size;}

void DataGrid::resize(OneDGrid::N_Type n){
free(); this->size = n;
alloc(size); clear();}

void DataGrid::clear(){
memset( Er,     0,size*sizeof(double));
memset( Gr,     0,size*sizeof(double));
memset( u,      0,size*sizeof(double));         ///< radial dispacement
memset( ro,     0,size*sizeof(double));
memset( dru,    0,size*sizeof(double));
memset( drdro,  0,size*sizeof(double));
memset( sst,    0,size*sizeof(double));
memset( ssr,    0,size*sizeof(double));  ///< circ. and radial stress
memset( drssr,  0,size*sizeof(double));
memset( eet,    0,size*sizeof(double));
memset( eer,    0,size*sizeof(double));  ///< circ. and radial deformation
memset( ss_c,   0,size*sizeof(double));
memset( sso,    0,size*sizeof(double));
memset( res_k,  0,size*sizeof(double));
memset( thetta, 0,size*sizeof(double));
}
DataGrid::~DataGrid(){
free();}


//==========================================
// g - zone's grid, g.ra not necessary equal mine's radius Ra;
void initYongModulus(DataGrid& dg, const Media& m, const OneDGrid& g, double Ra){
for (OneDGrid::N_Type i = 0; i < g.n(); i++){
dg.Er[i] = m.Em_p * ( 1.0 - m.db_param.a * pow(g.r(i)/Ra, - m.db_param.n ));
dg.Gr[i] = dg.Er[i]/(2.0*( 1.0 + m.mm_p ));}}

void initYongModulus_cem(DataGrid& dg, const Media& m, const OneDGrid& g, double Ra){
for (OneDGrid::N_Type i = 0; i < g.n(); i++){
dg.Er[i] = m.Em_p * ( 1.0 + m.db_param.a * pow(g.r(i)/Ra, - m.db_param.n ));
dg.Gr[i] = dg.Er[i]/(2.0*( 1.0 + m.mm_p ));}}

void initCompSigma(DataGrid& dg, const Media& m, const OneDGrid& g, double Ra){
for (OneDGrid::N_Type i = 0; i < g.n(); i++){
dg.ss_c[i] = m.ssm*(1.0 - m.db_param.b * pow(g.r(i)/Ra, - m.db_param.k));}}


//==========================================
Shaft::Shaft(){param.P=0; param.S=0; param.Ra=0; param.Rb=0;}

Shaft::Shaft(double ra, double rb){
param.Ra=ra; param.Rb=rb;}


void Shaft::clean(){
    elastic.detail.clear();
    elastic_cem.detail.clear();
    bak.detail.clear();
    plastic1.detail.clear();
    plastic2.detail.clear();}
}
