#ifndef GENERAL2_H
#define GENERAL2_H
#include "general.h"

using namespace geomech;

namespace plastic_zone_t{
void set_consts(const OneDGrid& g, Media& m, Crap &s, double S);

/// calculate values for plastic zone [pl]
void calc_at_raduis(geomech::Region<DataGrid> &plr, const Media& m,const Crap& s, int i, bool flagrk, bool flagrp, double *x);

/// calculate values at radius r*
/// results -> x;  ssrz=x[1]; sstz=x[2]; u*=x[3];
void calc_at_radius_z(double ra, double rz,const Media&m, Crap& s, double S, double *x);

// calculate pastic deformation's zone, need initialized values at Rz
bool calc(Shaft& sh);
}
#endif // GENERAL2_H

/*
 * [pl] (2013): Расчет горного давления в одиночных горизонтальных выработках, проведенных буровзрывным способом, при наличии запредельной зоны. In Известия вузов. Горный журнал (1), pp. 67–72.
 *
 *
 */
