﻿#include "bur_r.h"

/// a\b_, n\k values from x(r) = (1-a*r^-k).
void bur(double br, double ssl, double r0, geomech::DnBparam& dbp){
  double M__ = 0.0, m_ = 0.0;
  if ( ( ssl > 20. ) && ( ssl <= 40. ) ){M__ = 1.8;  m_ = 0.85;}
  if ( ( ssl > 40. ) && ( ssl <= 60. ) ){M__ = 1.3;  m_ = 0.75;}
  if ( ( ssl > 60. ) && ( ssl <= 80. ) ){M__ = 1.0;  m_ = 0.70;}
  if ( ssl > 80 )  {  M__ = 0.9;   m_ = 0.60;  }
  dbp.n = 1./log10( 1. + M__ / ( ( r0 / 1000.0 ) * pow( br / 1000.0, m_)));
  dbp.k = dbp.n;

dbp.b=0.9; // beta in ssc equation
dbp.a = pow( 0.98, dbp.n);}

void no_bur(geomech::DnBparam &dbp){
dbp.a=dbp.n=dbp.b=dbp.k=0.0;
}

void cem_options(geomech::DnBparam &dbp,double rz){
double a,b,n,k;
double r=rz;
double ex=1.6;//1.6
double ey=1.3;
double ssrx=2;//2.
double ssry=1.5;

a=ex-1.;
if (a<0)
n=log((ey-1.)/fabs(a))/log(r);
else
n=-log((ey-1.)/a)/log(r);
b=1.-ssrx;
if (b<0)
k=-log((ssry-1.)/fabs(b))/log(r);
else
k=log((ssry-1.)/b)/log(r);

dbp.a=a; dbp.n=n; dbp.b=b; dbp.k=k;
}



