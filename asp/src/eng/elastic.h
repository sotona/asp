#ifndef GENERAL_H
#define GENERAL_H
//general unit for those program.
/*There is general variables and functions used in deffirent programs.
*/

#include <math.h>
#include "general.h"
#include "bur_r.h"
#include "../cm/multidimensional_opt.h"
#include "../cm/unidimensional_opt.h"
using namespace geomech;
// U, drU, SSt, SSr, drSSr, EEt, EEr
struct PlainSym {
  double*  u;         ///< radial dispacement
  double*  dru;
  double*  sst, *ssr;  ///< circ. and radial stress
  double*  drssr;
  double*  eet, *eer;  ///< circ. and radial deformation
};
namespace elastic_zone{
// SSc, SSeq, res_k
struct SigDef {
  double* ss_c;  ///< ultimate compressive strength
  double* ss_eq; ///< equivalent stress
  double* res_k; ///< reserve coefficient
};

namespace strength_criterion {
 const unsigned char ssr_sst = 0;
 const unsigned char nl1 = 2;
 const unsigned char nl2 = 1;
}

double elastic_optfunc_2d(cm::double_vector& v, void* p); // [el]
double elastic_optfunc_nl(cm::double_vector& v, void* p);
double elastic_optfunc_nl2(cm::double_vector& v, void* p);
double elastic_optfunc_cem(cm::double_vector& v, void* p);

/// get values alange radiuse for elastic problem [el]
void  inr_u(const OneDGrid& g, const geomech::Media& m, geomech::DataGrid& res, double  S);
void  inr_u_cem(const OneDGrid& g, const Media& m, DataGrid& res, const double* ssrl,const double* sstl);

/// get values alange radiuse for elastic problem. logarifmic deformations
void  inr_ro(const OneDGrid& g, const Media& m, DataGrid& res, double S);          /*<getting dirivatives in derection r.  fenite diference method*/

/// calcfaltion in elastic region. [el]
/// @param x - initial values for u* and ssr*. IO param
double calc(Shaft& sh, cm::double_vector &x, double accuracy,
        bool elprob=0, unsigned char strength=strength_criterion::nl2);
double calc_cem(Shaft& sh, cm::double_vector &x, double accurary);
}
//=====================
namespace plastic_zone{
void calculation(Shaft& sh);}

#endif // GENERAL_H



/*
 * [pl_x] Немчин, Николай Павлович (2010): Решение осесимметричной задачи горного давления
 *  с несколькими неупругими зонами методом конечных разностей.
 *  In Известия вузов. Горный журнал (1), pp. 33–36.
 *
 * [el] МЕТОД УЧЁТА ИСТОРИИ НАГРУЖЕНИЯ В РЕШЕНИИ ЗАДАЧ УПРУГОСТИ ДЛЯ ОДИНОЧНЫХ ГОРИЗОНТАЛЬНЫХ ВЫРАБОТОК, ПРОВЕДЕННЫХ БУРОВЗРЫВНЫМ СПОСОБОМ. In Вестник ЗабГУ (6), pp. 37-47
*/
