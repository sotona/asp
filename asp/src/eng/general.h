#ifndef INKWN3_H
#define INKWN3_H

#include <stdlib.h>

namespace geomech{
extern double ff;

//===========================================
class OneDGrid{
public:
typedef long N_Type;

private:
double a,b;     ///< begin, end;
double _d;      ///< space between nodes
N_Type _n;    ///< node's count
double *_r;

public:

OneDGrid();
OneDGrid(const OneDGrid& g);
OneDGrid(double ra, double rb, N_Type n);     ///< begin, end, node's count
void init(double ra, double rb, N_Type n);
inline void init(double ra, double rb) {init(ra,rb,this->n());}
double ra() const;
double rb() const;
N_Type n() const;
double dr() const;
double r(N_Type i) const;
double rau(N_Type i) const;       ///< radius in ra units
const double * r() const;
N_Type indByVal(double val) const; ///< return index of point, by it's value
~OneDGrid();
// TODO добавить методы изменения Ra, Rb, n
};
//===========================================
/// Drilling and Blasting params
struct DnBparam {
  double a, n; ///< for E(i) = E(1-a*pow(n,r))
               ///< одной строчки мне мало
  double b, k;  ///< for ssc(i) = Sigma_m(1-b*pow(n,r))
};
/// params for strength criterion a - ssr*b + ssr*ssr *c = -sst
struct MediaParam_abc{
    double c0,c1,c2;};

//===========================================
// parameters of media (Er, Gr, mm, Elab)
struct Media {
  double Elab, Glab, Em, Em_p;     ///< Em_p - Young modulus for plain problem [MPa]
  double mm, mm_p;                 ///< Puasson's coefficient and another one for plain problem

  DnBparam db_param;
//  MediaParam_abc c;
  double c[3];               ///< coefficients in strenght criterion

  double density;            ///< плотность попрод
  double ssl_c;              ///< ultimate compressive strength lab. (UCS lab) [MPa]
  double ssl_t;              ///< ultimate tensive strength lab. (UCS lab) [MPa]
  double ko;                 ///< коэффициент структурного ослабления массива
  double kd;                 ///< коэффициент длительной прочности
  double kp;                 ///< коэф-т перегрузки
  double ku;                 ///< коэф-т, увеличивающий параметр от которого зависит снижение прочности в процессе разрыхления (0<ku<1)
  double ssm;                ///< /sigma сжатия массива                        [MPa]
  double T0;                 ///< постоянная из усл. прочности, зависящая от предела прочности на одноосное сжатие мас.
  double T;                  ///< параметр характеризующий разрушение массива вследствие разрыхления
  double aa, aa0;
  double bb, bb0;            ///< bb=beta. res.sst[1] = m.bb * res.ssr[1] - res.ss_c[1], another betta for dilatancy
  double ff1, ff0;            ///< angle of inner friction, dilatancy angle. [rad]
  int nx;                    ///< n*
  double rx;                 ///< r*
  void set_consts();
};
//===========================================
struct Crap{
    float omega;          ///< полярный угол в кровле выработки
    float ksr;           ///< коэффицент условия работы крепи (shorring resistance)
    double rp,rk;       ///< внешний радиус границы предельно разрушенной зоны, внешний радиус границы предельно разрыхленной зоны [мм]
    double ttk;         ///< limit thetta
    struct Media{
    float G;            ///< Shear modulus
    double E;           ///< smthng related to elab [MPa]
    } media;

    double tmz;
    double gmz;
    double ssgp;        ///< sigma compression max
    double T;           ///< T

    double sst/*[l+1]*/;
    double sstz/*[l+1]*/;

    double Br;      ///< расстояние мeжду шпурами
    double upr;     ///< пред перем
    double kd;      ///< к-т динамичности ри вывале пород в кровле
    double uw;      ///< пред перемещение при котором крепь полностью вступает в работу. (мм)
    double Pn;      ///< несущая способ-ть крепи
};


//===========================================
// Ro, drRo, U, drU, SSt, SSr, drSSr, EEt, EEr
class DataGrid {
private:
   void init();
   void alloc(unsigned size);
   void free();
   OneDGrid::N_Type size;
public:
  double *Er, *Gr;      ///< [MPa]
  double *u;            ///< radial dispacement, [mm]
  double *ro;           ///< [mm]
  double *dru;
  double *drdro;
  double *sst, *ssr;    ///< circ. and radial stress, [MPa]
  double *drssr;
  double *eet, *eer;    ///< circ. and radial deformation

  double *ss_c, *res_k;
  double *sso;
  double *thetta;

  void clear();         ///< clears all data

  DataGrid();
  DataGrid(OneDGrid::N_Type size);
  DataGrid(const DataGrid& dg);
  void resize(OneDGrid::N_Type n);
  OneDGrid::N_Type n() const;
  ~DataGrid();
};

//===========================================
template <class DG>
struct Region{
public:
    DG detail;
    OneDGrid grid;
};
//===========================================
struct main_param{
double P, S;                ///< pressure at inner and outer contour [MPa]
double Ra, Rb;              ///< [mm]
double w;                   ///< wdth [mm]
//double Rz;
double Rc;};

/// шахта
class Shaft{
public:
    Region<DataGrid> plastic1, plastic2, bak, elastic; ///< plastic analytical, plastic new, baklashov, elastic
    Region<DataGrid> elastic_cem;  ///< elastic cementation zone
    OneDGrid::N_Type N;         ///< count of grid nodes for el+pl
    main_param param;
    double rp, rk;
    Media media;
    Crap shit;
    Shaft();
    Shaft(double ra, double rb);
    void clean();

};
//===========================================
void initYongModulus(DataGrid& dg, const Media& m, const OneDGrid& g, double Ra);
void initYongModulus_cem(DataGrid &dg, const Media &m, const OneDGrid &g, double Ra);
void initCompSigma(DataGrid& dg, const Media& m, const OneDGrid& g, double Ra);
}
#endif //
