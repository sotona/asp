#include <string.h>
#include "../cm/cm.h"
#include "bur_r.h"
#include "plastic.h"
#include "../cm/g.h"
#ifndef NDEBUG
#include <iostream>
#include <iomanip>
#include <fstream>
#include <QDebug>
#endif

namespace plastic_zone_t{
void set_consts(const OneDGrid& g, Media& m, Crap &s, double S){
float t0=m.ssm*(1-m.aa)/2;

s.media.E = m.Elab*0.75*(1.+m.ko)/2.;                 // Elab -> mass E
//s.media.E = m.Em;
s.media.G = s.media.E/(2.*(1.+m.mm));     // mu

float tm=m.Elab*0.75*(1+m.ko)/2*(0.5-m.mm)/(0.5-0.2)*(2.5+m.ku)*m.aa0;//2.5
s.T=tm/(m.bb-1);
//s.ssr[g.n()-1]=-S*(1-s.aa)+t0;  ???
s.tmz=m.aa*S+t0;
s.gmz=s.tmz/s.media.G;}


/// @return x = {p-q, ssr, sst, u}
void calc_at_radius_z(double ra, double rz,const Media&m, Crap& s, double S, double *x){
double r;
double C1, C2, C3; //+
int n=4;
double nn=m.db_param.n;
double aa=m.db_param.a;
double n2a = nn+2.-2.*aa;
r=rz/ra; // rz/ra
C1=(nn+2.-2.*aa*pow(r,-nn))/n2a/(r*r);    //1
C2=(nn+2.-2.*aa*(nn+1.)*pow(r,-nn))/n2a/(r*r); //d-0.76
C3=(nn+2.)/n2a/r; //d1.39
double ss_c8=m.ssm*(1.0-m.db_param.b*pow(r, -nn));
double **a= new double*[4];
double b[4];
for (int i=0; i<4; i++)a[i]=new double[5];
a[0][0]=C1;                a[0][1]=1;     a[0][2]=0; a[0][3]=0; b[0]=-S;
a[1][0]=-C2;               a[1][1]=0;     a[1][2]=1; a[1][3]=0; b[1]=-S;
a[2][0]=-C3/(2*s.media.G)*
           ra;  a[2][1]=0;     a[2][2]=0; a[2][3]=1; b[2]=0;
a[3][0]=0;                 a[3][1]=-m.bb; a[3][2]=1; a[3][3]=0; b[3]=-ss_c8;

//#ifndef NDEBUG
//std::ofstream file("A8");
//for (int i=0; i<n; i++){
//for (int j=0; j<n+1; j++)    file << std::setprecision (3) << a[i][j]<<"   "; file <<std::endl;}
//file.close();
//#endif

plsu(a,b,x,4);
//if (cm::SoLE::Gaussian_elimination<double>(reinterpret_cast<double**>(a),x,4));
for (int i=0; i<n; i++)delete[]a[i]; delete[] a;
}


void calc_at_raduis(Region<DataGrid> &plr, const Media& m,const Crap& s, int i, bool flagrk, bool flagrp, double *x){
const unsigned xn=5;
double **a= new double*[xn]; for (unsigned j=0; j<xn; j++)a[j]=new double[xn];
double b[xn];
DataGrid& pl = plr.detail;
const OneDGrid::N_Type n=plr.grid.n()-1;
double Rz = plr.grid.rb();
a[0][0]=1.; a[0][1]=0.;   a[0][2]=0.; a[0][3]=-1./plr.grid.r(i);  a[0][4]=0.;
    b[0]=-pl.ssr[i]/plr.grid.r(i)+m.density*sin(s.omega);
if (flagrk==true)  {
a[1][0]=0.; a[1][1]=1.;   a[1][2]=0.; a[1][3]=0.;                  a[1][4]=0.;
    b[1]=s.ttk; } else {
a[1][0]=0.; a[1][1]=1.;   a[1][2]=0.; a[1][3]=0.;                  a[1][4]=-m.aa0;
    b[1]=-m.aa0*pl.u[i]/plr.grid.r(i) + m.aa0*2*pl.u[n]/Rz; }
a[2][0]=0.; a[2][1]=0;   a[2][2]=1; a[2][3]=1;                  a[2][4]=0.;
    b[2]=m.bb*pl.ssr[i];
a[3][0]=0.; a[3][1]=1;   a[3][2]=0; a[3][3]=0.;                  a[3][4]=-1;
    b[3]=pl.u[i]/plr.grid.r(i);//kt
if (flagrp==true)  {
a[4][0]=0.; a[4][1]=0.;   a[4][2]=1.; a[4][3]=0.;                  a[4][4]=0.;
    b[4]=m.ssm*s.ssgp; } else {
a[4][0]=.0; a[4][1]=s.T; a[4][2]=1.; a[4][3]=0.;                  a[4][4]=0.;
    b[4]=pl.ss_c[i];
}
plsu(a,b,x,xn);
for (unsigned i=0; i<xn; i++)delete[]a[i]; delete[] a;
}


//-------------------------------------------------------
// calculatin plastic zone without Rz values
bool calc(Shaft& sh){
bool flagrk=false;
bool flagrp=false;
double x[5];
Crap &s = sh.shit;
Media&m = sh.media;
OneDGrid &g = sh.plastic1.grid;
DataGrid &pl1 = sh.plastic1.detail;
// from rz to ra
for (int i=g.n()-1; i>=0; i--){
memset(x, 0, 5*sizeof(x[0]));
calc_at_raduis(sh.plastic1, m, s, i, flagrk, flagrp, x);

if ((pl1.thetta[i]>s.ttk) && (flagrk==false)) { flagrk=true;  s.rk=g.r(i);
    calc_at_raduis(sh.plastic1, m, s, i, flagrk, flagrp, x);
}
if ((pl1.sso[i]<=s.ssgp*m.ssm) && (flagrp==false)) { flagrp=true; s.rp=g.r(i);
    calc_at_raduis(sh.plastic1, m, s, i, flagrk, flagrp, x);}

pl1.drssr[i]=x[0]; pl1.thetta[i]=x[1]; pl1.sso[i]=x[2]; pl1.sst[i]=x[3]; pl1.dru[i]=x[4];
if (i>0){
pl1.u[i-1]   = pl1.u[i]   - g.dr()*pl1.dru[i];
pl1.ssr[i-1] = pl1.ssr[i] - g.dr()*pl1.drssr[i];}
pl1.eer[i] = pl1.dru[i];
pl1.eet[i] = pl1.u[i]/g.r(i);
}

return 1;}
}
