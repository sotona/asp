#include <float.h>
#include "string.h"
#include "general.h"
#include "bur_r.h"
#include "elastic.h"
#include <QDebug>


namespace elastic_zone{
using namespace cm::multidimensional_opt;
using namespace cm;
 /*<FDM. derivative by r. logarithmic deformations*/
void  inr_ro(const OneDGrid& g, const Media& m, DataGrid& res, double S){
  for (long i=0; i<g.n(); i++)  {
    res.ro[i] = g.r(i) + res.u[i];
    res.eet[i] = log((g.r(i) + res.u[i])/g.r(i));       // log = ln
    res.sst[i] = res.eet[i]*res.Er[i]-S+m.mm_p*(res.ssr[i]+S);
    res.eer[i] = (res.ssr[i] + S - m.mm_p * (res.sst[i] + S))/res.Er[i];
    res.drdro[i] = exp( res.eer[i] );
    res.dru[i] = res.drdro[i] - 1.0;
    res.u[i + 1] = res.u[i] + g.dr() * res.dru[i];
    res.drssr[i] = ( res.sst[i] - res.ssr[i] ) / res.ro[i] * res.drdro[i];
    res.ssr[i + 1] = res.ssr[i] + g.dr() * res.drssr[i];
    res.thetta[i] = res.eet[i]+res.eer[i];
    }}


void  inr_u(const OneDGrid& g, const Media& m, DataGrid& res, double S){
//    S = 0;
  for (long i = 0; i <g.n(); i++){
    res.eet[i] =  res.u[i] / g.r(i);
    res.sst[i] = res.eet[i] * res.Er[i] - S + m.mm_p * ( res.ssr[i] + S );
    res.eer[i] = ( res.ssr[i] + S - m.mm_p * ( res.sst[i] + S ) ) / res.Er[i];
    res.dru[i] = res.eer[i];
    res.u[i + 1] = res.u[i] + g.dr() * res.dru[i];
    res.drssr[i] = ( - ( res.ssr[i] - res.sst[i] ) ) / g.r(i);
    res.ssr[i + 1] = res.ssr[i] + g.dr() * res.drssr[i];
    res.thetta[i] = res.eer[i] + res.eet[i];
    }}


/// ssXl[i] - значения с предыдущего вычисления задачи ГД для различных r* ?
void  inr_u_cem(const OneDGrid& g, const Media& m, DataGrid& res,
       const double* ssrl,const double* sstl){
  for (long i = 0; i <g.n()-1; i++){
    res.eet[i] =  res.u[i] / g.r(i);
    res.sst[i] = res.eet[i] * res.Er[i] + sstl[i]  + m.mm_p * ( res.ssr[i] - ssrl[i]  );
    res.eer[i] = ( res.ssr[i] - ssrl[i] - m.mm_p * ( res.sst[i] - sstl[i]  ) ) / res.Er[i];
    res.dru[i] = res.eer[i];
    res.u[i + 1] = res.u[i] + g.dr() * res.dru[i];
    res.drssr[i] = ( - ( res.ssr[i] - res.sst[i] ) ) / g.r(i);
    res.ssr[i + 1] = res.ssr[i] + g.dr() * res.drssr[i];}}

inline double Rb_penalty(const DataGrid &d, double S){
return pow(d.ssr[d.n()-1]+S,2);}

/// for elastic region
double elastic_optfunc_2d(double_vector& v, void* p){
Shaft *sh = (Shaft*)p;
if (!sh) throw;
DataGrid& d = sh->elastic.detail;
OneDGrid& g = sh->elastic.grid;
Media& m = sh->media;
double S = sh->param.S;
d.u[0] = v[0];
d.ssr[0] = v[1];
inr_u( g, m, d,S);
ff = Rb_penalty(d,S) + pow( d.sst[0] - m.bb * d.ssr[0] + d.ss_c[0],2 );
return ff;}


double elastic_optfunc_cem(double_vector& v, void* p){
Shaft *sh = (Shaft*)p;
double *ssr = new double[sh->N+1], *sst = new double[sh->N+1];
OneDGrid::N_Type I=0;
for (OneDGrid::N_Type i=0; i<sh->plastic1.detail.n();i++, I++){
ssr[I]=sh->plastic1.detail.ssr[i];
sst[I]=sh->plastic1.detail.sst[i];}
for (OneDGrid::N_Type i=1; i<sh->elastic.detail.n();i++, I++){
ssr[I]=sh->elastic.detail.ssr[i];
sst[I]=sh->elastic.detail.sst[i];}

if (!sh) throw;
Region<DataGrid>& elc = sh->elastic_cem;
Media& m = sh->media;
DataGrid& d = sh->elastic_cem.detail;
d.u[0] = v[0];
d.ssr[0] = v[1];
inr_u_cem( elc.grid, m, elc.detail,ssr,sst);
double B = elc.detail.ssr[0];
ff = Rb_penalty(d,sh->param.S)+ B*B;

delete[] ssr;
delete[] sst;
return ff;}


double elastic_optfunc_nl(double_vector& v, void* p){
Shaft *sh = (Shaft*)p;
if (!sh) throw;
Media& m = sh->media;
DataGrid& d = sh->elastic.detail;
OneDGrid& g = sh->elastic.grid;
double S = sh->param.S;
d.u[0] = v[0];
d.ssr[0] = v[1];
inr_ro( g, m, d,S);
double rp=m.ssl_t, rc=m.ssl_c;
double psi = rp/rc;
double t = rc * ( pow(psi + (1-psi)*m.ko,0.5) - m.ko);
double tau_m = (d.ssr[0] -  d.sst[0])/2;
double sq = pow( rc*rc*psi - (1-psi)*(d.ssr[0]+d.sst[0])*rc,0.5)/2;
double B = tau_m - sq + t;
ff = Rb_penalty(d,S) + B*B;
return ff;}


double elastic_optfunc_nl2(double_vector& v, void* p){
Shaft *sh = (Shaft*)p;
if (!sh) throw;
DataGrid& d = sh->elastic.detail;
Media& m = sh->media;
double S = sh->param.S;
d.u[0] = v[0];
d.ssr[0] = v[1];
inr_ro( sh->elastic.grid, m, d,S);
double a = m.c[0], b = m.c[1], c = m.c[2];
double B = a + b*d.ssr[0] + c * d.ssr[0]*d.ssr[0] - d.sst[0];
ff = Rb_penalty(d,S) + B*B;
return ff;}


///  for elestic problem optimazation
double elastic_optfunc_1d(double_vector& x, void* p){
Shaft *sh = (Shaft*)p; if (!sh) throw; Region<DataGrid> & r = sh->elastic;
DataGrid& d = sh->elastic.detail;
  d.u[0] = x[0];  d.ssr[0] = - sh->param.P;
//  d.ssr[0] = -sh->param.S;
inr_u( r.grid, sh->media, d,sh->param.S);
double A = (d.ssr[r.grid.n()-1]+sh->param.S);
//double A1 = d.ssr[r.grid.n()-1];
//double B = (d.sst[0]-sh->param.S) - ((d.ssr[0]-sh->param.S)*sh->media.bb - d.ss_c[0]);
  ff = A*A;
return ff;}


//===============================================================
double calc(Shaft& sh, cm::double_vector &x, double accuracy,
                                    bool elprob, unsigned char strength){
Newton N(accuracy);
N.setIterations_maximum(1500);

foo f;
double ff;
double_vector opt_vec(2);
if (!elprob){
        switch (strength) {
        case strength_criterion::nl1:     f = elastic_optfunc_nl;       break;
        case strength_criterion::nl2:     f = elastic_optfunc_nl2;       break;
        case strength_criterion::ssr_sst: f = elastic_optfunc_2d;       break;
        default: f = elastic_optfunc_2d;     break;}
		N.setFunction(f, (void*)&sh);
		opt_vec=N.getOptimalValues(x);
		x=opt_vec;}
else {x.resize(1);  // elastic problem
      f=elastic_optfunc_1d;
      double ff0 = elastic_optfunc_1d(x, (void*)&sh);
	  double_vector v(1); v[0]=1.0;
      double a = -0.5*sh.param.Ra, b = 0.5*sh.param.Ra;
	  x[0]=unidimensional_opt::gold_section(a, b, f, (void*)&sh,x,v,accuracy);
      ff=elastic_optfunc_1d(x, (void*)&sh);}

ff = f(x,(void*)&sh);
return ff;}


double calc_cem(Shaft& sh, cm::double_vector &x, double accuracy){
Newton N(accuracy);
N.setIterations_maximum(1500);

foo f = elastic_optfunc_cem;
double ff;
double_vector opt_vec(2);
  N.setFunction(f, (void*)&sh);
  opt_vec=N.getOptimalValues(x);
  x=opt_vec;
ff = f(x,(void*)&sh);
return ff;}}

//====================
namespace plastic_zone{

void calculation(Shaft& sh){
DataGrid &d=sh.plastic2.detail;
const DataGrid &el=sh.elastic.detail;
OneDGrid &g=sh.plastic2.grid;
Media &m=sh.media;
const OneDGrid::N_Type n = d.n()-1;
double rz = g.rb();

bool rp=1, rk=1;

d.u[n]     = el.u[0];
d.eet[n]   = el.eet[0];
d.eer[n] = el.eer[0];
d.ssr[n] = el.ssr[0];

d.sst[n] = el.sst[0];

const double thetta_l = sh.shit.ttk;
const double thetta_z = el.dru[0] + el.u[0]/rz;
const double eetz = el.eet[0];
double omega = M_PI/2.;
double gamma = m.density;

d.thetta[n] = thetta_z;
if (thetta_z>thetta_l) d.dru[n] = - d.u[n]/rz + thetta_l;
else                   d.dru[n] = - d.u[n]/rz + thetta_z +
                       (1-m.c[1]-2*m.c[2]*d.ssr[n])*(d.u[n]/rz-d.eet[n]);

for (long i=n; i>0; i--){
d.drssr[i] = - (d.ssr[i]-d.sst[i])/g.r(i) + gamma*sin(omega);
d.u[i-1]   = d.u[i]   - g.dr()*d.dru[i];
d.ssr[i-1] = d.ssr[i] - g.dr()*d.drssr[i];
if (d.thetta[i] >= thetta_l-DBL_EPSILON) d.dru[i-1] = -d.u[i-1]/g.r(i-1) + thetta_l;
else                                     d.dru[i-1] = -d.u[i-1]/g.r(i-1) + thetta_z +
                            (1-m.c[1]-2*m.c[2]*d.ssr[i-1])*(d.u[i-1]/g.r(i-1)-eetz);
d.eer[i-1] = d.dru[i-1];
d.eet[i-1] =  d.u[i-1] / g.r(i-1);

d.thetta[i-1] = d.dru[i-1] + d.u[i-1]/g.r(i-1);
if (d.thetta[i-1]>thetta_l){
//    if (th==0) c0 = thetta_l*(d.ssr[i-1]+a), th=1;
    d.thetta[i-1] = thetta_l;
//    d.thetta[i-1] = c0/(d.ssr[i-1]+a);
    if (rk) {sh.rk=g.r(i-1); rk=0;}}
d.sso[i-1] = m.ssm - m.T*(d.thetta[i-1]-thetta_z);
if (d.sso[i-1] < 0)
    {d.sso[i-1] = 0;  if (rp) {sh.rp = g.r(i-1); rp=0;}}
d.sst[i-1]= - d.sso[i-1] + m.c[1]*d.ssr[i-1]+m.c[2]*d.ssr[i-1]*d.ssr[i-1];}
}

}
