#include <QCoreApplication>
#include <iostream>
#include "../../calculation.h"

void setDefaultPP(prob_param& pp){
    pp.ep_method=0;
    pp.burr=1;
    pp.el_numeric=0;
    pp.el_only=0;
    pp.strength_i=0;
    pp.eps=0.00001;}


void setDedaultCore(Core& c){
double ra = 2380, rb_ = 10;
double S = 19.8;
OneDGrid::N_Type Nrz = 11;
setDefaultPP(c.pp);
c.rz_range.init(ra, ra*rb_, Nrz);
c.shaft.param.Ra = ra;
c.shaft.param.Rb = ra*rb_;
c.shaft.param.S = S;

Media m;
m.Elab = 31700;
m.mm = 0.28;
m.kp = 1.8;
m.ssl_c = 72;
m.ko = 0.2;
m.density = 2.394;
m.ff=37.0*M_PI/180;
m.ff0=37.0*M_PI/180;
m.ku = 0.5;
m.kd = 2.0;
m.ke = 0.7;
m.set_consts();

c.shaft.media=m;

c.shaft.N = 1001;
plastic_zone_t::set_consts(c.rz_range,c.shaft.media,
        c.shaft.shit,c.shaft.param.S);
}


void print_pp(const prob_param& pp){
cout << "elastic region: " << ((pp.el_numeric)?"FDM+NLP":"analyt") << endl;
cout << "driling and blasting: " << pp.burr << endl;
cout << "epsilon" << pp.eps << endl;
}

using namespace std;

int main(int argc, char *argv[]){
QCoreApplication a(argc, argv);

Core c;
setDedaultCore(c);

cout << "calculating..." << endl;
calc_RP(c,c.pp);
    
return a.exec();}

