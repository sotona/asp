#-------------------------------------------------
#
# Project created by QtCreator 2013-12-09T18:53:18
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = test-old-p
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    ../../cm/unidimensional_opt.cpp \
    ../../cm/multidimensional_opt.cpp \
    ../../cm/g.cpp \
    ../../cm/cm.cpp \
    ../../eng/bur_r.cpp \
    ../../eng/elastic.cpp \
    ../../eng/general.cpp \
    ../../eng/plastic.cpp \
    ../../calculation.cpp

HEADERS += \
    ../../cm/unidimensional_opt.h \
    ../../cm/multidimensional_opt.h \
    ../../cm/g.h \
    ../../cm/cm.h \
    ../../eng/bur_r.h \
    ../../eng/elastic.h \
    ../../eng/general.h \
    ../../eng/plastic.h \
    ../../calculation.h
