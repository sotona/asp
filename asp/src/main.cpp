#include <QApplication>
#include <fstream>
#include "calculation.h"
#include "mainwindow.h"

int main(int argc, char *argv[]){

    QStringList paths = QCoreApplication::libraryPaths();
    paths.append(".");
    paths.append("imageformats");
    paths.append("platforms");
    QCoreApplication::setLibraryPaths(paths);

    QApplication a(argc, argv);

    MainWindow w;

    if (a.arguments().size()>1){
        QString param_filename = a.arguments().at(1);
        w.load_params(param_filename);}
    w.show();
    return a.exec();}
