#include <QtSvg/QSvgGenerator>
#include <QDesktopServices>
#include <QUrl>
#include <QImage>
#include <QClipboard>
#include <QSizePolicy>
#include <QSettings>

#include <QFileDialog>
#include <QMessageBox>

#include "../qwt/src/qwt_symbol.h"

#include <QFuture>
#include <QtConcurrent/QtConcurrent>

#include "html_out.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "shoring.h"

const QString ProgFullName = "Горное давление в одиночных горизонтальных выработках при нелинейной "\
        "функции разупрочнения";
const QString ProgName = "ГД-4";
const QString URLrepo = "https://bitbucket.org/sotona/asp";
const QString URLpubl = "https://bitbucket.org/sotona/asp/wiki/Home";
const QString URLmanual = "https://bitbucket.org/sotona/asp/src/18db5c56782902e7c053edc95142d291c42197e5"\
        "/manual.pdf?at=dev_shor.R";

// indexes for curves array of QwtPlotCurve
namespace ind{
unsigned char p=0;
unsigned char pob=1;
unsigned char rzra1=2;
unsigned char rzra2 =3;
unsigned char rkra=4;
unsigned char rpra=5;}

namespace plots{
const QString R_RA = "R/Ra";
const QString U_mm = "displacements U [mm]";
const QString Ssr_S = "radial stress [S units]";
const QString Sst_S = "circumferential stress [S units]";
const QString FoS = "Factor of safety";
const QString Ua_mm = "displacements Ua [mm]";
const QString P_kpa = "Pressure P [kPa]";}


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), ui(new Ui::MainWindow){
    ui->setupUi(this);
core = new Core();
plot.run=0;
using namespace plots;
plot.plot_staff[plot_ind::u] = new PlotStaff(ui->plot_u,     R_RA, U_mm);
plot.plot_staff[plot_ind::sst] = new PlotStaff(ui->plot_sst, R_RA, Ssr_S);
plot.plot_staff[plot_ind::ssr] = new PlotStaff(ui->plot_ssr, R_RA, Sst_S);
plot.plot_staff[plot_ind::fos] = new PlotStaff(ui->plot_sf,  R_RA, FoS);
plot.plot_staff[plot_ind::p] = new PlotStaff(ui->plot_P,     Ua_mm, P_kpa);
plot.plot_staff[plot_ind::r] = new PlotStaff(ui->plot_R,     Ua_mm, ".");
plot.plot_staff[plot_ind::k] = new PlotStaff(ui->plot_k,     R_RA,  "k");

init_plot_curves();
init_plot_markers();

QString val = QString::number(ui->SpinBox_rze->value()*ui->SpinBox_ra->value());
ui->label_rzval->setText(val);
        val = QString::number(ui->SpinBox_rb_->value()*ui->SpinBox_ra->value());
ui->label_rb_mm->setText(val);

connect(ui->spinBox_N,SIGNAL(valueChanged(int)),this,SLOT(update_el_step()));
connect(ui->SpinBox_ra,SIGNAL(valueChanged(double)),this,SLOT(update_el_step()));
connect(ui->SpinBox_rb_,SIGNAL(valueChanged(double)),this,SLOT(update_el_step()));
connect(ui->SpinBox_rze,SIGNAL(valueChanged(double)),this,SLOT(update_el_step()));
connect(&parallel.watcher_e, SIGNAL(finished()), this, SLOT(display_e_results()));
connect(&parallel.watcher_ep, SIGNAL(finished()), this, SLOT(display_ep_results()));
connect(&parallel.watcher_rp, SIGNAL(finished()), this, SLOT(display_rp_results()));
connect(&parallel.watcher_cem, SIGNAL(finished()), this, SLOT(display_cem_results()));
// connect params at simple-parms-tab with one's at all-params-tab1
connect(ui->SpinBox_ra, SIGNAL(valueChanged(double)), this, SLOT(ratoWidth(double)));
connect(ui->spinBox_width, SIGNAL(valueChanged(double)), this, SLOT(widthtoRa(double)));

on_comboBox_calc_method_currentIndexChanged(ui->comboBox_calc_method->currentIndex());
on_spinBox_eps_n_valueChanged(ui->spinBox_eps_n->value());
on_SpinBox_rze_valueChanged(ui->SpinBox_rze->value());
on_SpinBox_rb__valueChanged(ui->SpinBox_rb_->value());
update_el_step();

QString fname = Files::shorings;
QFile f(fname);
if (!f.exists()) fname="../"+Files::shorings;
core->shar = load_shoring_data(fname);
if (core->shar==NULL || core->shar->size()==0){
    ui->textBrowser_shoring->setText(err_no_shor_file.arg(Files::shorings));
    QMessageBox::warning(this,this->windowTitle(),err_no_shor_file.arg(Files::shorings));}
core->situable_shar = NULL;
RP_calc_n=0;}


MainWindow::~MainWindow(){delete ui;}


void MainWindow::init_plot_curves(){
for (short i=0; i<2; i++){
plot.curve_u[i] = new QwtPlotCurve(QString("displacements #")+QString::number(i+1));
plot.curve_ssr[i] = new QwtPlotCurve(QString("ssr/s #")+QString::number(i+1));
plot.curve_sst[i] = new QwtPlotCurve(QString("sst/s #")+QString::number(i+1));
plot.curve_fc[i] = new QwtPlotCurve(QString("res k #")+QString::number(i+1));
plot.curve_u[i]->setRenderHint  (QwtPlotItem::RenderAntialiased);
plot.curve_ssr[i]->setRenderHint(QwtPlotItem::RenderAntialiased);
plot.curve_sst[i]->setRenderHint(QwtPlotItem::RenderAntialiased);
plot.curve_fc[i]->setRenderHint (QwtPlotItem::RenderAntialiased);}

plot.curve_u[0]->setPen  (QPen(QColor(Qt::darkGreen),2));
plot.curve_ssr[0]->setPen(QPen(QColor(Qt::darkGreen),2));
plot.curve_sst[0]->setPen(QPen(QColor(Qt::darkGreen),2));
plot.curve_fc[0]->setPen (QPen(QColor(Qt::darkGreen)));
plot.curve_u[1]->setPen  (QPen(QColor(Qt::red)));
plot.curve_ssr[1]->setPen(QPen(QColor(Qt::red)));
plot.curve_sst[1]->setPen(QPen(QColor(Qt::red)));
plot.curve_fc[1]->setPen (QPen(QColor(Qt::red)));

for (unsigned char i=0 ;i<rzg_N; i++){
plot.curve[i]= new QwtPlotCurve(rz_labels[i]);
plot.curve[i]->setRenderHint(QwtPlotItem::RenderAntialiased);}

plot.curve[ind::p]->setPen    (QPen(QColor(Qt::red),2));
plot.curve[ind::rzra1]->setPen(QPen(QColor(Qt::gray)));
plot.curve[ind::rzra2]->setPen(QPen(QColor(Qt::green)));
plot.curve[ind::pob]->setPen  (QPen(QColor(Qt::blue),2));
plot.curve[ind::rpra]->setPen (QPen(QColor(Qt::yellow)));
plot.curve[ind::rkra]->setPen (QPen(QColor(Qt::blue)));}


void MainWindow::init_plot_markers(){
plot.markers.Pn =   create_line_marker("shoring P ",  QPen(Qt::darkGray, 1, Qt::DashDotLine));
plot.markers.uw =   create_line_marker("shoring uw ", QPen(Qt::darkGray, 1, Qt::DashDotLine), Qt::Vertical, QwtPlotMarker::VLine);
plot.markers.p_max= create_line_marker("P_max",       QPen(Qt::darkGray, 1, Qt::DashDotLine));
plot.markers.p_max->setLabelAlignment(Qt::AlignRight | Qt::AlignTop);
plot.markers.p_min= create_line_marker("P_min",       QPen(Qt::darkGray, 1, Qt::DashDotLine));
plot.markers.p_min->setLabelAlignment(Qt::AlignRight | Qt::AlignBottom);
plot.markers.rz1 =  create_line_marker("r*", QPen(Qt::black, 1, Qt::DashDotLine),Qt::Vertical, QwtPlotMarker::VLine);
plot.markers.rz2 =  create_line_marker("r*", QPen(Qt::black, 1, Qt::DashDotLine),Qt::Vertical, QwtPlotMarker::VLine);
plot.markers.rz3 =  create_line_marker("r*", QPen(Qt::black, 1, Qt::DashDotLine),Qt::Vertical, QwtPlotMarker::VLine);

plot.markers.last_p =  create_dot_marker(Qt::black);
plot.markers.p_uw =    create_dot_marker(Qt::black);
plot.markers.first_p = create_dot_marker(Qt::black);
plot.markers.pxp     = create_dot_marker(Qt::red);
plot.markers.ssr0 = create_dot_marker(Qt::black);
plot.markers.sst0 = create_dot_marker(Qt::black);
plot.markers.u0   = create_dot_marker(Qt::black);

plot.markers.plotMarks.elMarks = new QwtPlotMarker**[plot.markers.plotMarks.Nm];
for (unsigned i=0; i<plot.markers.plotMarks.Nm; i++){
plot.markers.plotMarks.elMarks[i] = new QwtPlotMarker*[plot.markers.plotMarks.Nmm];
for (unsigned j=0; j<plot.markers.plotMarks.Nmm; j++)
plot.markers.plotMarks.elMarks[i][j] = create_dot_marker(Qt::black);;}
}


//------------------------------------------------ init core
void MainWindow::init_core(){
get_params();
core->shaft.media.set_consts();
plastic_zone_t::set_consts(core->rz_range,core->shaft.media, core->shaft.shit,core->shaft.param.S); // need for initial values used by NLP function
core->shaft.clean();
core->aval.clear();}


//------------------------------------------------ get parameters
void MainWindow::get_params(){
core->pp.ep_method = ui->comboBox_calc_method->currentIndex()+1;
core->pp.burr = ui->checkBox_burr->isChecked();
core->pp.el_numeric = !ui->comboBox_el_method->currentIndex();
//core->pp.pl = (core->pp.ep_method==2)?0:1
core->pp.el_only = ui->checkBox_elproblem->isChecked();
core->pp.strength_i = ui->comboBox_strength->currentIndex();
core->pp.calc_ff=ui->checkBox_calcff->isChecked();
core->pp.eps = 1.0/pow(10,ui->spinBox_eps_n->value());

main_param param;
/*General params*/
param.w = ui->spinBox_width->value()*1000;
param.Ra = ui->SpinBox_ra->value();
param.Rb = ui->SpinBox_rb_->value()*param.Ra;
core->Rz = ui->SpinBox_rze->value()*param.Ra;
param.Rc=ui->SpinBox_rcem->value()+param.Ra;
//param.Rz = ui->SpinBox_rze->value()*param.Ra;

core->shaft.media.kp = ui->SpinBox_kp->value(); // коэф-т перегрузки
core->shaft.media.c[0] = ui->SpinBox_c0->value();
core->shaft.media.c[1] = ui->SpinBox_c1->value();
core->shaft.media.c[2] = ui->SpinBox_c2->value();
param.S = ui->SpinBox_S->value();
param.S = core->shaft.media.kp*param.S;
param.P = ui->SpinBox_P->value();  // for elastic problem\region
core->shaft.media.Elab = ui->SpinBox_Elab->value();
core->shaft.media.mm = ui->SpinBox_mu->value();

core->shaft.N = ui->spinBox_N->value();

// some Media params
core->shaft.media.density=ui->SpinBox_gg->value()*9.81e-6;//s.gg = плотность попрод
core->shaft.shit.omega = ui->SpinBox_thetta->value()*M_PI/180;
core->shaft.shit.Br=ui->SpinBox_Br->value();

core->shaft.media.ff1=ui->SpinBox_ff0->value()*M_PI/180;// угол внутреннего трения для дилатансионного соотношения
core->shaft.media.ff0=ui->SpinBox_ff1->value()*M_PI/180;// угол внутреннего трения для условия прочности
core->shaft.media.ssl_c = ui->SpinBox_ssl_c->value();
core->shaft.media.ssl_t = ui->SpinBox_ssl_t->value();

core->shaft.media.kd=  ui->SpinBox_ke->value();// длит проч-ть
core->shaft.media.ko=ui->SpinBox_ko->value();
core->shaft.media.ku = ui->SpinBox_ku->value();
core->shaft.shit.ksr=ui->SpinBox_km->value();// коэф-т условия работы крепи

core->shaft.shit.ttk=ui->SpinBox_ttk->value();// пред. занчение относ-го объёмного расширения
core->shaft.shit.upr=ui->SpinBox_upr->value()*10;// пред. перемещ.

core->rz_range.init(ui->SpinBox_rzmin->value()*param.Ra,
    ui->SpinBox_rzmax->value()*param.Ra, ui->spinBox_rzn->value());

core->shaft.shit.kd=ui->SpinBox_kd->value();// к-т динамичности
core->shaft.shit.ssgp=ui->SpinBox_ssgp->value()/100.0;// пред ост проч
core->shaft.shit.uw=ui->SpinBox_uw->value()*10;// пред. перем. ...
core->shaft.shit.Pn=ui->SpinBox_Pn->value();// несущая способ-ть крепи

core->shaft.param = param;

if (core->pp.calc_ff){ calc_ff(core->shaft.media, core->shaft.param.S);
ui->SpinBox_ff1->setValue(core->shaft.media.ff0/M_PI*180);
ui->SpinBox_ff0->setValue(core->shaft.media.ff1/M_PI*180);}
}

//------------------------------------------------ draw el and pl plots
void MainWindow::draw_ep_graphics(const Region<DataGrid>* pl, const Region<DataGrid>* el){
const long max_n = 200;
const long N = core->shaft.elastic.grid.n()-1;
long n = N, d = 1;
if (N > max_n){
d = N / max_n;
n = max_n;}

plot.run=0;
/*detach*/
{plot.curve_u[plot.run]->detach();
plot.curve_ssr[plot.run]->detach();
plot.curve_sst[plot.run]->detach();
plot.curve_fc[plot.run]->detach();
for (unsigned i=0; i<plot.markers.plotMarks.Nm; i++)
for (unsigned j=0; j<plot.markers.plotMarks.Nm; j++)
plot.markers.plotMarks.elMarks[i][j]->detach();}

double S = core->shaft.param.S;

// векторы данных для графиков
QVector<double> uval(n);
QVector<double> ssrval(n);
QVector<double> sstval(n);
QVector<double> reskval(n);
QVector<double> rval(n);// ось абсцисс для упругих графиков

long i=0; long I=0;

if (pl!=NULL)
for (; i<pl->grid.n(); I+=1){
rval[i] = pl->grid.r(I)/core->rz_range.ra(); // r[i]/Ra
uval[i] = pl->detail.u[I];
ssrval[i] = pl->detail.ssr[I]/S;
sstval[i] = pl->detail.sst[I]/S;
reskval[i] = pl->detail.res_k[I];
i++;}

I=0;
for (; i<max_n; I+=d){
rval[i] = el->grid.r(I)/core->rz_range.ra(); // r[i]/Ra
uval[i] = el->detail.u[I];
ssrval[i] = el->detail.ssr[I]/S;
sstval[i] = el->detail.sst[I]/S;
reskval[i] = el->detail.res_k[I];
i++;}

/*set and attach*/
{plot.curve_u[plot.run]->setSamples(rval, uval);
plot.curve_ssr[plot.run]->setSamples(rval, ssrval);
plot.curve_sst[plot.run]->setSamples(rval, sstval);
plot.curve_fc[plot.run]->setSamples(rval, reskval);
plot.curve_u[plot.run]->attach(ui->plot_u);
plot.curve_ssr[plot.run]->attach(ui->plot_ssr);
plot.curve_sst[plot.run]->attach(ui->plot_sst);
plot.curve_fc[plot.run]->attach(ui->plot_sf);}


{QPointF ppp(rval[0], ssrval[0]);
plot.markers.ssr0->setValue(ppp);
plot.markers.ssr0->setLabel(QwtText(QString::number(ssrval[0])));
plot.markers.ssr0->attach( ui->plot_ssr);
plot.markers.sst0->setValue(QPointF(rval[0], sstval[0]));
plot.markers.sst0->setLabel(QwtText(QString::number(sstval[0])));
plot.markers.sst0->attach( ui->plot_sst);
plot.markers.u0->setValue(QPointF(rval[0], uval[0]));
plot.markers.u0->setLabel(QwtText(QString::number(uval[0])));
plot.markers.u0->attach( ui->plot_u);}

const OneDGrid& g = el->grid;

add_marker(plot.markers.plotMarks.elMarks[plot.markers.plotMarks.umark],plot.markers.plotMarks.Nmm,
            el->detail.u,1,ui->plot_u,g,core->shaft.param.Ra);
add_marker(plot.markers.plotMarks.elMarks[plot.markers.plotMarks.ssrmark],
            plot.markers.plotMarks.Nmm,el->detail.ssr,S,ui->plot_ssr,g,core->shaft.param.Ra);
add_marker(plot.markers.plotMarks.elMarks[plot.markers.plotMarks.sstmark],plot.markers.plotMarks.Nmm,
            el->detail.sst,S,ui->plot_sst,g,core->shaft.param.Ra);


for(unsigned j=0;j<plot.markers.plotMarks.Nmm;j++)
    plot.markers.plotMarks.elMarks[plot.markers.plotMarks.ssrmark][j]->attach(ui->plot_ssr);
for(unsigned j=0;j<plot.markers.plotMarks.Nmm;j++)
    plot.markers.plotMarks.elMarks[plot.markers.plotMarks.sstmark][j]->attach(ui->plot_sst);

const double Rz = core->Rz/core->shaft.param.Ra;

plot.markers.rz1->setXValue(Rz);
plot.markers.rz2->setXValue(Rz);
plot.markers.rz3->setXValue(Rz);
plot.markers.rz1->setLabel(QwtText("r* = " + QString::number(Rz)));
plot.markers.rz2->setLabel(QwtText("r* = " + QString::number(Rz)));
plot.markers.rz3->setLabel(QwtText("r* = " + QString::number(Rz)));
plot.markers.rz1->attach( ui->plot_u);
plot.markers.rz2->attach( ui->plot_ssr);
plot.markers.rz3->attach( ui->plot_sst);

ui->plot_u->replot();
ui->plot_ssr->replot();
ui->plot_sst->replot();
ui->plot_sf->replot();}


//-------------------------------------------------- draw graphics
void MainWindow::draw_rp_graphics(){
plot.markers.last_p->detach();
plot.markers.first_p->detach();
plot.markers.p_uw->detach();
plot.markers.pxp->detach(); plot.markers.Pn->detach();
plot.markers.p_max->detach(); plot.markers.p_min->detach();
plot.markers.uw->detach();
plot.markers.ssr0->detach();plot.markers.sst0->detach();plot.markers.u0->detach();

for (unsigned char i=0; i<rzg_N; i++)plot.curve[i]->detach();

// synamic Rz curves
QVector<double> ua(core->aval.n);// ось абсцисс для изменяемого rz
QVector<double> p(core->aval.n);
QVector<double> rzra_1k(core->aval.n);
QVector<double> rzra(core->aval.n);
QVector<double> pob(core->aval.n);
QVector<double> rpra(core->aval.n);
QVector<double> rkra(core->aval.n);

// для разных R*
for (unsigned i=0; i<core->aval.n; i++){
ua[i] = fabs(core->aval.ua[i]);
p[i] =       core->aval.p[i]*1000; //MPa -> kPa
rzra[i] =    core->aval.rzra[i];
rzra_1k[i] = core->aval.rzra[i]*1000;  // для наглядности на граф. P
pob[i] =     core->aval.pob[i]*1000;//MPa -> kPa
rpra[i] =    core->aval.rpra[i];
rkra[i] =    core->aval.rkra[i];}

plot.curve[ind::p]->setSamples(ua, p);
plot.curve[ind::rzra1]->setSamples(ua, rzra_1k);
plot.curve[ind::rzra2]->setSamples(ua, rzra);
plot.curve[ind::pob]->setSamples(ua, pob);
plot.curve[ind::rpra]->setSamples(ua, rpra);
plot.curve[ind::rkra]->setSamples(ua, rkra);

plot.curve[ind::p]->attach(ui->plot_P);
plot.curve[ind::rzra1]->attach(ui->plot_P);
plot.curve[ind::rzra2]->attach(ui->plot_R);
plot.curve[ind::pob]->attach(ui->plot_P);
plot.curve[ind::rpra]->attach(ui->plot_R);
plot.curve[ind::rkra]->attach(ui->plot_R);

if (core->aval.pp_i!=0){// intersection of P and Pob
double xx;
double x=(ua[(int)ceil(core->aval.pp_i)]-ua[(int)floor(core->aval.pp_i)]) ;
x*=modf(core->aval.pp_i, &xx);
x+=ua[(int)floor(core->aval.pp_i)];
plot.markers.pxp->setValue( QPointF(x, core->aval.pp_val*1000) );
plot.markers.pxp->setLabel(QwtText(QString::number(core->aval.pp_val*1000)));
plot.markers.pxp->attach( ui->plot_P );}

double shor_u = core->shaft.shit.uw;
if (-core->aval.ua[core->aval.n-1] >= shor_u && -core->aval.ua[0] <= shor_u && core->aval.n>1){
OneDGrid::N_Type i=core->aval.n-1;
for (;-core->aval.ua[i] < shor_u || -core->aval.ua[i-1] > shor_u;) i--;
double p = (core->aval.pob[i] + core->aval.pob[i-1])/2;
plot.markers.p_uw->setValue( QPointF(shor_u, p*1000) );
plot.markers.p_uw->setLabel(QwtText(QString::number(p*1000)));
plot.markers.p_uw->attach( ui->plot_P );}

plot.markers.last_p->setValue( QPointF(ua[core->aval.n-1], core->aval.p[core->aval.n-1]*1000) );
plot.markers.last_p->setLabel(QwtText(QString::number(core->aval.p[core->aval.n-1]*1000)));
plot.markers.last_p->attach( ui->plot_P );

plot.markers.first_p->setValue( QPointF(ua[0], core->aval.p[0]*1000) );
plot.markers.first_p->setLabel(QwtText(QString::number(core->aval.p[0]*1000)));
plot.markers.first_p->attach( ui->plot_P );

plot.markers.Pn->setYValue(core->shaft.shit.Pn /** 1000*/);
plot.markers.Pn->attach(ui->plot_P);

if (plot.show_pminmax){
plot.markers.p_max->setLabel(QwtText("max = "+QString::number(core->Pmax*1000.0)));
plot.markers.p_max->setYValue(core->Pmax*1000.0); plot.markers.p_max->attach(ui->plot_P);
plot.markers.p_min->setLabel(QwtText("min = "+QString::number(core->Pmin*1000.0)));
plot.markers.p_min->setYValue(core->Pmin*1000.0); plot.markers.p_min->attach(ui->plot_P);}

plot.markers.uw->setXValue(core->shaft.shit.uw);
plot.markers.uw->attach(ui->plot_P);

ui->plot_P->setAxisAutoScale(0,1);
ui->plot_P->setAxisAutoScale(1,1);
ui->plot_P->updateAxes();
ui->plot_R->updateAxes();
ui->plot_P->replot();
ui->plot_R->replot();}


void MainWindow::draw_elastic_list_graphics(vector<Region<DataGrid> > &reg){
const long max_n = (500>reg[0].grid.n()-1)?reg[0].grid.n()-1:500;
const long N = reg[0].grid.n()-1;
long n = N, d = 1;
if (N > max_n){
d = N / max_n;
n = max_n;}

plot.run=0;
/*detach*/
{plot.curve_u[plot.run]->detach();
plot.curve_ssr[plot.run]->detach();
plot.curve_sst[plot.run]->detach();
plot.curve_fc[plot.run]->detach();
//curve_k[plot.run]->detach();
}

for (unsigned i=0; i<plot.markers.plotMarks.Nm; i++)
for (unsigned j=0; j<plot.markers.plotMarks.Nm; j++)
plot.markers.plotMarks.elMarks[i][j]->detach();


QVector<double> uval(n),kval(n), ssrval(n), sstval(n), reskval(n);
QVector<double> rval(n);

long i=0;
for (long I=0; i<max_n; I+=d){
rval[i] = reg.at(0).grid.r(I)/core->shaft.param.Ra; // r[i]/Ra
qDebug()<<rval[i];
uval[i] = reg[0].detail.u[I];
ssrval[i] = reg[0].detail.ssr[I];
sstval[i] = reg[0].detail.sst[I];
reskval[i] = reg[0].detail.res_k[I];
i++;}
qDebug()<<"__________________________________";
double min=10;
int ki=-1;
double delta_zz=255/reg.size();
double zz=0;
for (unsigned vi=0; vi<reg.size(); vi++){
long i=0;
for (long I=0; i<max_n; I+=d){
float ttm=(-reg[vi].detail.sst[I]+reg[vi].detail.ssr[I])/2.;
float ssc=(reg[vi].detail.sst[I]+reg[vi].detail.ssr[I])/2.;

float t0=reg[vi].detail.ss_c[I]*(1-core->shaft.media.aa)/2.;
kval[i]=fabs((-core->shaft.media.aa*ssc+t0)/ttm);
if (kval[i]<min){min=kval[i];ki=vi;}
qDebug()<<kval[i];
i++;
}

QwtPlotCurve* curve_k=new QwtPlotCurve(QString::number(vi+1));
curve_k->setRenderHint(QwtPlotItem::RenderAntialiased);
curve_k->setPen(QPen(QColor(fabs(zz-255.*0.3)*2,255.*1.0-fabs(zz-255*.5)*2,255.*1.0-zz),1));
curve_k->setSamples(rval,kval);
curve_k->attach(ui->plot_k);
zz+=delta_zz;
}

qDebug()<<"k("<<ki<<")="<<min;

/*set and attach*/
{plot.curve_u[plot.run]->setSamples(rval, uval);
plot.curve_ssr[plot.run]->setSamples(rval, ssrval);
plot.curve_sst[plot.run]->setSamples(rval, sstval);
plot.curve_fc[plot.run]->setSamples(rval, reskval);

plot.curve_u[plot.run]->attach(ui->plot_u);
plot.curve_ssr[plot.run]->attach(ui->plot_ssr);
plot.curve_sst[plot.run]->attach(ui->plot_sst);
plot.curve_fc[plot.run]->attach(ui->plot_sf);
}

// Р±РѕР·РЅР°С‡РёРј С‚РѕС‡РєСѓ РІ РЅР°С‡Р°Р»Рµ РіСЂР°С„РёРєР°
{QPointF ppp(rval[0], ssrval[0]);
plot.markers.ssr0->setValue(ppp);
plot.markers.ssr0->setLabel(QwtText(QString::number(ssrval[0])));
plot.markers.ssr0->attach( ui->plot_ssr);
plot.markers.sst0->setValue(QPointF(rval[0], sstval[0]));
plot.markers.sst0->setLabel(QwtText(QString::number(sstval[0])));
plot.markers.sst0->attach( ui->plot_sst);
plot.markers.u0->setValue(QPointF(rval[0], uval[0]));
plot.markers.u0->setLabel(QwtText(QString::number(uval[0])));
plot.markers.u0->attach( ui->plot_u);
}

//const OneDGrid& g = reg[0].grid;

//add_marker(plot.markers.plotMarks.elMarks[plot.markers.plotMarks.umark],plot.markers.plotMarks.Nmm,
//            reg[0].detail.u,1,ui->plot_u,g,core->shaft.param.Ra);
//add_marker(plot.markers.plotMarks.elMarks[plot.markers.plotMarks.ssrmark],
//            plot.markers.plotMarks.Nmm,reg[0]->detail.ssr,S,ui->plot_ssr,g,core->shaft.param.Ra);
//add_marker(plot.markers.plotMarks.elMarks[plot.markers.plotMarks.sstmark],plot.markers.plotMarks.Nmm,
//            reg[0]->detail.sst,S,ui->plot_sst,g,core->shaft.param.Ra);


//for(unsigned j=0;j<plot.markers.plotMarks.Nmm;j++)
//    plot.markers.plotMarks.elMarks[plot.markers.plotMarks.ssrmark][j]->attach(ui->plot_ssr);
//for(unsigned j=0;j<plot.markers.plotMarks.Nmm;j++)
//    plot.markers.plotMarks.elMarks[plot.markers.plotMarks.sstmark][j]->attach(ui->plot_sst);

ui->plot_u->replot();
ui->plot_ssr->replot();
ui->plot_sst->replot();
ui->plot_sf->replot();
ui->plot_k->replot();

}


//----------------------------------------------------------- save garphics
void MainWindow::save_graphics(){
QPixmap qPix0 = ui->plot_u->grab();   qPix0.save("u_chart.png",   "png");
QPixmap qPix1 = ui->plot_ssr->grab(); qPix1.save("ssr_chart.png", "png");
QPixmap qPix2 = ui->plot_sst->grab(); qPix2.save("sst_chart.png", "png");
QPixmap qPix3 = ui->plot_sf->grab();  qPix3.save("resk_chart.png","png");
QPixmap qPix4 = ui->plot_P->grab();   qPix4.save("2_chart.png",   "png");
QPixmap qPix5 = ui->plot_R->grab();   qPix5.save("3_chart.png",   "png");}


void MainWindow::updateTable_rad(const Region<DataGrid> &pl){
int row=ui->tw_radius->rowCount();
ui->tw_radius->setRowCount(row+1);
const double S = core->shaft.param.S, Ra = core->shaft.param.Ra;
const int n = pl.detail.n()-1;
insert_to_table(ui->tw_radius,  pl.grid.rb()/Ra, row, 0);
insert_to_table(ui->tw_radius,  pl.detail.u[0], row, 1);
insert_to_table(ui->tw_radius,  pl.detail.u[n], row, 2);
insert_to_table(ui->tw_radius,  pl.detail.ssr[0]/S, row, 3);
insert_to_table(ui->tw_radius,  pl.detail.ssr[n]/S, row, 4);
insert_to_table(ui->tw_radius,  pl.detail.sst[0]/S, row, 5);
insert_to_table(ui->tw_radius,  pl.detail.sst[n]/S, row, 6);}


void MainWindow::updateTable_RP(){
int row=ui->tw_RP->rowCount();
ui->tw_RP->insertRow(row);
const RPdata &av = core->aval;
double xx;
double ua_x=(av.ua[(int)ceil(core->aval.pp_i)]-av.ua[(int)floor(core->aval.pp_i)]) ;
ua_x*=modf(core->aval.pp_i, &xx);
ua_x+=av.ua[(int)floor(core->aval.pp_i)];
insert_to_table(ui->tw_RP,  av.ua[0],   row, 0);
insert_to_table(ui->tw_RP,  av.p[0],    row, 1);
insert_to_table(ui->tw_RP,  av.pp_val,  row, 2);
insert_to_table(ui->tw_RP,  ua_x,       row, 3);
insert_to_table(ui->tw_RP,  core->Pmax, row, 4);
insert_to_table(ui->tw_RP,  core->Pmin, row, 5);}


void MainWindow::show_shorings(){
int row=ui->tableWidget_shoring->rowCount();
ui->tableWidget_shoring->insertRow(row);
insert_to_table(ui->tableWidget_shoring,  RP_calc_n+1, row, 0, 0);
for (size_t i=0; i<core->situable_shar->size(); i++){
if (i!=0) {row++; ui->tableWidget_shoring->insertRow(row);}
shoring sh = core->situable_shar->at(i);
insert_to_table(ui->tableWidget_shoring, sh.name, row, 1);
insert_to_table(ui->tableWidget_shoring, QString::number(sh.w_min)+"-"+QString::number(sh.w_max), row, 2);
insert_to_table(ui->tableWidget_shoring, sh.P, row, 3, 2);
insert_to_table(ui->tableWidget_shoring, sh.density, row, 4, 2);
insert_to_table(ui->tableWidget_shoring, 1/sh.density, row, 5, 2);
}
RP_calc_n++;}


void MainWindow::on_SpinBox_rze_valueChanged(double arg1){
    QString val = QString::number(arg1*ui->SpinBox_ra->value())+" мм";
    ui->label_rzval->setText(val);}
void MainWindow::on_SpinBox_rb__valueChanged(double arg1){
    QString val = QString::number(arg1*ui->SpinBox_ra->value())+" мм";
    ui->label_rb_mm->setText(val);}


void MainWindow::on_SpinBox_rcem_valueChanged(double arg1){
//ui->SpinBox_rze->setValue((ui->SpinBox_ra->value()+arg1)/ui->SpinBox_ra->value());
}


void MainWindow::update_el_step(){
main_param& p = core->shaft.param;
p.Ra = ui->SpinBox_ra->value();
p.Rb = p.Ra*ui->SpinBox_rb_->value();
core->shaft.N =  ui->spinBox_N->value();
double d = (p.Rb - p.Ra)/core->shaft.N;
ui->label_step->setText(QString::number(d)+" мм");}


//------------------------------------------------- calc elastic
void MainWindow::on_button_calc_e_clicked(){
setCalwaitState();
ui->button_calc_e->setDisabled(1);
init_core();
core->pp.ep_method=0;
parallel.future_e = QtConcurrent::run(calc_elastic, &core->shaft, core->pp, core->Rz);
parallel.watcher_e.setFuture(parallel.future_e); this->setEnabled(1);
calc_elastic_b(core->shaft,core->pp.burr);}


//----------------------------------------------- calc elastic-plastic
void MainWindow::on_button_calc_ep_clicked(){
setCalwaitState();
init_core();
Shaft &sh = this->core->shaft;
Region<DataGrid> *pl;
double f=-1;
if (core->pp.ep_method==2) {calc_ep_d(sh, this->core->pp, core->Rz,f); pl = &sh.plastic2;}
else                       {calc_ep(sh, this->core->pp, core->Rz,f); pl = &sh.plastic1;}
draw_ep_graphics(pl, &sh.elastic);
ui->SpinBox_ff_2->setValue(f);
display_ep_results();}


//-----------------------------------------------
void MainWindow::on_button_calc_rp_clicked(){
setCalwaitState();
ui->label_shoring_c_2->setText("<span style='font-size:12pt;'>идут вычисления...</span>");
init_core();
if (core->pp.ep_method==2){
core->pp.el_numeric=1;
core->pp.el_only=0;
core->pp.burr=0;}
parallel.future_rp = QtConcurrent::run(calc_rp, core, core->pp);
parallel.watcher_rp.setFuture(parallel.future_rp); this->setEnabled(1);}


//----------------------------------------------- calc cementation
void MainWindow::on_button_calc_ep_cem_clicked(){
setCalwaitState();
ui->comboBox_calc_method->setCurrentIndex(0);  // [T]
ui->comboBox_el_method->setCurrentIndex(0);    // calc elastic zone with nlp
ui->comboBox_strength->setCurrentIndex(0);     // use linear strength criterion
init_core();

vector<double> rzs;
rzs.push_back(this->core->Rz/this->core->shaft.param.Ra);

Shaft &sh = this->core->shaft;

vector <Region<DataGrid> > cem_vec;

for (unsigned rzi=0; rzi<rzs.size(); rzi++){
init_core();
calc_ep_cem(&sh, this->core->pp, rzs[rzi]*this->core->shaft.param.Ra);
cem_vec.push_back(core->shaft.elastic_cem);}
draw_elastic_list_graphics(cem_vec);
display_cem_results();
//future_cem = QtConcurrent::run(calc_ep_cem, &sh, core->pp, sh.param.Rz);
//watcher_cem.setFuture(future_cem); this->setEnabled(1);
}


void MainWindow::display_e_results(){
ui->button_calc_e->setEnabled(1);
core->f = parallel.watcher_e.result();
ui->SpinBox_ff_2->setValue(core->f);
//calc_elastic_b(core->shaft, ui->SpinBox_rze->value()*core->shaft.param.Ra);
draw_ep_graphics(NULL, &core->shaft.elastic);

restoresFormState();
ui->tabWidget->setCurrentIndex(1);           // go to graphics tab
ui->tabWidget_graphics->setCurrentIndex(0);  // go to the elastic region graphics
if (ui->action_html_report->isChecked()){
elastic_report(el_report_fn, *core);
save_graphics();}
if (ui->action_datafile->isChecked()){
    last_datafile=el_data;
    save_data_region(el_data,core->shaft.elastic, this->core->shaft.param);
    save_data_region(el_data_b,core->shaft.bak,   this->core->shaft.param);
}}


void MainWindow::display_ep_results(){
ui->tabWidget->setCurrentIndex(1);           // go to graphics tab
ui->tabWidget_graphics->setCurrentIndex(0);  // go to the elastic region graphics
if (ui->action_html_report->isChecked()){
elastic_report(el_report_fn, *core);             // < ep report
save_graphics();}
restoresFormState();
const Region<DataGrid>& pl = (core->pp.ep_method==1)?core->shaft.plastic1:core->shaft.plastic2;
if (ui->action_datafile->isChecked()){
  last_datafile = "PE"; last_datafile+=(core->pp.ep_method==1)?((core->pp.el_numeric)?"(n)":"(1)"):"2";
  last_datafile+=","+QString::number(core->Rz/core->shaft.param.Ra)+"ra";
  last_datafile+=".data";
  save_data_ep(last_datafile.toStdString().c_str(), pl,
                core->shaft.elastic, core->shaft.param);}
updateTable_rad(pl);
ui->tabWidget_tables->setCurrentIndex(0);}


void MainWindow::display_rp_results(){
QString shoring_state_ = ShorState::ruined;
ui->label_pmax_val->setText("-");
ui->label_pmin_val->setText("-");
int p_state = getPminimax(*core);
const QString no_shorings="Нет подходящих крепей";
QString shor_report="";
switch (p_state){
case -1: shoring_state_ = "Невозможно вычислить давление: необходимо увеличить параметр rz_max.";break;
case 1:{
    if (core->Pmax < core->shaft.shit.Pn/1000) shoring_state_ = ShorState::solid;
    else if (core->Pmin < core->shaft.shit.Pn/1000) shoring_state_ = ShorState::possible_ruined;
    ui->label_pmax_val->setVisible(1); ui->label_pmin_val->setVisible(1);
    ui->label_pmax->setVisible(1); ui->label_pmin->setVisible(1);
    ui->label_pmax_val->setText("<span style='font-size:12pt;'>"+QString::number(core->Pmax*1000)+"</span>");
    ui->label_pmin_val->setText("<span style='font-size:12pt;'>"+QString::number(core->Pmin*1000)+"</span>");
    plot.show_pminmax=1;
    core->situable_shar = calc_shorings(core->shaft.param.w/1000, core->Pmax, *core->shar);
    if (core->situable_shar->size()==0) shor_report = "данной ширине выработки не соответствует ни одна крепь \n";
    else {shor_report = "" ;"подходящих крепей: "+QString::number(core->situable_shar->size())+"\n";
    show_shorings();}
    shor_report += "Давление P на контуре выработки: "+QString::number(core->Pmax*1000)+" КПа";
    break;}
case 0: {ui->label_pmax_val->setVisible(0);  ui->label_pmin_val->setVisible(0);
         ui->label_pmax->setVisible(0);      ui->label_pmin->setVisible(0);
         shoring_state_ = "Невозможно вычислить давление. (возможно параметр S указан неверно)";
         plot.show_pminmax=0;
         break;}}

ui->label_pn_val->setText("<span style='font-size:12pt;'>"+QString::number(core->shaft.shit.Pn)+"</span>");
ui->label_shoring_c_2->setText("<span style='font-size:12pt;'>"+shoring_state_+"</span>");
if (p_state!=1) shor_report = no_shorings + "\n"+shoring_state_;
ui->textBrowser_shoring->setText(shor_report);

draw_rp_graphics();
if (ui->action_html_report->isChecked()){
general_output(ep_report_fn, *core, ui->comboBox_el_method->currentIndex());
save_graphics();}
if (ui->action_datafile->isChecked()){
    last_datafile = gen_RPdatafilename(core->pp);
    save_data_rzvar(last_datafile.toStdString().c_str(),*core);}
restoresFormState();
ui->tabWidget->setCurrentIndex(1); // go to graphics tab
ui->tabWidget_graphics->setCurrentIndex(1); // go to the rock pressure graphics
updateTable_RP();
//ui->tabWidget_tables->setCurrentIndex(1);
}


void MainWindow::display_cem_results(){
//draw_elastic_list_graphics(cem_vec);//draw_elsatic_graphics(core->shaft.elastic_cem);
ui->tabWidget->setCurrentIndex(1); // go to graphics tab
ui->tabWidget_graphics->setCurrentIndex(0); // go to the rock pressure graphics
if (ui->action_html_report->isChecked()){
general_output(ep_report_fn, *core, ui->comboBox_el_method->currentIndex());
save_graphics();}
if (ui->action_datafile->isChecked()){
    save_data_ep("PE(n)", core->shaft.plastic1,core->shaft.elastic, core->shaft.param);
    save_data_region(el_data_cem,core->shaft.elastic_cem, this->core->shaft.param);}
restoresFormState();}


void MainWindow::on_action_paramload_triggered(){
QFileDialog fd;
fd.setFileMode(QFileDialog::ExistingFile);
fd.setDirectory(QDir::currentPath());
QString filename = fd.getOpenFileName(this,"Open File","","ini file (*.ini)");
if (!load_params(filename))
   QMessageBox::warning(this,this->windowTitle(),err_load_param.arg(filename));}


bool MainWindow::load_params(const QString filename){
    if (QFile(filename).exists()){
    QSettings p(filename,QSettings::IniFormat);
    ui->spinBox_eps_n->setValue(p.value("accuracy").toInt());
    p.beginGroup("general_params");
    ui->SpinBox_ra->setValue(p.value("ra").toDouble());
    ui->SpinBox_rb_->setValue(p.value("rb").toDouble());
    ui->SpinBox_S->setValue(p.value("S").toDouble());
    ui->SpinBox_P->setValue(p.value("P").toDouble());
    ui->SpinBox_Elab->setValue(p.value("Elab").toDouble());
    ui->SpinBox_mu->setValue(p.value("mu").toDouble());
    ui->SpinBox_ssl_c->setValue(p.value("ssl_c").toDouble());
    ui->SpinBox_ssl_t->setValue(p.value("ssl_t").toDouble());
    ui->SpinBox_ko->setValue(p.value("ko").toDouble());
    ui->spinBox_N->setValue(p.value("nodes_count").toDouble());
    p.endGroup();
    p.beginGroup("rz_range");
    ui->SpinBox_rzmin->setValue(p.value("r_min").toDouble());
    ui->SpinBox_rzmax->setValue(p.value("r_max").toDouble());
    ui->spinBox_rzn->setValue(p.value("r_n").toInt());
    p.endGroup();
    p.beginGroup("elastic");
    ui->SpinBox_rze->setValue(p.value("rz_el").toDouble());
    ui->checkBox_elproblem->setChecked(p.value("elaastic_problem").toBool());
    p.endGroup();
    p.beginGroup("plastic");
    ui->SpinBox_thetta->setValue(p.value("thetta").toDouble());
    ui->SpinBox_Br->setValue(p.value("Br").toDouble());
    ui->SpinBox_Br->setValue(p.value("Br").toDouble());
    ui->SpinBox_gg->setValue(p.value("gg").toDouble()); // density, include to general
    ui->SpinBox_ff0->setValue(p.value("ff0").toDouble());
    ui->SpinBox_ff1->setValue(p.value("ff1").toDouble());
    ui->SpinBox_km->setValue(p.value("km").toDouble());
    ui->SpinBox_ttk->setValue(p.value("ttk").toDouble());
    ui->SpinBox_upr->setValue(p.value("upr").toDouble());
    ui->SpinBox_kp->setValue(p.value("kp").toDouble());
    ui->SpinBox_ku->setValue(p.value("ku").toDouble());
    ui->SpinBox_kd->setValue(p.value("kd").toDouble());
    ui->SpinBox_ke->setValue(p.value("ke").toDouble());
    ui->SpinBox_ssgp->setValue(p.value("ssgp").toDouble());
    ui->SpinBox_uw->setValue(p.value("Uw").toDouble());
    ui->SpinBox_Pn->setValue(p.value("Pn").toDouble());
    p.endGroup();
    ui->checkBox_burr->setChecked(p.value("burr").toBool());
    ui->SpinBox_c0->setValue(p.value("c0").toDouble());
    ui->SpinBox_c1->setValue(p.value("c1").toDouble());
    ui->SpinBox_c2->setValue(p.value("c2").toDouble());
    ui->comboBox_el_method->setCurrentIndex(p.value("method").toInt());
    ui->label_param_file->setText(filename);
    if (p.status() != QSettings::NoError) return 0; else return 1;}
    else return 0;}


void MainWindow::on_action_paramsave_triggered(){
QFileDialog fd;
fd.setFileMode(QFileDialog::ExistingFile);
fd.setDirectory(QDir::currentPath());
QString file = fd.getSaveFileName(this,"Save File","","ini file (*.ini)");
if (file.size()){
QSettings p(file,QSettings::IniFormat);
p.setValue("accuracy",ui->spinBox_eps_n->value());
p.beginGroup("general_params");
p.setValue("ra",ui->SpinBox_ra->value());
p.setValue("rb",ui->SpinBox_rb_->value());
p.setValue("S",ui->SpinBox_S->value());
p.setValue("P",ui->SpinBox_P->value());
p.setValue("Elab",ui->SpinBox_Elab->value());
p.setValue("mu",ui->SpinBox_mu->value());
p.setValue("ssl_c",ui->SpinBox_ssl_c->value());
p.setValue("ssl_t",ui->SpinBox_ssl_t->value());
p.setValue("ko",ui->SpinBox_ko->value());
p.setValue("nodes_count",ui->spinBox_N->value());
p.endGroup();
p.beginGroup("rz_range");
p.setValue("r_min",ui->SpinBox_rzmin->value());
p.setValue("r_max",ui->SpinBox_rzmax->value());
p.setValue("r_n",ui->spinBox_rzn->value());
p.endGroup();
p.beginGroup("elastic");
p.setValue("rz_el",ui->SpinBox_rze->value());
p.setValue("elaastic_problem",ui->checkBox_elproblem->isChecked());

p.endGroup();
p.beginGroup("plastic");
p.setValue("thetta",ui->SpinBox_thetta->value());
p.setValue("Br",ui->SpinBox_Br->value());
p.setValue("Br",ui->SpinBox_Br->value());
p.setValue("gg",ui->SpinBox_gg->value());
p.setValue("ff0",ui->SpinBox_ff0->value());
p.setValue("ff1",ui->SpinBox_ff1->value());
p.setValue("km",ui->SpinBox_km->value());
p.setValue("ttk",ui->SpinBox_ttk->value());
p.setValue("upr",ui->SpinBox_upr->value());
p.setValue("kp",ui->SpinBox_kp->value());
p.setValue("ku",ui->SpinBox_ku->value());
p.setValue("kd",ui->SpinBox_kd->value());
p.setValue("ke",ui->SpinBox_ke->value());
p.setValue("ssgp",ui->SpinBox_ssgp->value());
p.setValue("Uw",ui->SpinBox_uw->value());
p.setValue("Pn",ui->SpinBox_Pn->value());
p.endGroup();
p.setValue("burr",ui->checkBox_burr->isChecked());
p.setValue("c0", ui->SpinBox_c0->value());
p.setValue("c1", ui->SpinBox_c1->value());
p.setValue("c2", ui->SpinBox_c2->value());
p.setValue("method",ui->comboBox_el_method->currentIndex());
p.sync();
ui->label_param_file->setText(file);}}


void MainWindow::on_action_openel_report_triggered(){
    QUrl url(el_report_fn); QDesktopServices::openUrl(url);}


void MainWindow::on_action_open_ep_report_triggered(){
    QDesktopServices::openUrl(QUrl(QString(ep_report_fn)));}


void MainWindow::on_comboBox_calc_method_currentIndexChanged(int index){
    if (index>0) ui->comboBox_el_method->setEnabled(0);
    else ui->comboBox_el_method->setEnabled(1);}

void MainWindow::on_spinBox_eps_n_valueChanged(int arg1){
    ui->label_eps->setText(QString::number(1./pow(10,arg1),'f',10));}


void MainWindow::on_checkBox_elproblem_stateChanged(int arg1){
if (arg1) ui->groupBox_sc->setEnabled(0);
else ui->groupBox_sc->setEnabled(1);}

void MainWindow::on_checkBox_burr_stateChanged(int arg1){
if (arg1==0) ui->label_Er->setEnabled(0);
else ui->label_Er->setEnabled(1);}

void MainWindow::on_pushButton_delta_clicked(){
    QTableWidget* tab = (ui->tabWidget_tables->currentIndex()==0)?ui->tw_radius:ui->tw_RP;
    calc_delta(tab);}


void MainWindow::setCalwaitState(){
ui->groupBox_5->setEnabled(0);
this->title = this->windowTitle();
QString c = QString::fromUtf8("идут вычисления..."); this->setEnabled(0);
this->setWindowTitle(c);}

void MainWindow::restoresFormState(){
setWindowTitle(title);
setEnabled(1);
ui->groupBox_5->setEnabled(1);}


void MainWindow::on_action_src_triggered(){QDesktopServices::openUrl(QUrl(URLrepo));}

void MainWindow::on_action_pub_triggered(){QDesktopServices::openUrl(QUrl(URLpubl));}


void MainWindow::on_pushButton_clear_table_clicked(){
QTableWidget* tab = (ui->tabWidget_tables->currentIndex()==0)?ui->tw_radius:ui->tw_RP;
//tab->clear();
tab->setRowCount(0);}


void MainWindow::on_action_triggered(){QMessageBox::about(this, ProgName,ProgFullName);}


void MainWindow::on_checkBox_calcff_toggled(bool checked){
    ui->SpinBox_ff0->setEnabled(!checked);
    ui->SpinBox_ff1->setEnabled(!checked);}


void MainWindow::on_comboBox_strength_currentIndexChanged(int index){
    if (index) ui->groupBox_6->setEnabled(1);
    else ui->groupBox_6->setEnabled(0);}


void MainWindow::widthtoRa(double w) { ui->SpinBox_ra->setValue(w/2*1000);}

void MainWindow::ratoWidth(double ra) { ui->spinBox_width->setValue(ra*2/1000);}

void MainWindow::on_action_manual_triggered(){
QUrl manual_url=QUrl(URLmanual);
if (QFile::exists(Files::user_manual)) QDesktopServices::openUrl(Files::user_manual);
else if (QFile::exists("../"+Files::user_manual)) QDesktopServices::openUrl("../"+Files::user_manual);
else QDesktopServices::openUrl(manual_url);}


void MainWindow::on_action_more_param_triggered(){
    bool state = ui->action_more_param->isChecked();
    ui->SpinBox_ko->setEnabled(state);
    ui->SpinBox_ku->setEnabled(state);
    ui->SpinBox_kd->setEnabled(state);
    ui->SpinBox_ke->setEnabled(state);
    ui->SpinBox_km->setEnabled(state);
    ui->SpinBox_uw->setEnabled(state);}


void MainWindow::on_action_open_shorings_triggered(){
QString fname=Files::shorings;
if (QFile::exists(fname)) QDesktopServices::openUrl(fname);
else if (QFile::exists("../"+fname)) QDesktopServices::openUrl("../"+fname);
else QMessageBox::warning(this,this->windowTitle(),err_no_shor_file.arg(Files::shorings));}


void MainWindow::on_SpinBox_ssl_c_valueChanged(double ssl){
    get_params();
    init_core();
    ui->SpinBox_c0->setValue(-core->shaft.media.ssm);
    init_core();}

void MainWindow::on_actionGnuplot_triggered(){
if (last_datafile!="") QDesktopServices::openUrl(last_datafile);}
