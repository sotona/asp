#ifndef HTML_OUT_H
#define HTML_OUT_H
#include "calculation.h"
/// r8 = 0,analytical solution; 1,from elastic zone
void general_output(const char *filename, const Core& c, bool r8);
void elastic_report(const char* filename, const Core& c);

// save_data_* functions - save arrays of DataGrid to file
void save_data_region(const char* filename, const Region<DataGrid> &reg, const main_param &p);
void save_data_ep(const char* filename, const Region<DataGrid> &pl, const Region<DataGrid> &el,
                                                                        const main_param& p);
void save_data_rzvar(const char* filename, const Core& c);

#endif // HTML_OUT_H
