#include <string.h>
#include <vector>
#include "html_out.h"
#include "calculation.h"
#include "cm/g.h"
#include <QDebug>
using namespace std;

RPdata::RPdata(){
clear();}

RPdata::RPdata(const RPdata &rpd){
clear();
unsigned long size = N*sizeof(double);
memcpy(ua,    rpd.ua,     size);
memcpy(uz,    rpd.ua,     size);
memcpy(p,     rpd.p,      size);
memcpy(pob,   rpd.pob,    size);
memcpy(rzra,  rpd.rzra,   size);
memcpy(rkra,  rpd.rkra,   size);
memcpy(rpra,  rpd.rpra,   size);
memcpy(ssrz,  rpd.ssrz,   size);
memcpy(sstz,  rpd.sstz,   size);
memcpy(f,     rpd.f,      size);
memcpy(sscz,  rpd.sscz,   size);
memcpy(sso,   rpd.sso,    size);
memcpy(theta, rpd.theta,  size);
memcpy(ttmz,  rpd.ttmz,   size);
memcpy(Er_z,  rpd.Er_z,   size);
memcpy(ss_c_z,rpd.ss_c_z, size);
n=rpd.n; pp_i=rpd.pp_i; pp_val=rpd.pp_val;
}

void RPdata::clear(){
unsigned long size = N*sizeof(double);
memset(ua,    0, size);
memset(p,     0, size);
memset(pob,   0, size);
memset(rzra,  0, size);
memset(rkra,  0, size);
memset(rpra,  0, size);
memset(ssrz,  0, size);
memset(sstz,  0, size);
memset(f,     0, size);
memset(sscz,  0, size);
memset(sso,   0, size);
memset(theta, 0, size);
memset(ttmz,  0, size);
memset(Er_z,  0, size);
memset(ss_c_z,0, size);
n=0;pp_i=0;pp_val=0;}

double accuracy=0.000001;


/// find first intersection of the curves. return next index after (at) intersection. return 0, if no intersection
/// curve 1 and curve 2 mush be same long and corresponded indexes
unsigned find_intersection(double *curve1, double *curve2, unsigned n){
bool x=(curve1[0]>curve2[0]);
for (unsigned i=0; i<n; i++) if ((curve1[i]>curve2[i]) != x)return i;
return 0;}


//---------------------------------------------------------------------------------------
/// INTERsection pOINT
void interoint(double _x1, double _y1, double _x2, double _y2,
                         double x1, double y1, double x2, double y2, double &x, double &y){
x=-((_x1*_y2-_x2*_y1)*(x2-x1)-(x1*y2-x2*y1)*(_x2-_x1))/((_y1-_y2)*(x2-x1)-(y1-y2)*(_x2-_x1));
y=-((y1-y2)*x-(x1*y2-x2*y1))/(x2-x1);}


/// calculate P_min, P_max and intresection P_ob with P curves
/// return 1 - ok
int  getPminimax(Core& c){
unsigned ii = find_intersection(c.aval.pob, c.aval.p, c.aval.n);
if (ii-1<ii){
    interoint(ii-1,c.aval.p[ii-1], ii, c.aval.p[ii], ii-1,c.aval.pob[ii-1], ii, c.aval.pob[ii], c.aval.pp_i,c.aval.pp_val);
    double px = c.aval.pp_val;
    OneDGrid::N_Type ix = ii;
    c.Pmin = px;

    double pob_0, pob_uw;
    OneDGrid::N_Type i_0=-1, i_uw=-1;

// где p=0?
//if (c.aval.p[c.aval.n-1]>0) return -1;    // нужно увеличить rz_max
for (OneDGrid::N_Type i = c.aval.n-1; i>=0; i--)
  if (c.aval.p[i]<=0) {i_0=i; pob_0 = c.aval.pob[i_0]; break;}

double shor_u = c.shaft.shit.uw;
// if there is UW and Pob intersection
if (-c.aval.ua[c.aval.n-1] >= shor_u && -c.aval.ua[0] <= shor_u && c.aval.n>1){
OneDGrid::N_Type i=c.aval.n-1;
for (;-c.aval.ua[i] < shor_u || -c.aval.ua[i-1] > shor_u;) i--;
pob_uw = (c.aval.pob[i] + c.aval.pob[i-1])/2;
i_uw=i;}
else i_uw=-1; // no Uw and Pob intersection

if (i_uw==-1)
  if (i_0==-1) return 0;
  else c.Pmax = pob_0;
else
   if (i_uw<ix)
    if (i_0==-1) return 0;
    else c.Pmax = pob_0;
   else
   if (i_uw>i_0) c.Pmax = pob_uw;
   else          c.Pmax = pob_0;
return 1;

} else return 0;
}


//-----------------------------------------------------------------------------
inline void calc_at_rz_b(Shaft& sh, double ra, double rz, double &uz, double &ssrz, double& sstz){
double x[4];
plastic_zone_t::calc_at_radius_z(ra,rz,sh.media,sh.shit,sh.param.S,x);
uz=x[3]; ssrz=x[1]; sstz=x[2];
}


//---------------------
/// calcutalate elstic, and return u, ssr at rz.
/// @param elprob   - elastic problem?
/// @param strength - index of strength criterion
inline void calc_at_rz_e(Shaft& sh, double &u, double &ssr, const prob_param& pp){
double_vector x(2);
x[0] = u; x[1] = ssr;
elastic_zone::calc(sh,x, pp.eps, pp.el_only, pp.strength_i);
u=sh.elastic.detail.u[0];
ssr=sh.elastic.detail.ssr[0];}


//--------------------------------------
void calc_elastic_b(Shaft &sh, bool burr){
double Ra = sh.param.Ra, S = sh.param.S;
sh.bak.detail.clear();
sh.bak.grid.init(Ra,sh.param.Rb,sh.elastic.grid.n());
sh.bak.detail.resize(sh.elastic.grid.n());

Media &m = sh.media;
bur(sh.shit.Br, m.ssl_c, Ra, m.db_param);
if (!burr) no_bur(m.db_param);
initYongModulus(sh.bak.detail, sh.media, sh.bak.grid, Ra);

double a = sh.media.db_param.a, n = sh.media.db_param.n;
double n22r;
double p=-sh.param.P, r=Ra;

for (OneDGrid::N_Type i=0; i<sh.bak.grid.n(); i++){
r = sh.bak.grid.r(i);
n22r=(n+2.-2.*a)*pow(r/Ra,2.);
double power = pow(r/Ra,-n);
double q = -S;
sh.bak.detail.ssr[i]= q+((p-q)*(n+2.-2.* a * power       )/n22r);
sh.bak.detail.sst[i]=q+(-p+q)*(n+2.-2.* a * pow(r/Ra,-n)*(n+1.))/n22r;
sh.bak.detail.u[i]= 3.*(q-p)/2./sh.media.Em * (n+2.) / (n+2-2*a) * (1./(r/Ra)) * Ra;
sh.bak.detail.eet[i]=sh.bak.detail.u[i]/r;}
}


//-----------------------------------------------------------------------------
double calc_elastic(Shaft* sh, const prob_param &pp, double Rz){
Media &m=sh->media;
const double Ra = sh->param.Ra, Rb = sh->param.Rb;
bur(sh->shit.Br, m.ssl_c, Ra, m.db_param);
if (!pp.burr) no_bur(m.db_param);
double rr = Rb-Ra;
OneDGrid::N_Type Ne = (Rb-Rz)/rr*sh->N;
OneDGrid::N_Type Np = sh->N-Ne;
sh->elastic.grid.init(Rz, Rb, Ne);
sh->plastic1.grid.init(Ra, Rz, Np);
sh->elastic.detail.resize(Ne);
initYongModulus(sh->elastic.detail, sh->media, sh->elastic.grid, Ra);
initCompSigma(sh->elastic.detail, sh->media, sh->elastic.grid, Ra);
double_vector x(3);
calc_at_rz_b(*sh,Ra,Rz,x[0],x[1],x[2]);
x.resize(2);
return elastic_zone::calc(*sh,x,pp.eps,pp.el_only,pp.strength_i);}


inline void initNeNp(Region<DataGrid> &pl, Region<DataGrid> &el, OneDGrid::N_Type N, const main_param& pp, double Rz){
const double rr = pp.Rb-pp.Ra;
OneDGrid::N_Type Ne = (pp.Rb-Rz)/rr*N;
OneDGrid::N_Type Np = N-Ne;
if (Np==0){pl.grid.init(pp.Ra,Rz,Np);Np = pl.grid.n();Ne = N-Np;}
el.grid.init(Rz, pp.Rb, Ne);   el.detail.resize(el.grid.n());
pl.grid.init(pp.Ra, Rz, Np);   pl.detail.resize(pl.grid.n());
}

//-------------------------------------------------------
bool calc_ep(Shaft& sh, const prob_param& pp, double Rz, double& f){
const double Ra = sh.param.Ra;
Region<DataGrid> &el = sh.elastic;
Region<DataGrid> &pl1 =sh.plastic1;
const OneDGrid &pg=pl1.grid;
Media &m = sh.media;
Crap &s = sh.shit;
// some initializations
initNeNp(pl1,el,sh.N, sh.param, Rz);
const OneDGrid::N_Type n = pg.n()-1;
(!pp.burr)?no_bur(m.db_param):bur(sh.shit.Br, m.ssl_c, Ra, m.db_param);
initYongModulus(el.detail, sh.media, el.grid, Ra);
initCompSigma  (el.detail, sh.media, el.grid, Ra);
pl1.detail.sso[n]=1; pl1.detail.thetta[n]=0;
for (OneDGrid::N_Type i=0; i<pg.n(); i++){
pl1.detail.ss_c[i]=m.ssm*(1.0-m.db_param.b*pow(pg.r(i)/Ra,-m.db_param.n));}

double_vector v(2);
calc_at_rz_b(sh, Ra, Rz, v[0], v[1], s.sstz);

if (pp.el_numeric==0) {pl1.detail.u[n]=v[0]; pl1.detail.ssr[n]=v[1];}
else {
 f = elastic_zone::elastic_optfunc_2d(v,(void*)&sh);
 calc_at_rz_e(sh, v[0], v[1], pp);
 f = elastic_zone::elastic_optfunc_2d(v,(void*)&sh);
 sh.shit.sstz=sh.elastic.detail.sst[0];
 pl1.detail.u[n] = el.detail.u[0];
 pl1.detail.ssr[n] = el.detail.ssr[0];
}
s.rk=0;  s.rp=0;       plastic_zone_t::calc(sh);
sh.param.P=-pl1.detail.ssr[0]/s.ksr;
return 1;}


//----------------------------------------------------------
bool calc_ep_d(Shaft& sh, const prob_param &pp, double rz, double& f){
const double Ra = sh.param.Ra, Rb = sh.param.Rb;
Region<DataGrid> &pl = sh.plastic2;
Region<DataGrid> &el = sh.elastic;
Media &m = sh.media;
double rr = Rb-Ra;
OneDGrid::N_Type Ne = (Rb-rz)/rr*sh.N;
OneDGrid::N_Type Np = sh.N-Ne;
el.grid.init(rz, Rb, Ne);
el.detail.resize(Ne);
bur(sh.shit.Br, m.ssl_c, Ra, m.db_param);
if (!pp.burr) no_bur(m.db_param);
initYongModulus(el.detail, sh.media, el.grid, Ra);
initCompSigma  (el.detail, sh.media, el.grid, Ra);

sh.plastic1.grid.init(Ra, rz);
double_vector x(3);
calc_at_rz_b(sh,Ra,rz, x[0],x[1],x[2]);
f = elastic_zone::calc(sh,x,pp.eps,pp.el_only,pp.strength_i);

if (el.detail.ssr[0]<=0) {
pl.grid.init(Ra, rz, Np);
pl.detail.resize(pl.grid.n());
plastic_zone::calculation(sh);
if (fabs(pl.detail.u[0])>sh.shit.upr) return 0;
else return 1;} else return 0;
return 1;}


///calc el for cmentation
double calc_e_cem(Shaft* sh, const prob_param &pp, double Rz){
const double Ra = sh->param.Ra;
sh->elastic_cem.grid.init(Ra, sh->param.Rb, sh->N);
sh->elastic_cem.detail.resize(sh->elastic_cem.grid.n());
cem_options(sh->media.db_param, sh->param.Rc/Ra);
initYongModulus_cem(sh->elastic_cem.detail, sh->media, sh->elastic_cem.grid, Ra);
initCompSigma(sh->elastic_cem.detail, sh->media, sh->elastic_cem.grid, Ra);
double_vector x(3);
calc_at_rz_b(*sh,Ra,Rz,x[0],x[1],x[2]);
x.resize(2);
return elastic_zone::calc_cem(*sh,x,pp.eps);}


/// calc el and pl zones for cementation
void calc_ep_cem(Shaft *sh, const prob_param &pp, double rz){
Region<DataGrid> &pl = sh->plastic1;
Region<DataGrid> &el = sh->elastic;
double f=-1;
calc_ep(*sh,pp,rz,f);
calc_e_cem(sh,pp, rz);
Region<DataGrid> &el_c = sh->elastic_cem;

for (OneDGrid::N_Type i=0; i<pl.detail.n();i++){
el_c.detail.u[i]=pl.detail.u[i]+el_c.detail.u[i];
//el_c.detail.ssr[i]=pl.detail.ssr[i]+el_c.detail.ssr[i];
//el_c.detail.sst[i]=pl.detail.sst[i]+el_c.detail.sst[i];
}
for (OneDGrid::N_Type i=0; i<el.detail.n(); i++){
el_c.detail.u[i+pl.detail.n()]=el.detail.u[i]+el_c.detail.u[i+pl.detail.n()];
//el_c.detail.ssr[i+pl.detail.n()]=el.detail.ssr[i]+el_c.detail.ssr[i+pl.detail.n()];
//el_c.detail.sst[i+pl.detail.n()]=el.detail.sst[i]+el_c.detail.sst[i+pl.detail.n()];
}
}




inline void append_RPdata(RPdata& rpd, const OneDGrid& rz, const Region<DataGrid>& pl, const Region<DataGrid>& el,
            const Media& m, const Crap& crap, double Ra, OneDGrid::N_Type i){
rpd.rzra[i]=rz.r(i)/Ra;
rpd.ua[i] = pl.detail.u[0];
rpd.p[i] = -pl.detail.ssr[0]/crap.ksr;
rpd.uz[i] = el.detail.u[0];
rpd.ssrz[i] = el.detail.ssr[0];
rpd.sstz[i] = el.detail.sst[0];
rpd.eerz[i] = el.detail.eer[0];
rpd.eetz[i] = el.detail.eet[0];
rpd.thetaz[i] = el.detail.thetta[0];
rpd.theta[i] = pl.detail.thetta[0];
rpd.ss_c_z[i] = el.detail.ss_c[0]/m.ssm;
rpd.Er_z[i] = el.detail.Er[0]/m.Em_p;
rpd.pob[i]=(crap.kd* m.density*(rz.r(i)-Ra))/crap.ksr;
// see rpd.f assigment in calc_ep calc
rpd.rkra[i]=crap.rk/Ra;
rpd.rpra[i]=crap.rp/Ra;
rpd.sscz[i] = (rpd.ssrz[i]+rpd.sstz[i])/2;
rpd.sso[i] = pl.detail.sso[0];
rpd.ttmz[i] = (rpd.ssrz[i]-rpd.sstz[i])/2;}


/// rz=[ra..k*rz] grid, grid for plastic deformation zone, ...
void calc_RP(Core &c, const prob_param& pp, bool &is_pl){
Region<DataGrid> &pl = c.shaft.plastic1;
Region<DataGrid> &el = c.shaft.elastic;
Crap &s = c.shaft.shit;
const Media &m = c.shaft.media;
const double Ra = c.shaft.param.Ra, Rz = c.shaft.param.Ra, Rb = c.shaft.param.Rb;
pl.grid.init(Ra,Rz,1); pl.detail.resize(pl.grid.n());
// calc at Ra
if (pp.el_numeric==0){
    el.grid.init(Rz, Rz, 1);
    el.detail.resize(el.grid.n());
    calc_at_rz_b(c.shaft, Ra, Rz, pl.detail.u[pl.grid.n()-1], pl.detail.ssr[pl.grid.n()-1], s.sstz);c.shaft.param.P = -pl.detail.ssr[0]/s.ksr;}
else{el.grid.init(Ra, Rb, c.shaft.N-1);
     el.detail.resize(el.grid.n());
     initYongModulus(el.detail, c.shaft.media, el.grid, Ra);
     initCompSigma(el.detail, c.shaft.media, el.grid, Ra);
     calc_at_rz_e(c.shaft,pl.detail.u[pl.grid.n()-1], pl.detail.ssr[pl.detail.n()-1], c.pp);
     c.shaft.param.P=-pl.detail.ssr[0]/s.ksr;}

if (pl.detail.ssr[pl.detail.n()-1] < 0) is_pl=0;
else is_pl=1;

OneDGrid::N_Type i;
for (i=0; i<c.rz_range.n(); i++){
calc_ep(c.shaft, pp, c.rz_range.r(i), c.aval.f[i]);
if (c.shaft.param.P<0) c.shaft.param.P=0;
if ((s.omega>75*M_PI/180) && (s.omega<115*M_PI/180)){
    el.detail.u[0]=pl.detail.u[pl.grid.n()-1];
    el.detail.ssr[0]=pl.detail.ssr[pl.grid.n()-1];
    el.detail.sst[0]=pl.detail.sst[pl.grid.n()-1];
append_RPdata(c.aval, c.rz_range, pl, el, m, s, Ra,i);
}
if (c.aval.p[i] < -c.pp.eps*2 ){break;}
if  ((fabs(pl.detail.u[0])>s.upr) || (c.shaft.param.P+(i==0)?0:(c.aval.p[i-1])<DBL_EPSILON)) {break; }}
c.aval.n=(i+1<c.rz_range.n())?i+1:i;}


//-----------------------------------------
/// calc el and pl zones vith dilatancy
void calc_RP_d(Core &c){
const double Ra = c.shaft.param.Ra;
Region<DataGrid> &pl = c.shaft.plastic2;
Region<DataGrid> &el = c.shaft.elastic;
Crap &s = c.shaft.shit;
Media &m = c.shaft.media;

OneDGrid::N_Type i=0;
for (; i<c.rz_range.n(); i++){
bool r = calc_ep_d(c.shaft,c.pp,c.rz_range.r(i),c.aval.f[i]);
append_RPdata(c.aval, c.rz_range, pl, el, m, s, Ra,i);
if (r==0) {i--; break;} // u > u_max
if (c.aval.p[i] < -c.pp.eps*2 ) break;}
c.aval.n=(i+1<c.rz_range.n())?i+1:i;
}


/// main calculation function
void calc_rp(Core* c, const prob_param& pp){
Media &m=c->shaft.media;
bur(c->shaft.shit.Br, m.ssl_c, c->shaft.param.Ra, m.db_param);
if (!c->pp.burr) no_bur(m.db_param);
plastic_zone_t::set_consts(c->rz_range,c->shaft.media,c->shaft.shit,c->shaft.param.S);
if (pp.ep_method==1) calc_RP  (*c,pp, c->is_pl);
else                 calc_RP_d(*c);
}
