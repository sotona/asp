#include "shoring.h"
#include <fstream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <stdlib.h>
#include <QFile>
#include <QStringList>


using namespace std;


shoringArray* load_shoring_data(QString filename){
shoringArray *shar = new shoringArray;
QFile f(filename);
if (!f.open(QIODevice::ReadOnly | QIODevice::Text)) return shar;
if (f.isOpen()){
QString line;
while (!f.atEnd()){
    line = f.readLine();
    line.replace(",",".");
    QStringList sl = line.split(";");
    if (line.count("#")>0 || line.size()<7) continue;
    shoring sh;
    sh.name = sl[2];
    sh.w_min = sl.at(0).toFloat();
    sh.w_max = sl.at(1).toFloat();
    if (sh.w_max < sh.w_min) swap(sh.w_max, sh.w_min);
    sh.P = sl[3].toFloat();
    shar->push_back(sh);}
f.close();}
return shar;}


// width [m], P [MPa]
shoringArray* calc_shorings(double width, double P, const shoringArray& sl){
shoringArray* shar = new shoringArray;
for (std::size_t i=0; i<sl.size(); i++){
 if (sl[i].w_min <= width && sl[i].w_max >= width){
 shoring sh = sl[i];
 sh.density = 1000*P*width/sl[i].P;
 shar->push_back(sh);}
}
return shar;}


inline QString shorToStr(const shoring &sh){
return QString::number(sh.w_min)+"-"+QString::number(sh.w_max)+"    "+sh.name + " " +
                    QString::number(sh.P) + "   " + QString::number(sh.density);}


