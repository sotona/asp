#ifndef CALCULATION_H
#define CALCULATION_H
#include <utility>
#include "cm/multidimensional_opt.h"
#include "eng/elastic.h"
#include "eng/plastic.h"
#include "shoring.h"

using namespace cm;
const short N=1024; // elements count in rz arrays

class RPdata{ // arrays of values at [rzm..rzp] section
public:
	double ua[N];           ///< displacements at inner (Ra) contour [mm]
	double uz[N];           ///< displacements at inner (Ra) contour [mm]
	double p[N];            ///< pressure at inner (Ra) contour      [MPa]
	double pob[N];          ///< collapse pressure					 [MPa]
    double rzra[N];         ///< rz/ra
    double rkra[N];
    double rpra[N];
    double ssrz[N];          ///< radial stress at R*				  [MPa]
    double sstz[N];          ///< circular stress at R*				  [MPa]
    double eerz[N], eetz[N];
    double sscz[N];          ///< sigma c at R*
    double sso[N];          ///< sigma o at R*
    double theta[N];        ///< ttp
    double thetaz[N];
    double ttmz[N];          ///< tau m
    unsigned n;             ///< length of this arrays
    double pp_i;            ///< real value index of intersection p and pob arrays
    double pp_val;          ///< Pressure at p and Pob intersection
	double Er_z[N];			///< Er*/Em
	double ss_c_z[N];		///< sigma_c*/sigma_m
    double f[N];            ///< NLP function's value
    RPdata();
    RPdata(const RPdata& rpd);
void clear();
};

/// params of the problem
struct prob_param{
    prob_param(){ep_method=0, burr=1, el_only=0, el_numeric=1,
    calc_ff = 0, strength_i = elastic_zone::strength_criterion::nl2;
     eps = 0.00001;}
    unsigned char ep_method;            ///< nl.dilat = 2, lin.dilat=1, 0 - undefined (RP or cem problem)
    bool burr;                          ///< is drilling and blasting used?
    bool el_numeric;           ///< FDM.NLP=1, T=0
    bool calc_ff;
    bool el_only;                       ///< elastic problem or elastic region = (0 or 1)
    unsigned char strength_i;           ///< index of strength criterion(function) = (0,1,2)
    double eps;                         ///< epsilon = accuracy
};

struct Core {
    OneDGrid rz_range;
    double Rz;
    Shaft shaft;
    RPdata aval;
    prob_param pp; ///< other params of the problem's solytion

	double f;

    double Pmin, Pmax;
    bool is_pl;   /// is there plastic zone?

    shoringArray *shar;              // array of all shorings
    shoringArray *situable_shar;
};

extern double accuracy;
double optimize_elastic(double_vector& v, void* p);      ///< function is using in nonlinear programming in elastic zone
const bool fdm_nlp = 1;
const bool analyt_rz = 0;

/// calc elstic problem(region problem) [el]
/// results -> Shaft::elastic
double calc_elastic(Shaft* sh, const prob_param &pp, double Rz);

/// get analitical solution for elastic zone [bak]
/// results -> Shaft::bak
void calc_elastic_b(Shaft &sh, bool burr);

/// main calc function. calc rock pressure with differrent (see rz_range) r* values
/// @param ep_method equal 1 for solution with dilatancy and numerical method fol elastic zone,
/// otherwise no dilatancy and elastic zone according with el_method
/// @param el_method equal 0 for FDM+NLP for elastic zone, otherwise analytical solution at r*
/// results -> Core::a_values
void calc_rp(Core* d, const prob_param &pp);

/// calcfaltion in elastic region and plastique regions
/// results -> Shft::crap and Shaft::elastic
/// @param method   - method for elastic {analyt_rz or fdm_nlp}
/// @param elprob   - elastic problem (rz=1)?
/// @param strength - index of strength criterion (ssc, nl, nl2)
bool calc_ep(Shaft& sh, const prob_param& pp, double Rz, double &f);

/// calculate elasctic and plastic zones with dilatancy
/// results -> Shaft::elastic and Shaft::plastic
bool calc_ep_d(Shaft& sh, const prob_param &pp, double rz, double &f);

/// calculate all regions with different Rz values
/// @param method - method of calculation elastic zone
/// results -> Core::a_values
void calc_RP(Core &c, const prob_param &pp, bool &is_pl);

int getPminimax(Core& c);

/**
* @brief calc_ep_cemenation
* @param sh
* @param pp
* @param rz
* @param ra
* @param rb
*/
void calc_ep_cem(Shaft *sh, const prob_param &pp, double rz);

double calc_e_cem(Shaft* sh, const prob_param &pp, double Rz);

#endif // CALCULATION_H

/*
 * [bak] Баклашов, Игорь Владимирович (2004): Геомеханика. Учеб. для студентов вузов, обучающихся по направлению, подгот. бакалавров и магистров "Горн. дело" и по специальностям "Физ. процессы горн. или нефтегаз. пр-ва" и "Шахт. и подзем. стр-во" направлению подгот. дипломир. специалистов "Горн. дело" : В 2 т. М: Изд-во МГГУ.
 * [el] МЕТОД УЧЁТА ИСТОРИИ НАГРУЖЕНИЯ В РЕШЕНИИ ЗАДАЧ УПРУГОСТИ ДЛЯ ОДИНОЧНЫХ ГОРИЗОНТАЛЬНЫХ ВЫРАБОТОК, ПРОВЕДЕННЫХ БУРОВЗРЫВНЫМ СПОСОБОМ. In Вестник ЗабГУ (6), pp. 37-47
 *
 *
 */



