#include "auxiliary.h"

const char* el_report_fn = "elastic_report.html";
const char* ep_report_fn = "report.html";
const char* report_dir = "report";
const char* el_data = "elastic.data";
const char* el_data_b = "elastic_b.data";
const char* el_data_cem = "elastic_cem.data";
//const char* ep_data = "plastic";
const char* rzv_data[] = {"rz_var.tt.data", "rz_var.t1.data","rz_var.2.data"}; ///< T solution, 2 - with dilatancy

void setup_grid(QwtPlotGrid* grid){
grid->enableXMin(true);
grid->setMajorPen(QPen(Qt::black,0,Qt::DotLine));
grid->setMinorPen(QPen(Qt::gray,0,Qt::DotLine));}

void setup_picker(QwtPlotPicker* p, QwtPlot *plot){
p = new QwtPlotPicker(QwtPlot::xBottom, QwtPlot::yLeft,
    QwtPlotPicker::CrossRubberBand, QwtPicker::AlwaysOn,
    plot->canvas());
p->setStateMachine(new QwtPickerDragPointMachine());
p->setRubberBandPen(QColor(Qt::green));
p->setRubberBand(QwtPicker::CrossRubberBand);
p->setTrackerPen(QColor(Qt::black));}


PlotStaff::PlotStaff(QwtPlot *plot, QString bttm_ax_titl, QString lft_ax_ttl){
grid = new QwtPlotGrid();
grid->enableXMin(true);
grid->setMajorPen(QPen(Qt::black,0,Qt::DotLine));
grid->setMinorPen(QPen(Qt::gray,0,Qt::DotLine));
grid->attach(plot);

picker = new QwtPlotPicker(QwtPlot::xBottom, QwtPlot::yLeft,
    QwtPlotPicker::CrossRubberBand, QwtPicker::AlwaysOn,
    plot->canvas());
picker->setStateMachine(new QwtPickerDragPointMachine());
picker->setRubberBandPen(QColor(Qt::green));
picker->setRubberBand(QwtPicker::CrossRubberBand);
picker->setTrackerPen(QColor(Qt::black));

panner = new QwtPlotPanner(plot->canvas());
mag = new QwtPlotMagnifier(plot->canvas());
legend = new QwtLegend();
plot->insertLegend(legend, QwtPlot::BottomLegend);
plot->setAxisTitle(QwtPlot::xBottom, bttm_ax_titl);
plot->setAxisTitle(QwtPlot::yLeft, lft_ax_ttl);}
//------------------------------------------------------------
QImage * make_hmap(QRectF r, float dx, float dy, const cm::foo f, Shaft& sh){
unsigned long w = r.width()/dx;
unsigned long h = r.height()/dy;
float map[w][h];
float max=DBL_MIN, min=DBL_MAX;
double_vector x(2);
for (unsigned long i=0; i<w; i++){ x[0]=r.x()+i*dx;
    for (unsigned long j=0; j<h; j++){ x[1]=r.y()+j*dy;
    float ff =  f(x,(void*)&sh);
    map[i][j] = ff;
    if (map[i][j]>max) max = map[i][j];
    if (map[i][j]<min) min = map[i][j];}}

QImage *img = new QImage(w,h,QImage::Format_RGB32);
img->fill(Qt::white);
QPainter p;
p.setPen(QColor(0,0,0));
p.begin(img);
float n;
for (unsigned long i=0; i<w; i++){
    for (unsigned long j=0; j<h; j++){
    n=(map[i][j]-min)/max;
    p.setPen(QColor(255*n,255*n,255*n));
    p.drawPoint(j,i);}}
p.end();
return img;}

QwtPlotMarker* create_dot_marker(const QColor& c){
QwtPlotMarker *m = new QwtPlotMarker();
QwtSymbol* s = new QwtSymbol( QwtSymbol::Diamond, c, Qt::NoPen, QSize( 5, 5) );
m->setSymbol(s);
m->setLabelAlignment(Qt::AlignTop);
return m;}


void calc_delta(QTableWidget* tab){
if (tab->selectionModel()->hasSelection()){
QModelIndexList il = tab->selectionModel()->selectedRows();
if (il.count()==2){
    int i1=il.first().row(), i2=il.last().row();
    int ri = tab->rowCount();
    int cols = tab->columnCount()-1;
    tab->insertRow(ri);

    tab->setItem(ri, cols, new QTableWidgetItem("Delta: "+QString::number(i1+1)+"-"+QString::number(i2+1)));
    double d=0;
    for (int i=0; i<cols; i++){
        QVariant a = tab->item(i1, i)->data(0);
        QVariant b = tab->item(i2, i)->data(0);
        bool ok=0;
        d = a.toDouble(&ok)-b.toDouble(&ok);
        insert_to_table(tab, d, ri, i);
    }
}
}}


QString gen_RPdatafilename(const prob_param& pp){
QString fn="RP-";
if (pp.ep_method==2) fn+="2";
else {fn+="T";
    if (pp.el_numeric==1) {fn+="1(";
    fn+=(pp.strength_i==0)?"l":"nl";
    fn+=")";}
    else fn+="T";
      fn+=(pp.burr)?"-DnB":"";}
return fn+".data";
}

bool calc_ff(Media& m, double S){
QLibrary myLib("fflib.dll");
typedef void (*dll_func)();
dll_func ff_calc = (dll_func) myLib.resolve("ff_calc");
double * ssrl_g = (double*)myLib.resolve("ssrl_g");
double * sssl_g = (double*)myLib.resolve("sssl_g");
double * ko_g = (double*)myLib.resolve("ko_g");
double * kp_g = (double*)myLib.resolve("kp_g");
double * q_g = (double*)myLib.resolve("q_g");
double * nt_g = (double*)myLib.resolve("nt_g");
//double * k0_g = (double*)myLib.resolve("k0_g");
double * ff0 = (double*)myLib.resolve("ff0_g");
//double * k1_g = (double*)myLib.resolve("k1_g");
double * ff1 = (double*)myLib.resolve("ff1_g");

if (ff_calc!=NULL){
*ssrl_g=m.ssl_t, *sssl_g=m.ssl_c,*ko_g=m.ko, *kp_g=m.kp, *q_g=S/m.kp, *nt_g=0.2;
ff_calc();
m.ff1=*ff1; m.ff0=*ff0;} return 0;
}
