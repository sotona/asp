#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>
#include "html_out.h"

using namespace std;

const char*table = "<table>";
const char*etable = "</table>";

const string tr = "<tr>";
const string td = "<td>";
const string etd = "</td>";
const string etr = "</tr>";

const string br = "<br>";
const string eol = "\n";

const string _ = "\t";

const string el_data_header =    "#r[rau] u[mm] ssr[MPa] sst[MPa] ssr/S sst/S eer*1000 eet*1000 Er[MPa] ss_c[MPa] sso[MPa] thetta*1000";
const string rzvar_data_header = "#r*/r_a -u_a[mm] u*[mm] p[MPa] p_ob[MPa] ss_r*[MPa] ss_t*[MPa] eer* eet* thetaz[x1000] r_k/r_a r_p/r_a f";


const string head = "<head>\n <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"> </head>";


inline string generate_indexes(unsigned n, string name){
string s=tr+td+name+etd+td+etd;
for (unsigned i=0; i<n; i++){ostringstream ss; ss <<i; s+=td+ss.str()+etd+" ";}
return s+td+etd+td+name+etd+etr;}


template <class T>
string generate_table_row(T* array, unsigned n, string name, unsigned precission=4, unsigned step=1){
string s=tr+td + name + etd+td+etd;
for (unsigned i=0; i<n; i+=step){
ostringstream ss;
ss << fixed << setprecision(precission) << array[i];
s+=td+ss.str()+etd+" ";}
s+=td+etd+td+name+etd+etr; s+="\n";
return s;}


void write_rzmrzp_values(const char *filename, const Core& c){
std::ofstream file(filename,std::ios::app);
file << "<table border=1 cellspacing=0 cellpadding=2 bgcolor=#FFFFFF bordercolor=#000000>\n";
file << generate_indexes(c.aval.n, "номер узла");
file << tr+td+etd+etr;
file << generate_table_row<double>((double*)c.aval.rzra, c.aval.n, "r*/r<sub>a</sub>");
file << generate_table_row<double>((double*)c.aval.ua,   c.aval.n, "u<sub>a</sub> (mm)");
file << generate_table_row<double>((double*)c.aval.uz,   c.aval.n, "u<sup>*</sup> (mm)");
file << generate_table_row<double>((double*)c.aval.ssrz, c.aval.n, "&sigma;<sub>r</sub>* (Mpa)");
file << generate_table_row<double>((double*)c.aval.sstz, c.aval.n, "&sigma;<sub>&theta;</sub>* (Mpa)");
file << generate_table_row<double>((double*)c.aval.sscz, c.aval.n, "&sigma;<sub>c</sub>* (Mpa)");
file << generate_table_row<double>((double*)c.aval.ttmz, c.aval.n, "&tau;<sub>m</sub>* (Mpa)");
file << generate_table_row<double>((double*)c.aval.p,    c.aval.n, "p (MPa)");
file << generate_table_row<double>((double*)c.aval.pob,  c.aval.n, "pob (MPa)");
file << generate_table_row<double>((double*)c.aval.rpra, c.aval.n, "rpra");
file << generate_table_row<double>((double*)c.aval.f,    c.aval.n, "func",13);
file << generate_table_row<double>((double*)c.aval.sso,  c.aval.n, "sso",5);
file << generate_table_row<double>((double*)c.aval.theta,c.aval.n, "ttp",5);

unsigned precission = 10;
string name = "Er*/Em";
string s=tr+td + name + etd+td+etd;
for (unsigned i=0; i<c.aval.n; i++){
ostringstream ss;
ss << fixed << setprecision(precission) << c.aval.Er_z[i];
s+=td+ss.str()+etd+" ";}
s+=td+etd+td+name+etd+etr; s+="\n";
file << s;

name = "&sigma;<sub>сж</sub>*/ &sigma; <sub>сж. лаб</sub>";
s=tr+td + name + etd+td+etd;
for (unsigned i=0; i<c.aval.n; i++){
ostringstream ss;
ss << fixed << setprecision(precission) << c.aval.ss_c_z[i];
s+=td+ss.str()+etd+" ";}
s+=td+etd+td+name+etd+etr; s+="\n";
file << s;

file << "</table>\n";
file.close();
}


void write_el_values(const char *filename, const Core& c){
std::ofstream file(filename,std::ios::app);
string name;
double Ra = c.shaft.param.Ra;
OneDGrid::N_Type i=0;
const Region<DataGrid> &elr=c.shaft.elastic;
file << "<table border=1 cellspacing=0 cellpadding=2 bgcolor=#FFFFFF bordercolor=#000000>";
file << generate_indexes(elr.grid.n(), "номер узла");
file << tr+td+etd+etr;

name = "r<sub>i</sub>/r<sub>a</sub>";
file << tr<<td<<name<<etd<<td<<etd;
for(i=0;i<elr.grid.n();i++)file<<td<<elr.grid.r(i)/Ra<<etd+" ";
file << td<<etd<<td<< name <<etd<<etr<<"\n";
name = "r<sub>i (mm)";
file << tr<<td<< name <<etd<<td<<etd;for(i=0;i<elr.grid.n();i++)file<<td<<elr.grid.r(i)<<etd+" ";
file << td<<etd<<td<< name <<etd<<etr<<"\n";

file << tr+td+etd+etr;

file << generate_table_row<double>((double*)elr.detail.u,elr.grid.n(),    "u (mm)",10);
file << generate_table_row<double>((double*)elr.detail.ssr,elr.grid.n(),  "&sigma;<sub>r</sub> (Mpa)", 10);
file << generate_table_row<double>((double*)elr.detail.sst,elr.grid.n(),  "&sigma;<sub>&theta;</sub> (Mpa)", 10);
file << generate_table_row<double>((double*)elr.detail.eer,elr.grid.n(),  "&epsilon;<sub>r</sub>", 10);
file << generate_table_row<double>((double*)elr.detail.eet,elr.grid.n(),  "&epsilon;<sub>&theta;</sub>", 10);
//file << generate_table_row<double>((double*)elr.detail.res_k,elr.grid.n(),"Р“Р„Р“В®Р“Р…Р“Т‘. Р“В§Р“В Р“Р‡Р“В Р“В±Р“В ", 10);

unsigned precission = 10;
name = "Er/Em";
string s=tr+td + name + etd+td+etd;
for (OneDGrid::N_Type i=0; i<elr.grid.n(); i++){
ostringstream ss;
ss << fixed << setprecision(precission) << elr.detail.Er[i]/c.shaft.media.Em_p;
s+=td+ss.str()+etd+" ";}
s+=td+etd+td+name+etd+etr; s+="\n";
file << s;

name = "&sigma;<sub>сж</sub><sup>i</sup>/&sigma;<sub>сж.лаб</sub>";
s=tr+td + name + etd+td+etd;
for (OneDGrid::N_Type i=0; i<elr.grid.n(); i++){
ostringstream ss;
ss << fixed << setprecision(precission) << elr.detail.ss_c[i]/c.shaft.media.ssm;
s+=td+ss.str()+etd+" ";}
s+=td+etd+td+name+etd+etr; s+="\n";
file << s;

file << "</table>\n";

file << "f = "<< c.f <<endl;

float dr = 0.1;
//float r_max = 7.0;
float Dr = (c.shaft.elastic.grid.rb()/Ra - c.shaft.elastic.grid.ra()/Ra)/c.shaft.elastic.grid.n();
OneDGrid::N_Type step = dr/Dr;
OneDGrid::N_Type n = step*100;
file << "сокращённые массивы" << br<<eol;
file << "<table border=1 cellspacing=0 cellpadding=2 bgcolor=#FFFFFF bordercolor=#000000>";
name = "r<sub>i</sub>/r<sub>a</sub>";
file << tr<<td<<name<<etd<<td<<etd;
/* сокращённые массивы значений */
for(i=0;i<n;i+=step)file<<td<<elr.grid.r(i)/Ra<<etd+" ";
file << td<<etd<<td<< name <<etd<<etr<<"\n";
file << tr+td+etd+etr;
file << generate_table_row<double>((double*)elr.detail.u,   n,    "u (mm)",10,step);
file << generate_table_row<double>((double*)elr.detail.ssr, n,    "&sigma;<sub>r</sub> (Mpa)", 10,step);
file << generate_table_row<double>((double*)elr.detail.sst, n,    "&sigma;<sub>&theta;</sub> (Mpa)", 10,step);
file << "</table>\n";

file.close();
}

void write_bk_values(const char *filename, const Core& c){
std::ofstream file(filename,std::ios::app);
double Ra = c.shaft.param.Ra;
OneDGrid::N_Type i;
const Region<DataGrid> &bak=c.shaft.bak;
file << "<table border=1 cellspacing=0 cellpadding=2 bgcolor=#FFFFFF bordercolor=#000000>";
file << generate_indexes(bak.grid.n(), "индексы");
file << tr+td+etd+etr;
file << tr<<td<<"радиус (r<sub>i</sub>/r<sub>a</sub>)"<<etd<<td<<etd;
		for(i=0;i<bak.grid.n();i++)file<<td<<bak.grid.r(i)/Ra<<etd+" ";
file << td<<etd<<td<<"радиус (r<sub>i</sub>/r<sub>a</sub>)"<<etd<<etr<<"\n";
file << tr<<td<<"радиус (mm)"<<etd<<td<<etd;
		for(i=0;i<bak.grid.n();i++)file<<td<<bak.grid.r(i)<<etd+" ";
file << td<<etd<<td<<"радиус (mm)"<<etd<<etr<<"\n";
file << tr+td+etd+etr;
file << generate_table_row<double>((double*)bak.detail.u,bak.grid.n(),    "u (mm)",10);
file << generate_table_row<double>((double*)bak.detail.ssr,bak.grid.n(),  "&sigma;<sub>r</sub> (Mpa)", 10);
file << generate_table_row<double>((double*)bak.detail.sst,bak.grid.n(),  "&sigma;<sub>&theta;</sub> (Mpa)", 10);
file << "</table>\n";
file.close();
}

void write_media_values(std::ofstream &file, const Core& c){
double S = c.shaft.param.S;
file << "E<sub>lab</sub> = "<<c.shaft.media.Elab<<", E<sub>m</sub> = "<<c.shaft.media.Em<<
     ", E<sub>m</sub><sup>plain</sup>= "<<c.shaft.media.Em_p<<br<<eol;
file << "&mu; = "<<c.shaft.media.mm<<", &mu;<sup>plain</sup> = "<<c.shaft.media.mm_p<<br;
file << "k <sub>o</sub> = "<<c.shaft.media.ko<<", "<< "k <sub>п</sub> = "<<c.shaft.media.kp<<br<<eol;
file << "S = "<<S<<" MPa"<<br<<eol;
file << "&sigma;<sub>lab</sub> = "<<c.shaft.media.ssl_c<<" MPa"<<", &sigma;<sub>m</sub> = "<<c.shaft.media.ssm<<"<br>"<<eol;
file << "E(r) = E<sub>m</sub>(1-"<<c.shaft.media.db_param.a<<"*r<sup>"<<c.shaft.media.db_param.n<<"</sup>)<br>";
file << "&sigma; (r) = &sigma;<sub>m</sub>(1-"<<c.shaft.media.db_param.b<<"*r<sup>"<<c.shaft.media.db_param.k<<"</sup>)<br>";
//simple_elr.detailtrictions(log);
file << "&beta; = "<<c.shaft.media.bb;
//file << ", b<sub>1</sub> = "<<c.shaft.media.db_param.b<<br<<eol;
}


void warite_other_values(std::ofstream &file, const Core& c){
file << "r* = ("<<c.rz_range.ra()<<", "<<c.rz_range.rb()<<")"<<br<<eol;
file << "n<sub>r*</sub> = "<<c.rz_range.n()<<br<<eol;
}


void general_output(const char* filename, const Core& c, bool r8){
std::ofstream file(filename);
const Region<DataGrid> &elr=c.shaft.elastic;
const Region<DataGrid> &plr=c.shaft.plastic1;
double Ra = c.shaft.param.Ra,
	   Rb = c.shaft.param.Rb;
file << "<html>" << head << "<body>";
file << "<table>"<<tr<<td;
file << "<b>Параметры сетки</b>" <<br<<eol;
file << "r<sub>a</sub> = " << Ra<<" mm<br>"<<"  r<sub>b</sub> = "<<Rb<<" mm"<<"<br><br>";
file << "n<sub>1</sub> = " << elr.grid.n() << br<<eol;
file << "n<sub>2</sub> = " << plr.grid.n() << br<<etd<<eol;
file << td << "<b>Параметры среды</b>" <<br<<eol;
write_media_values(file,c); file<< etd<<etr;
file << "</table>";
file << "решение для R*: ";
if (r8) file << "аналитическое"; else file << "из упругой зоны";
file <<br<<eol;

//file << "ff = ("<<elr.detail.ssr[elr.grid.n()]<<" + S)<sup>2</sup> + "<<
//        "("<<elr.detail.sst[1]<<" - &beta;*("<<elr.detail.ssr[1]<<") + "<<elr.detail.ss_c[1]<<
//        ")<sup>2</sup> = "<<ff<< "<br><br>";
file << "<hr>"<<endl;
//file << "<h4>1</h4><br>" << std::endl;
file << "r*/r<sub>a</sub> = "<<elr.grid.ra()<<br<<eol;
file << "<img src=u_chart.png><img src=ssr_chart.png><img src=sst_chart.png><img src=elr.detailk_chart.png>";
file << "<hr><b>Графики давления на контуре выработки, давления обрушения; размеров предельно разрушеной и предельно разрыхлённой зон </b> <br>" << std::endl;
file << "<img src=2_chart.png><img src=3_chart.png>";
if (c.aval.pp_i) {file << "<br>пересечения давления на контуре выработки и давления обрушения: "<<c.aval.pp_val*1000.<<" мм ";}
else {file << "<br>пересечения давления на контуре выработки и давления обрушения нет<br><hr>";}
//file << "<br>6 r* = "<<c.shaft.elastic.grid.ra()<<" Р“В¬Р“В¬<br>";
file.close();
write_el_values(filename,c);
file.open(filename,std::ios::app);file << "<br>таблица значений для различных r*<br>";file.close();
write_rzmrzp_values(filename,c);
file.open(filename,std::ios::app); file << "</body></html>";
//write_for_freeMat();
}


/// create html report for elestic region\problem
void elastic_report(const char* filename, const Core& c){
std::ofstream file(filename);
const Region<DataGrid> &elr=c.shaft.elastic;
const Region<DataGrid> &plr=c.shaft.plastic1;
double Ra = c.shaft.param.Ra,
	   Rb = c.shaft.param.Rb;
file << "<html>" << head << "<body>";

file << "<table>"<<tr<<td;
file << "<b>параметры сетки</b>" <<br<<eol;
file << "r<sub>a</sub> = " << Ra<<" mm<br>"<<"  r<sub>b</sub> = "<<Rb<<" mm"<<"<br><br>";
file << "n<sub>1</sub> = " << elr.grid.n() << br<<eol;
file << "n<sub>2</sub> = " << plr.grid.n() << br<<etd<<eol;
file << td << "<b>Параметры среды</b>" <<br<<eol;
write_media_values(file,c); file<< etd<<etr;
file << "</table>";

file << "<hr>"<<endl;
file << "<h4>Графики для упругой зоны</h4><br>" << std::endl;
file << "r*/r<sub>a</sub> = "<<elr.grid.ra()<<br<<eol;
file << "<img src=u_chart.png><img src=ssr_chart.png><img src=sst_chart.png><img src=elr.detailk_chart.png>";
file.close();
write_el_values(filename,c); file.open(filename,std::ios::app);
file <<br<< "Баклашов" <<br<<eol; file.close();
write_bk_values(filename,c);}


inline void write_data_line(std::ofstream &file, const Region<DataGrid>& r, unsigned i, const main_param& mp){
file << r.grid.r(i)/mp.Ra << _ << r.detail.u[i] << _ <<
        r.detail.ssr[i]  << _ << r.detail.sst[i] << _ <<
        r.detail.ssr[i]/mp.S  << _ << r.detail.sst[i]/mp.S << _ <<
        r.detail.eer[i]*1000  << _ << r.detail.eet[i]*1000 << _ << r.detail.Er[i] << _<<
     r.detail.ss_c[i] << _ << r.detail.sso[i] << _ << r.detail.thetta[i]*1000;}


/// write r*, ua, u*, p, p_ob, ... to file @param file
inline void write_data_line_rz(std::ofstream &file, const RPdata& a, unsigned i){
file << a.rzra[i] << _ << -a.ua[i] << _<< a.uz[i]<<_<<a.p[i]<<_<<a.pob[i]<<_
 << a.ssrz[i]  << _<< a.sstz[i] << _<< a.eerz[i] << _<< a.eetz[i] << _<< a.theta[i]*1000
 << _<< a.rkra[i] << _<< a.rpra[i] <<_<< a.f[i];}


/// save all arrays for elastic problem/region to file
void save_data_region(const char* filename, const Region<DataGrid>& reg, const main_param& p){
std::ofstream file(filename);
const OneDGrid & g = reg.grid;
file <<el_data_header<<endl;
for (unsigned i=0; i<g.n(); i+=2){
write_data_line(file,reg,i,p); //write_data_line(file,c.shaft.bak,i,c.shaft.param);
file << endl;}
file.close();}


/// save all arrays for elastic problem/region to file @param filename
void save_data_ep(const char* filename, const Region<DataGrid> &pl, const Region<DataGrid> &el,
                                                                        const main_param& p){
std::ofstream file(filename);
const OneDGrid & eg = el.grid;
const OneDGrid & pg = pl.grid;
file <<el_data_header<<endl;
for (OneDGrid::N_Type i=0; i<pg.n(); i+=1){
write_data_line(file,pl,i,p); //write_data_line(file,c.shaft.bak,i,c.shaft.param);
file << endl;}
for (OneDGrid::N_Type i=0; eg.rau(i)<=10.0 && i<eg.n(); i+=1){
write_data_line(file,el,i,p); //write_data_line(file,c.shaft.bak,i,c.shaft.param);
file << endl;}
file.close();}


/// write r* values arrays (ua, p, pob, ... for various r*) to @param filename
void save_data_rzvar(const char* filename, const Core& c){
std::ofstream file(filename);
const RPdata &aval = c.aval;
file <<rzvar_data_header<<endl;
for (unsigned i=0; i<aval.n; i++){
write_data_line_rz(file,aval,i);
file << endl;}
file.close();}


