#-------------------------------------------------
#
# Project created by QtCreator 2012-05-26T03:45:43
#
#-------------------------------------------------

QT = core gui widgets
#QT += opengl
TARGET = asp
TEMPLATE = app

HEADERS  += mainwindow.h \
    calculation.h \
    eng/bur_r.h \
    html_out.h \
    cm/multidimensional_opt.h \
    cm/cm.h \
    cm/unidimensional_opt.h \
    eng/elastic.h \
    eng/plastic.h \
    eng/general.h \
    auxiliary.h \
    shoring.h

SOURCES += main.cpp\
        mainwindow.cpp \
    calculation.cpp \
    html_out.cpp \
    cm/multidimensional_opt.cpp \
    cm/cm.cpp \
    cm/unidimensional_opt.cpp \
    eng/elastic.cpp \
    eng/plastic.cpp \
    eng/general.cpp \
    eng/bur_r.cpp \
    cm/g.cpp \
    auxiliary.cpp \
    shoring.cpp

FORMS    += mainwindow.ui

LIBS+= -lstdc++

win32:CONFIG(release, debug|release):    LIBS += -L$$PWD/../qwt/lib/ -lqwt
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../qwt/lib/ -lqwtd
else:unix: LIBS += -L$$PWD/../qwt/lib/ -lqwt

INCLUDEPATH += $$PWD/../qwt
DEPENDPATH += $$PWD/../qwt

OTHER_FILES += \
    ../icon.png

# + windows plugin dll

