#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFutureWatcher>
#include <QGraphicsScene>
#include <../qwt/src/qwt_plot_curve.h>
#include <../qwt/src/qwt_plot_grid.h>
#include "calculation.h"
#include "auxiliary.h"


const QString err_no_shor_file = "Программа не сможет дать рекомендации по креплению выработок, "\
                                "ибо файл с описанием крепей '%1' не найден в каталоге с программой";
const QString err_load_param = "Ошибка загрузки параметров из файла '%1'";
const QString rz_labels[] =  {"P (kPa)","P_ob (kPa)", "r_z/r_a*1000","r_z/r_a", "r_p/r_a", "r_k/r_a"};  ///< labels for r* plots


namespace Files{
const QString shorings = "shoring.txt";
const QString user_manual = "manual.pdf";}


namespace ShorState {
const QString ruined = "будет разрушена";
const QString solid = "не разрушется";
const QString possible_ruined = "может быть разрушение";
}

const unsigned char rzg_N=6; ///< count of rz graphics
const unsigned char plot_N = 6;

namespace Ui {class MainWindow;}

class MainWindow : public QMainWindow{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    bool load_params(const QString filename);
    ~MainWindow();
private slots:
    void init_plot_curves();
    void init_plot_markers();
    void on_SpinBox_rze_valueChanged(double arg1);
	void on_button_calc_e_clicked();					///< calculate eastic region problem
    void on_button_calc_rp_clicked();					///< calculate for various r*

	void on_SpinBox_rb__valueChanged(double arg1);
	void update_el_step();

    void on_action_paramload_triggered();
    void on_action_paramsave_triggered();
    void on_action_openel_report_triggered();
    void on_action_open_ep_report_triggered();

    void get_params();
    void init_core();
    void draw_rp_graphics();
    void draw_ep_graphics(const Region<DataGrid> *pl, const Region<DataGrid> *el);
    void draw_elastic_list_graphics(vector<Region<DataGrid> > &reg);
    void save_graphics();
    void updateTable_rad(const Region<DataGrid> &pl);
    void updateTable_RP();

    void display_e_results();
    void display_ep_results();
    void display_rp_results();
    void display_cem_results();

    void on_comboBox_calc_method_currentIndexChanged(int index);

    void on_button_calc_ep_clicked(); ///< calc rock pressure for specific r*
    void on_button_calc_ep_cem_clicked();
    void on_spinBox_eps_n_valueChanged(int arg1);
    void on_checkBox_elproblem_stateChanged(int arg1);
    void on_checkBox_burr_stateChanged(int arg1);
    void on_pushButton_delta_clicked();
    void on_SpinBox_rcem_valueChanged(double arg1);
    void on_action_src_triggered();
    void on_action_pub_triggered();
    void on_pushButton_clear_table_clicked();
    void on_action_triggered();
    void on_checkBox_calcff_toggled(bool checked);
    void on_comboBox_strength_currentIndexChanged(int index);
    void show_shorings();
    void on_action_manual_triggered();
    void on_action_more_param_triggered();
    void widthtoRa(double w);
    void ratoWidth(double ra);
    void on_action_open_shorings_triggered();
    void on_SpinBox_ssl_c_valueChanged(double arg1);
    void on_actionGnuplot_triggered();

private:
    void setCalwaitState(); /// set calculation wait state of form
    void restoresFormState();    /// restore normal window state (after CalwaitState)

private:
    Ui::MainWindow *ui;

    struct{
        QFuture<double> future_e;
        QFutureWatcher<double> watcher_e; ///< control the e calc thread
        QFuture<void> future_ep;
        QFutureWatcher<void> watcher_ep; ///< control the e calc thread
        QFuture<void> future_rp;
        QFutureWatcher<void> watcher_rp; ///< control the e calc thread
        QFuture<void> future_cem;
        QFutureWatcher<void> watcher_cem; ///< control the e calc thread
        } parallel;
    /*фигня для графиков*/
    // для двух циклов вычислений
    struct {
        QwtPlotCurve *curve_u[2];     // перемещения
        QwtPlotCurve *curve_ssr[2];
        QwtPlotCurve *curve_sst[2];
        QwtPlotCurve *curve_fc[2];      // fc - factor of safety
        QwtPlotCurve *curve_k[2];
        QwtPlotCurve *curve[rzg_N];     ///< curves for rock pressure problem

        PlotStaff *plot_staff[plot_N]; ///< разные штуки для виджета с графиками

        struct{
            QwtPlotMarker *Pn,  *pxp, *uw, *ssr0, *sst0, *u0, *p_max, *p_min, *rz1, *rz2, *rz3,*k0, *first_p, *last_p, *p_uw;
            struct {
                static const unsigned Nmm = 3;			///< count of el_markers (second index)
                static const unsigned Nm = 3;			///< count of el_markers
                static const unsigned umark=0;		    ///< first indexes for el_markers
                static const unsigned ssrmark=1;
                static const unsigned sstmark=2;
                QwtPlotMarker ***elMarks;} plotMarks;       ///< marks for points 1.ra, 2.ra, ... for plots u,sst, ssr
            } markers;

        bool show_pminmax;
        bool run;
    } plot;

    QString title; // used as temporary storage
    Core *core;
    unsigned RP_calc_n; // how much RP button pressed

    QString last_datafile;  ///< last saved file with gnuplotable data

};

#endif // MAINWINDOW_H
