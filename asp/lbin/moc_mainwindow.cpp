/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.3.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../src/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.3.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[58];
    char stringdata[1208];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10),
QT_MOC_LITERAL(1, 11, 16),
QT_MOC_LITERAL(2, 28, 0),
QT_MOC_LITERAL(3, 29, 17),
QT_MOC_LITERAL(4, 47, 27),
QT_MOC_LITERAL(5, 75, 4),
QT_MOC_LITERAL(6, 80, 24),
QT_MOC_LITERAL(7, 105, 25),
QT_MOC_LITERAL(8, 131, 27),
QT_MOC_LITERAL(9, 159, 14),
QT_MOC_LITERAL(10, 174, 29),
QT_MOC_LITERAL(11, 204, 29),
QT_MOC_LITERAL(12, 234, 33),
QT_MOC_LITERAL(13, 268, 34),
QT_MOC_LITERAL(14, 303, 10),
QT_MOC_LITERAL(15, 314, 9),
QT_MOC_LITERAL(16, 324, 16),
QT_MOC_LITERAL(17, 341, 16),
QT_MOC_LITERAL(18, 358, 23),
QT_MOC_LITERAL(19, 382, 2),
QT_MOC_LITERAL(20, 385, 2),
QT_MOC_LITERAL(21, 388, 26),
QT_MOC_LITERAL(22, 415, 26),
QT_MOC_LITERAL(23, 442, 3),
QT_MOC_LITERAL(24, 446, 13),
QT_MOC_LITERAL(25, 460, 15),
QT_MOC_LITERAL(26, 476, 16),
QT_MOC_LITERAL(27, 493, 14),
QT_MOC_LITERAL(28, 508, 17),
QT_MOC_LITERAL(29, 526, 18),
QT_MOC_LITERAL(30, 545, 18),
QT_MOC_LITERAL(31, 564, 19),
QT_MOC_LITERAL(32, 584, 43),
QT_MOC_LITERAL(33, 628, 5),
QT_MOC_LITERAL(34, 634, 25),
QT_MOC_LITERAL(35, 660, 29),
QT_MOC_LITERAL(36, 690, 29),
QT_MOC_LITERAL(37, 720, 34),
QT_MOC_LITERAL(38, 755, 29),
QT_MOC_LITERAL(39, 785, 27),
QT_MOC_LITERAL(40, 813, 28),
QT_MOC_LITERAL(41, 842, 23),
QT_MOC_LITERAL(42, 866, 23),
QT_MOC_LITERAL(43, 890, 33),
QT_MOC_LITERAL(44, 924, 19),
QT_MOC_LITERAL(45, 944, 26),
QT_MOC_LITERAL(46, 971, 7),
QT_MOC_LITERAL(47, 979, 40),
QT_MOC_LITERAL(48, 1020, 13),
QT_MOC_LITERAL(49, 1034, 26),
QT_MOC_LITERAL(50, 1061, 30),
QT_MOC_LITERAL(51, 1092, 9),
QT_MOC_LITERAL(52, 1102, 1),
QT_MOC_LITERAL(53, 1104, 9),
QT_MOC_LITERAL(54, 1114, 2),
QT_MOC_LITERAL(55, 1117, 33),
QT_MOC_LITERAL(56, 1151, 29),
QT_MOC_LITERAL(57, 1181, 26)
    },
    "MainWindow\0init_plot_curves\0\0"
    "init_plot_markers\0on_SpinBox_rze_valueChanged\0"
    "arg1\0on_button_calc_e_clicked\0"
    "on_button_calc_rp_clicked\0"
    "on_SpinBox_rb__valueChanged\0update_el_step\0"
    "on_action_paramload_triggered\0"
    "on_action_paramsave_triggered\0"
    "on_action_openel_report_triggered\0"
    "on_action_open_ep_report_triggered\0"
    "get_params\0init_core\0draw_rp_graphics\0"
    "draw_ep_graphics\0const Region<DataGrid>*\0"
    "pl\0el\0draw_elastic_list_graphics\0"
    "vector<Region<DataGrid> >&\0reg\0"
    "save_graphics\0updateTable_rad\0"
    "Region<DataGrid>\0updateTable_RP\0"
    "display_e_results\0display_ep_results\0"
    "display_rp_results\0display_cem_results\0"
    "on_comboBox_calc_method_currentIndexChanged\0"
    "index\0on_button_calc_ep_clicked\0"
    "on_button_calc_ep_cem_clicked\0"
    "on_spinBox_eps_n_valueChanged\0"
    "on_checkBox_elproblem_stateChanged\0"
    "on_checkBox_burr_stateChanged\0"
    "on_pushButton_delta_clicked\0"
    "on_SpinBox_rcem_valueChanged\0"
    "on_action_src_triggered\0on_action_pub_triggered\0"
    "on_pushButton_clear_table_clicked\0"
    "on_action_triggered\0on_checkBox_calcff_toggled\0"
    "checked\0on_comboBox_strength_currentIndexChanged\0"
    "show_shorings\0on_action_manual_triggered\0"
    "on_action_more_param_triggered\0widthtoRa\0"
    "w\0ratoWidth\0ra\0on_action_open_shorings_triggered\0"
    "on_SpinBox_ssl_c_valueChanged\0"
    "on_actionGnuplot_triggered"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      45,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  239,    2, 0x08 /* Private */,
       3,    0,  240,    2, 0x08 /* Private */,
       4,    1,  241,    2, 0x08 /* Private */,
       6,    0,  244,    2, 0x08 /* Private */,
       7,    0,  245,    2, 0x08 /* Private */,
       8,    1,  246,    2, 0x08 /* Private */,
       9,    0,  249,    2, 0x08 /* Private */,
      10,    0,  250,    2, 0x08 /* Private */,
      11,    0,  251,    2, 0x08 /* Private */,
      12,    0,  252,    2, 0x08 /* Private */,
      13,    0,  253,    2, 0x08 /* Private */,
      14,    0,  254,    2, 0x08 /* Private */,
      15,    0,  255,    2, 0x08 /* Private */,
      16,    0,  256,    2, 0x08 /* Private */,
      17,    2,  257,    2, 0x08 /* Private */,
      21,    1,  262,    2, 0x08 /* Private */,
      24,    0,  265,    2, 0x08 /* Private */,
      25,    1,  266,    2, 0x08 /* Private */,
      27,    0,  269,    2, 0x08 /* Private */,
      28,    0,  270,    2, 0x08 /* Private */,
      29,    0,  271,    2, 0x08 /* Private */,
      30,    0,  272,    2, 0x08 /* Private */,
      31,    0,  273,    2, 0x08 /* Private */,
      32,    1,  274,    2, 0x08 /* Private */,
      34,    0,  277,    2, 0x08 /* Private */,
      35,    0,  278,    2, 0x08 /* Private */,
      36,    1,  279,    2, 0x08 /* Private */,
      37,    1,  282,    2, 0x08 /* Private */,
      38,    1,  285,    2, 0x08 /* Private */,
      39,    0,  288,    2, 0x08 /* Private */,
      40,    1,  289,    2, 0x08 /* Private */,
      41,    0,  292,    2, 0x08 /* Private */,
      42,    0,  293,    2, 0x08 /* Private */,
      43,    0,  294,    2, 0x08 /* Private */,
      44,    0,  295,    2, 0x08 /* Private */,
      45,    1,  296,    2, 0x08 /* Private */,
      47,    1,  299,    2, 0x08 /* Private */,
      48,    0,  302,    2, 0x08 /* Private */,
      49,    0,  303,    2, 0x08 /* Private */,
      50,    0,  304,    2, 0x08 /* Private */,
      51,    1,  305,    2, 0x08 /* Private */,
      53,    1,  308,    2, 0x08 /* Private */,
      55,    0,  311,    2, 0x08 /* Private */,
      56,    1,  312,    2, 0x08 /* Private */,
      57,    0,  315,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 18, 0x80000000 | 18,   19,   20,
    QMetaType::Void, 0x80000000 | 22,   23,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 26,   19,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   33,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   46,
    QMetaType::Void, QMetaType::Int,   33,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,   52,
    QMetaType::Void, QMetaType::Double,   54,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,    5,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->init_plot_curves(); break;
        case 1: _t->init_plot_markers(); break;
        case 2: _t->on_SpinBox_rze_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 3: _t->on_button_calc_e_clicked(); break;
        case 4: _t->on_button_calc_rp_clicked(); break;
        case 5: _t->on_SpinBox_rb__valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 6: _t->update_el_step(); break;
        case 7: _t->on_action_paramload_triggered(); break;
        case 8: _t->on_action_paramsave_triggered(); break;
        case 9: _t->on_action_openel_report_triggered(); break;
        case 10: _t->on_action_open_ep_report_triggered(); break;
        case 11: _t->get_params(); break;
        case 12: _t->init_core(); break;
        case 13: _t->draw_rp_graphics(); break;
        case 14: _t->draw_ep_graphics((*reinterpret_cast< const Region<DataGrid>*(*)>(_a[1])),(*reinterpret_cast< const Region<DataGrid>*(*)>(_a[2]))); break;
        case 15: _t->draw_elastic_list_graphics((*reinterpret_cast< vector<Region<DataGrid> >(*)>(_a[1]))); break;
        case 16: _t->save_graphics(); break;
        case 17: _t->updateTable_rad((*reinterpret_cast< const Region<DataGrid>(*)>(_a[1]))); break;
        case 18: _t->updateTable_RP(); break;
        case 19: _t->display_e_results(); break;
        case 20: _t->display_ep_results(); break;
        case 21: _t->display_rp_results(); break;
        case 22: _t->display_cem_results(); break;
        case 23: _t->on_comboBox_calc_method_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 24: _t->on_button_calc_ep_clicked(); break;
        case 25: _t->on_button_calc_ep_cem_clicked(); break;
        case 26: _t->on_spinBox_eps_n_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 27: _t->on_checkBox_elproblem_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 28: _t->on_checkBox_burr_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 29: _t->on_pushButton_delta_clicked(); break;
        case 30: _t->on_SpinBox_rcem_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 31: _t->on_action_src_triggered(); break;
        case 32: _t->on_action_pub_triggered(); break;
        case 33: _t->on_pushButton_clear_table_clicked(); break;
        case 34: _t->on_action_triggered(); break;
        case 35: _t->on_checkBox_calcff_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 36: _t->on_comboBox_strength_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 37: _t->show_shorings(); break;
        case 38: _t->on_action_manual_triggered(); break;
        case 39: _t->on_action_more_param_triggered(); break;
        case 40: _t->widthtoRa((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 41: _t->ratoWidth((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 42: _t->on_action_open_shorings_triggered(); break;
        case 43: _t->on_SpinBox_ssl_c_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 44: _t->on_actionGnuplot_triggered(); break;
        default: ;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, 0, 0}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 45)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 45;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 45)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 45;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
