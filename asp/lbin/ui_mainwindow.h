/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.3.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "../qwt/src/qwt_plot.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *action_paramload;
    QAction *action_paramsave;
    QAction *action_openel_report;
    QAction *action_open_ep_report;
    QAction *action_html_report;
    QAction *action_datafile;
    QAction *action111;
    QAction *action_src;
    QAction *action_pub;
    QAction *action;
    QAction *action_manual;
    QAction *action_more_param;
    QAction *action_open_shorings;
    QAction *actionGnuplot;
    QWidget *centralWidget;
    QGridLayout *gridLayout_2;
    QTabWidget *tabWidget;
    QWidget *tab_param;
    QVBoxLayout *verticalLayout_17;
    QTabWidget *tabWidget_params;
    QWidget *elastic;
    QHBoxLayout *horizontalLayout_25;
    QVBoxLayout *verticalLayout_24;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_18;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label;
    QSpinBox *spinBox_N;
    QLabel *label_5;
    QLabel *label_step;
    QSpacerItem *horizontalSpacer_3;
    QHBoxLayout *horizontalLayout_6;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_2;
    QDoubleSpinBox *spinBox_width;
    QFrame *line;
    QGridLayout *gridLayout;
    QLabel *label_3;
    QLabel *label_32;
    QDoubleSpinBox *SpinBox_rb_;
    QLabel *label_rb_mm;
    QDoubleSpinBox *SpinBox_ra;
    QLabel *label_8;
    QHBoxLayout *horizontalLayout_42;
    QLabel *label_33;
    QDoubleSpinBox *SpinBox_rze;
    QLabel *label_rzval;
    QSpacerItem *horizontalSpacer_5;
    QHBoxLayout *horizontalLayout_38;
    QLabel *label_700;
    QDoubleSpinBox *SpinBox_rcem;
    QSpacerItem *horizontalSpacer_131;
    QCheckBox *checkBox_elproblem;
    QGroupBox *groupBox_sc;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_35;
    QComboBox *comboBox_strength;
    QSpacerItem *horizontalSpacer_10;
    QGroupBox *groupBox_6;
    QHBoxLayout *horizontalLayout_10;
    QDoubleSpinBox *SpinBox_c0;
    QDoubleSpinBox *SpinBox_c1;
    QDoubleSpinBox *SpinBox_c2;
    QSpacerItem *horizontalSpacer_7;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_20;
    QHBoxLayout *horizontalLayout_20;
    QComboBox *comboBox_calc_method;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_37;
    QComboBox *comboBox_el_method;
    QSpacerItem *horizontalSpacer_14;
    QGroupBox *groupBox_4;
    QHBoxLayout *horizontalLayout_7;
    QVBoxLayout *verticalLayout_7;
    QLabel *label_6;
    QDoubleSpinBox *SpinBox_rzmin;
    QVBoxLayout *verticalLayout_8;
    QLabel *label_12;
    QDoubleSpinBox *SpinBox_rzmax;
    QVBoxLayout *verticalLayout_22;
    QLabel *label_31;
    QSpinBox *spinBox_rzn;
    QSpacerItem *horizontalSpacer_8;
    QVBoxLayout *verticalLayout_21;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_3;
    QCheckBox *checkBox_burr;
    QLabel *label_Er;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_4;
    QDoubleSpinBox *SpinBox_Elab;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_10;
    QDoubleSpinBox *SpinBox_mu;
    QHBoxLayout *horizontalLayout_36;
    QLabel *label_34;
    QDoubleSpinBox *SpinBox_P;
    QHBoxLayout *horizontalLayout_22;
    QLabel *label_16;
    QDoubleSpinBox *SpinBox_S;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_30;
    QSpacerItem *horizontalSpacer_6;
    QDoubleSpinBox *SpinBox_kp;
    QHBoxLayout *horizontalLayout_27;
    QLabel *label_19;
    QDoubleSpinBox *SpinBox_ssl_c;
    QHBoxLayout *horizontalLayout_34;
    QLabel *label_35;
    QDoubleSpinBox *SpinBox_ssl_t;
    QHBoxLayout *horizontalLayout_28;
    QLabel *label_20;
    QDoubleSpinBox *SpinBox_ko;
    QSpacerItem *verticalSpacer_6;
    QSpacerItem *verticalSpacer_2;
    QSplitter *splitter;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout;
    QLabel *label_11;
    QSpinBox *spinBox_eps_n;
    QLabel *label_eps;
    QSpacerItem *horizontalSpacer_11;
    QSpacerItem *horizontalSpacer_15;
    QWidget *other;
    QHBoxLayout *horizontalLayout_8;
    QVBoxLayout *verticalLayout_23;
    QHBoxLayout *horizontalLayout_33;
    QLabel *label_13;
    QDoubleSpinBox *SpinBox_thetta;
    QHBoxLayout *horizontalLayout_31;
    QLabel *label_15;
    QDoubleSpinBox *SpinBox_gg;
    QCheckBox *checkBox_calcff;
    QHBoxLayout *horizontalLayout_30;
    QLabel *label_18;
    QDoubleSpinBox *SpinBox_ff0;
    QHBoxLayout *horizontalLayout_29;
    QLabel *label_17;
    QDoubleSpinBox *SpinBox_ff1;
    QHBoxLayout *horizontalLayout_15;
    QLabel *label_23;
    QDoubleSpinBox *SpinBox_upr;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_22;
    QDoubleSpinBox *SpinBox_ttk;
    QHBoxLayout *horizontalLayout_21;
    QLabel *label_26;
    QDoubleSpinBox *SpinBox_ssgp;
    QSpacerItem *verticalSpacer;
    QVBoxLayout *verticalLayout_16;
    QHBoxLayout *horizontalLayout_16;
    QLabel *label_29;
    QDoubleSpinBox *SpinBox_ku;
    QHBoxLayout *horizontalLayout_17;
    QLabel *label_28;
    QDoubleSpinBox *SpinBox_kd;
    QHBoxLayout *horizontalLayout_18;
    QLabel *label_27;
    QDoubleSpinBox *SpinBox_ke;
    QHBoxLayout *horizontalLayout_23;
    QLabel *label_25;
    QDoubleSpinBox *SpinBox_uw;
    QHBoxLayout *horizontalLayout_24;
    QLabel *label_24;
    QDoubleSpinBox *SpinBox_Pn;
    QHBoxLayout *horizontalLayout_26;
    QLabel *label_21;
    QDoubleSpinBox *SpinBox_km;
    QHBoxLayout *horizontalLayout_32;
    QLabel *label_14;
    QDoubleSpinBox *SpinBox_Br;
    QSpacerItem *verticalSpacer_4;
    QSpacerItem *horizontalSpacer;
    QWidget *tab_graphics;
    QHBoxLayout *horizontalLayout_2;
    QTabWidget *tabWidget_graphics;
    QWidget *tab_e;
    QVBoxLayout *verticalLayout_11;
    QTabWidget *tabWidget_3;
    QWidget *tab_eu;
    QVBoxLayout *verticalLayout_19;
    QwtPlot *plot_u;
    QWidget *tab_essr;
    QVBoxLayout *verticalLayout_15;
    QwtPlot *plot_ssr;
    QWidget *tab_esst;
    QVBoxLayout *verticalLayout_14;
    QwtPlot *plot_sst;
    QWidget *tab_eFoS;
    QVBoxLayout *verticalLayout_13;
    QwtPlot *plot_sf;
    QWidget *tab_k;
    QVBoxLayout *verticalLayout;
    QwtPlot *plot_k;
    QHBoxLayout *horizontalLayout_43;
    QLabel *label_ff;
    QDoubleSpinBox *SpinBox_ff_2;
    QSpacerItem *horizontalSpacer_9;
    QWidget *tab_3;
    QVBoxLayout *verticalLayout_10;
    QwtPlot *plot_P;
    QHBoxLayout *horizontalLayout_46;
    QLabel *label_shoring_c;
    QLabel *label_shoring_c_2;
    QFrame *line_2;
    QHBoxLayout *horizontalLayout_45;
    QLabel *label_pmax;
    QLabel *label_pmax_val;
    QHBoxLayout *horizontalLayout_44;
    QLabel *label_pmin;
    QLabel *label_pmin_val;
    QHBoxLayout *horizontalLayout_381;
    QLabel *label_p_info_3;
    QLabel *label_pn_val;
    QLabel *label_7;
    QSpacerItem *horizontalSpacer_13;
    QWidget *tab_4;
    QVBoxLayout *verticalLayout_9;
    QwtPlot *plot_R;
    QWidget *tab_legend;
    QHBoxLayout *horizontalLayout_41;
    QTextBrowser *textBrowser;
    QWidget *tab_data;
    QVBoxLayout *verticalLayout_5;
    QTabWidget *tabWidget_tables;
    QWidget *tab_RPz_table;
    QVBoxLayout *verticalLayout_25;
    QTableWidget *tw_radius;
    QWidget *tab_RP_table;
    QVBoxLayout *verticalLayout_26;
    QTableWidget *tw_RP;
    QHBoxLayout *horizontalLayout_37;
    QSpacerItem *horizontalSpacer_12;
    QPushButton *pushButton_clear_table;
    QPushButton *pushButton_delta;
    QWidget *tab_shoring;
    QVBoxLayout *verticalLayout_27;
    QTableWidget *tableWidget_shoring;
    QTextBrowser *textBrowser_shoring;
    QHBoxLayout *horizontalLayout_40;
    QLabel *label_param;
    QLabel *label_param_file;
    QSpacerItem *horizontalSpacer_4;
    QGroupBox *groupBox_5;
    QHBoxLayout *horizontalLayout_39;
    QPushButton *button_calc_ep_cem;
    QPushButton *button_calc_rp;
    QPushButton *button_calc_ep;
    QPushButton *button_calc_e;
    QMenuBar *menuBar;
    QMenu *menu_params;
    QMenu *menu_2;
    QMenu *menu;
    QMenu *menu_shorings;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1036, 630);
        QFont font;
        font.setPointSize(12);
        MainWindow->setFont(font);
        QIcon icon;
        icon.addFile(QStringLiteral("../icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        MainWindow->setIconSize(QSize(64, 64));
        action_paramload = new QAction(MainWindow);
        action_paramload->setObjectName(QStringLiteral("action_paramload"));
        action_paramsave = new QAction(MainWindow);
        action_paramsave->setObjectName(QStringLiteral("action_paramsave"));
        action_openel_report = new QAction(MainWindow);
        action_openel_report->setObjectName(QStringLiteral("action_openel_report"));
        action_openel_report->setCheckable(false);
        action_openel_report->setEnabled(true);
        action_open_ep_report = new QAction(MainWindow);
        action_open_ep_report->setObjectName(QStringLiteral("action_open_ep_report"));
        action_html_report = new QAction(MainWindow);
        action_html_report->setObjectName(QStringLiteral("action_html_report"));
        action_html_report->setCheckable(true);
        action_html_report->setChecked(true);
        action_html_report->setEnabled(true);
        action_datafile = new QAction(MainWindow);
        action_datafile->setObjectName(QStringLiteral("action_datafile"));
        action_datafile->setCheckable(true);
        action_datafile->setChecked(true);
        action111 = new QAction(MainWindow);
        action111->setObjectName(QStringLiteral("action111"));
        action_src = new QAction(MainWindow);
        action_src->setObjectName(QStringLiteral("action_src"));
        QFont font1;
        font1.setUnderline(true);
        action_src->setFont(font1);
        action_pub = new QAction(MainWindow);
        action_pub->setObjectName(QStringLiteral("action_pub"));
        action_pub->setFont(font1);
        action = new QAction(MainWindow);
        action->setObjectName(QStringLiteral("action"));
        action_manual = new QAction(MainWindow);
        action_manual->setObjectName(QStringLiteral("action_manual"));
        action_more_param = new QAction(MainWindow);
        action_more_param->setObjectName(QStringLiteral("action_more_param"));
        action_more_param->setCheckable(true);
        action_open_shorings = new QAction(MainWindow);
        action_open_shorings->setObjectName(QStringLiteral("action_open_shorings"));
        actionGnuplot = new QAction(MainWindow);
        actionGnuplot->setObjectName(QStringLiteral("actionGnuplot"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setAutoFillBackground(true);
        gridLayout_2 = new QGridLayout(centralWidget);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(6, 6, 6, 6);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setFont(font);
        tabWidget->setStyleSheet(QStringLiteral(""));
        tabWidget->setTabPosition(QTabWidget::West);
        tabWidget->setTabShape(QTabWidget::Rounded);
        tab_param = new QWidget();
        tab_param->setObjectName(QStringLiteral("tab_param"));
        verticalLayout_17 = new QVBoxLayout(tab_param);
        verticalLayout_17->setSpacing(4);
        verticalLayout_17->setContentsMargins(6, 6, 6, 6);
        verticalLayout_17->setObjectName(QStringLiteral("verticalLayout_17"));
        verticalLayout_17->setContentsMargins(4, 4, 4, 4);
        tabWidget_params = new QTabWidget(tab_param);
        tabWidget_params->setObjectName(QStringLiteral("tabWidget_params"));
        tabWidget_params->setFont(font);
        tabWidget_params->setStyleSheet(QStringLiteral(""));
        elastic = new QWidget();
        elastic->setObjectName(QStringLiteral("elastic"));
        horizontalLayout_25 = new QHBoxLayout(elastic);
        horizontalLayout_25->setSpacing(6);
        horizontalLayout_25->setContentsMargins(6, 6, 6, 6);
        horizontalLayout_25->setObjectName(QStringLiteral("horizontalLayout_25"));
        verticalLayout_24 = new QVBoxLayout();
        verticalLayout_24->setSpacing(4);
        verticalLayout_24->setObjectName(QStringLiteral("verticalLayout_24"));
        groupBox = new QGroupBox(elastic);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout_18 = new QVBoxLayout(groupBox);
        verticalLayout_18->setSpacing(6);
        verticalLayout_18->setContentsMargins(6, 6, 6, 6);
        verticalLayout_18->setObjectName(QStringLiteral("verticalLayout_18"));
        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_11->addWidget(label);

        spinBox_N = new QSpinBox(groupBox);
        spinBox_N->setObjectName(QStringLiteral("spinBox_N"));
        spinBox_N->setEnabled(true);
        spinBox_N->setMaximumSize(QSize(62, 16777215));
        spinBox_N->setMinimum(2);
        spinBox_N->setMaximum(999999999);
        spinBox_N->setSingleStep(10);
        spinBox_N->setValue(1001);

        horizontalLayout_11->addWidget(spinBox_N);

        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setEnabled(true);

        horizontalLayout_11->addWidget(label_5);

        label_step = new QLabel(groupBox);
        label_step->setObjectName(QStringLiteral("label_step"));
        label_step->setEnabled(true);
        label_step->setFont(font);

        horizontalLayout_11->addWidget(label_step);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_3);


        verticalLayout_18->addLayout(horizontalLayout_11);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(1);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout_2->addWidget(label_2);

        spinBox_width = new QDoubleSpinBox(groupBox);
        spinBox_width->setObjectName(QStringLiteral("spinBox_width"));
        spinBox_width->setMaximumSize(QSize(91, 16777215));
        spinBox_width->setMinimum(0.01);
        spinBox_width->setMaximum(1e+09);
        spinBox_width->setSingleStep(0.1);
        spinBox_width->setValue(3);

        verticalLayout_2->addWidget(spinBox_width);


        horizontalLayout_6->addLayout(verticalLayout_2);

        line = new QFrame(groupBox);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);

        horizontalLayout_6->addWidget(line);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setHorizontalSpacing(10);
        gridLayout->setVerticalSpacing(1);
        gridLayout->setContentsMargins(0, -1, -1, -1);
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setEnabled(true);
        label_3->setStyleSheet(QStringLiteral("font: 8pt \"MS Shell Dlg 2\";"));

        gridLayout->addWidget(label_3, 0, 1, 1, 1);

        label_32 = new QLabel(groupBox);
        label_32->setObjectName(QStringLiteral("label_32"));
        label_32->setEnabled(true);

        gridLayout->addWidget(label_32, 0, 2, 1, 1);

        SpinBox_rb_ = new QDoubleSpinBox(groupBox);
        SpinBox_rb_->setObjectName(QStringLiteral("SpinBox_rb_"));
        SpinBox_rb_->setEnabled(true);
        SpinBox_rb_->setMaximumSize(QSize(61, 16777215));
        SpinBox_rb_->setMinimum(1);
        SpinBox_rb_->setMaximum(1e+09);
        SpinBox_rb_->setValue(20);

        gridLayout->addWidget(SpinBox_rb_, 1, 1, 1, 1);

        label_rb_mm = new QLabel(groupBox);
        label_rb_mm->setObjectName(QStringLiteral("label_rb_mm"));
        label_rb_mm->setEnabled(true);
        label_rb_mm->setFont(font);

        gridLayout->addWidget(label_rb_mm, 1, 2, 1, 1);

        SpinBox_ra = new QDoubleSpinBox(groupBox);
        SpinBox_ra->setObjectName(QStringLiteral("SpinBox_ra"));
        SpinBox_ra->setEnabled(true);
        SpinBox_ra->setMaximumSize(QSize(91, 16777215));
        SpinBox_ra->setMinimum(0.01);
        SpinBox_ra->setMaximum(1e+09);
        SpinBox_ra->setSingleStep(10);
        SpinBox_ra->setValue(3000);

        gridLayout->addWidget(SpinBox_ra, 1, 0, 1, 1);

        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setEnabled(true);

        gridLayout->addWidget(label_8, 0, 0, 1, 1);


        horizontalLayout_6->addLayout(gridLayout);


        verticalLayout_18->addLayout(horizontalLayout_6);

        horizontalLayout_42 = new QHBoxLayout();
        horizontalLayout_42->setSpacing(6);
        horizontalLayout_42->setObjectName(QStringLiteral("horizontalLayout_42"));
        horizontalLayout_42->setContentsMargins(0, -1, -1, -1);
        label_33 = new QLabel(groupBox);
        label_33->setObjectName(QStringLiteral("label_33"));
        label_33->setEnabled(true);

        horizontalLayout_42->addWidget(label_33);

        SpinBox_rze = new QDoubleSpinBox(groupBox);
        SpinBox_rze->setObjectName(QStringLiteral("SpinBox_rze"));
        SpinBox_rze->setEnabled(true);
        SpinBox_rze->setMinimum(1);
        SpinBox_rze->setMaximum(100);
        SpinBox_rze->setSingleStep(0.05);
        SpinBox_rze->setValue(1.1);

        horizontalLayout_42->addWidget(SpinBox_rze);

        label_rzval = new QLabel(groupBox);
        label_rzval->setObjectName(QStringLiteral("label_rzval"));
        label_rzval->setEnabled(false);
        label_rzval->setFont(font);

        horizontalLayout_42->addWidget(label_rzval);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_42->addItem(horizontalSpacer_5);


        verticalLayout_18->addLayout(horizontalLayout_42);

        horizontalLayout_38 = new QHBoxLayout();
        horizontalLayout_38->setSpacing(6);
        horizontalLayout_38->setObjectName(QStringLiteral("horizontalLayout_38"));
        label_700 = new QLabel(groupBox);
        label_700->setObjectName(QStringLiteral("label_700"));
        label_700->setEnabled(true);
        QFont font2;
        font2.setPointSize(11);
        label_700->setFont(font2);

        horizontalLayout_38->addWidget(label_700);

        SpinBox_rcem = new QDoubleSpinBox(groupBox);
        SpinBox_rcem->setObjectName(QStringLiteral("SpinBox_rcem"));
        SpinBox_rcem->setEnabled(true);
        SpinBox_rcem->setFont(font2);
        SpinBox_rcem->setMinimum(0);
        SpinBox_rcem->setMaximum(10000);
        SpinBox_rcem->setSingleStep(100);
        SpinBox_rcem->setValue(1500);

        horizontalLayout_38->addWidget(SpinBox_rcem);

        horizontalSpacer_131 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_38->addItem(horizontalSpacer_131);


        verticalLayout_18->addLayout(horizontalLayout_38);


        verticalLayout_24->addWidget(groupBox);

        checkBox_elproblem = new QCheckBox(elastic);
        checkBox_elproblem->setObjectName(QStringLiteral("checkBox_elproblem"));
        checkBox_elproblem->setEnabled(true);

        verticalLayout_24->addWidget(checkBox_elproblem);

        groupBox_sc = new QGroupBox(elastic);
        groupBox_sc->setObjectName(QStringLiteral("groupBox_sc"));
        groupBox_sc->setEnabled(true);
        verticalLayout_4 = new QVBoxLayout(groupBox_sc);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(6, 6, 6, 6);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        horizontalLayout_35 = new QHBoxLayout();
        horizontalLayout_35->setSpacing(4);
        horizontalLayout_35->setObjectName(QStringLiteral("horizontalLayout_35"));
        comboBox_strength = new QComboBox(groupBox_sc);
        comboBox_strength->setObjectName(QStringLiteral("comboBox_strength"));
        comboBox_strength->setEnabled(true);

        horizontalLayout_35->addWidget(comboBox_strength);

        horizontalSpacer_10 = new QSpacerItem(55, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_35->addItem(horizontalSpacer_10);


        verticalLayout_4->addLayout(horizontalLayout_35);

        groupBox_6 = new QGroupBox(groupBox_sc);
        groupBox_6->setObjectName(QStringLiteral("groupBox_6"));
        groupBox_6->setEnabled(true);
        groupBox_6->setCheckable(false);
        horizontalLayout_10 = new QHBoxLayout(groupBox_6);
        horizontalLayout_10->setSpacing(0);
        horizontalLayout_10->setContentsMargins(6, 6, 6, 6);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        horizontalLayout_10->setContentsMargins(0, 0, 0, 0);
        SpinBox_c0 = new QDoubleSpinBox(groupBox_6);
        SpinBox_c0->setObjectName(QStringLiteral("SpinBox_c0"));
        SpinBox_c0->setMaximumSize(QSize(81, 16777215));
        SpinBox_c0->setDecimals(4);
        SpinBox_c0->setMinimum(-100000);
        SpinBox_c0->setSingleStep(0.1);
        SpinBox_c0->setValue(-14.4);

        horizontalLayout_10->addWidget(SpinBox_c0);

        SpinBox_c1 = new QDoubleSpinBox(groupBox_6);
        SpinBox_c1->setObjectName(QStringLiteral("SpinBox_c1"));
        SpinBox_c1->setMaximumSize(QSize(81, 16777215));
        SpinBox_c1->setDecimals(4);
        SpinBox_c1->setMinimum(-100000);
        SpinBox_c1->setSingleStep(0.1);
        SpinBox_c1->setValue(5.41);

        horizontalLayout_10->addWidget(SpinBox_c1);

        SpinBox_c2 = new QDoubleSpinBox(groupBox_6);
        SpinBox_c2->setObjectName(QStringLiteral("SpinBox_c2"));
        SpinBox_c2->setMaximumSize(QSize(81, 16777215));
        SpinBox_c2->setDecimals(4);
        SpinBox_c2->setMinimum(-100000);
        SpinBox_c2->setSingleStep(0.1);
        SpinBox_c2->setValue(0.12);

        horizontalLayout_10->addWidget(SpinBox_c2);

        horizontalSpacer_7 = new QSpacerItem(210, 17, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_7);


        verticalLayout_4->addWidget(groupBox_6);


        verticalLayout_24->addWidget(groupBox_sc);

        groupBox_2 = new QGroupBox(elastic);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setEnabled(true);
        groupBox_2->setCheckable(false);
        verticalLayout_20 = new QVBoxLayout(groupBox_2);
        verticalLayout_20->setSpacing(6);
        verticalLayout_20->setContentsMargins(6, 6, 6, 6);
        verticalLayout_20->setObjectName(QStringLiteral("verticalLayout_20"));
        horizontalLayout_20 = new QHBoxLayout();
        horizontalLayout_20->setSpacing(6);
        horizontalLayout_20->setObjectName(QStringLiteral("horizontalLayout_20"));
        comboBox_calc_method = new QComboBox(groupBox_2);
        comboBox_calc_method->setObjectName(QStringLiteral("comboBox_calc_method"));
        comboBox_calc_method->setEnabled(true);
        comboBox_calc_method->setMaximumSize(QSize(170, 16777215));
        comboBox_calc_method->setEditable(true);

        horizontalLayout_20->addWidget(comboBox_calc_method);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        label_37 = new QLabel(groupBox_2);
        label_37->setObjectName(QStringLiteral("label_37"));
        label_37->setEnabled(true);

        verticalLayout_6->addWidget(label_37);

        comboBox_el_method = new QComboBox(groupBox_2);
        comboBox_el_method->setObjectName(QStringLiteral("comboBox_el_method"));
        comboBox_el_method->setEnabled(true);
        comboBox_el_method->setMaximumSize(QSize(171, 16777215));
        comboBox_el_method->setEditable(true);

        verticalLayout_6->addWidget(comboBox_el_method);


        horizontalLayout_20->addLayout(verticalLayout_6);

        horizontalSpacer_14 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_20->addItem(horizontalSpacer_14);


        verticalLayout_20->addLayout(horizontalLayout_20);

        groupBox_4 = new QGroupBox(groupBox_2);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        groupBox_4->setEnabled(true);
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(groupBox_4->sizePolicy().hasHeightForWidth());
        groupBox_4->setSizePolicy(sizePolicy);
        horizontalLayout_7 = new QHBoxLayout(groupBox_4);
        horizontalLayout_7->setSpacing(4);
        horizontalLayout_7->setContentsMargins(6, 6, 6, 6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        horizontalLayout_7->setContentsMargins(4, 4, 4, 4);
        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setSpacing(2);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        label_6 = new QLabel(groupBox_4);
        label_6->setObjectName(QStringLiteral("label_6"));

        verticalLayout_7->addWidget(label_6);

        SpinBox_rzmin = new QDoubleSpinBox(groupBox_4);
        SpinBox_rzmin->setObjectName(QStringLiteral("SpinBox_rzmin"));
        SpinBox_rzmin->setMaximumSize(QSize(79, 16777215));
        SpinBox_rzmin->setDecimals(2);
        SpinBox_rzmin->setMinimum(1);
        SpinBox_rzmin->setMaximum(100000);
        SpinBox_rzmin->setSingleStep(0.5);
        SpinBox_rzmin->setValue(1);

        verticalLayout_7->addWidget(SpinBox_rzmin);


        horizontalLayout_7->addLayout(verticalLayout_7);

        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setSpacing(2);
        verticalLayout_8->setObjectName(QStringLiteral("verticalLayout_8"));
        label_12 = new QLabel(groupBox_4);
        label_12->setObjectName(QStringLiteral("label_12"));

        verticalLayout_8->addWidget(label_12);

        SpinBox_rzmax = new QDoubleSpinBox(groupBox_4);
        SpinBox_rzmax->setObjectName(QStringLiteral("SpinBox_rzmax"));
        SpinBox_rzmax->setMaximumSize(QSize(79, 16777215));
        SpinBox_rzmax->setDecimals(2);
        SpinBox_rzmax->setMinimum(1);
        SpinBox_rzmax->setMaximum(100000);
        SpinBox_rzmax->setSingleStep(0.5);
        SpinBox_rzmax->setValue(10);

        verticalLayout_8->addWidget(SpinBox_rzmax);


        horizontalLayout_7->addLayout(verticalLayout_8);

        verticalLayout_22 = new QVBoxLayout();
        verticalLayout_22->setSpacing(2);
        verticalLayout_22->setObjectName(QStringLiteral("verticalLayout_22"));
        label_31 = new QLabel(groupBox_4);
        label_31->setObjectName(QStringLiteral("label_31"));

        verticalLayout_22->addWidget(label_31);

        spinBox_rzn = new QSpinBox(groupBox_4);
        spinBox_rzn->setObjectName(QStringLiteral("spinBox_rzn"));
        spinBox_rzn->setMinimum(1);
        spinBox_rzn->setMaximum(100000);
        spinBox_rzn->setSingleStep(1);
        spinBox_rzn->setValue(300);

        verticalLayout_22->addWidget(spinBox_rzn);


        horizontalLayout_7->addLayout(verticalLayout_22);

        horizontalSpacer_8 = new QSpacerItem(15, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_8);


        verticalLayout_20->addWidget(groupBox_4);


        verticalLayout_24->addWidget(groupBox_2);


        horizontalLayout_25->addLayout(verticalLayout_24);

        verticalLayout_21 = new QVBoxLayout();
        verticalLayout_21->setSpacing(4);
        verticalLayout_21->setObjectName(QStringLiteral("verticalLayout_21"));
        groupBox_3 = new QGroupBox(elastic);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setFlat(false);
        verticalLayout_3 = new QVBoxLayout(groupBox_3);
        verticalLayout_3->setSpacing(4);
        verticalLayout_3->setContentsMargins(6, 6, 6, 6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(6, 6, 6, 6);
        checkBox_burr = new QCheckBox(groupBox_3);
        checkBox_burr->setObjectName(QStringLiteral("checkBox_burr"));
        checkBox_burr->setEnabled(true);
        checkBox_burr->setChecked(true);

        verticalLayout_3->addWidget(checkBox_burr);

        label_Er = new QLabel(groupBox_3);
        label_Er->setObjectName(QStringLiteral("label_Er"));
        label_Er->setEnabled(true);

        verticalLayout_3->addWidget(label_Er);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        label_4 = new QLabel(groupBox_3);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_9->addWidget(label_4);

        SpinBox_Elab = new QDoubleSpinBox(groupBox_3);
        SpinBox_Elab->setObjectName(QStringLiteral("SpinBox_Elab"));
        SpinBox_Elab->setMaximumSize(QSize(85, 16777215));
        SpinBox_Elab->setMinimum(0);
        SpinBox_Elab->setMaximum(1e+09);
        SpinBox_Elab->setSingleStep(100);
        SpinBox_Elab->setValue(31700);

        horizontalLayout_9->addWidget(SpinBox_Elab);


        verticalLayout_3->addLayout(horizontalLayout_9);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label_10 = new QLabel(groupBox_3);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setFont(font);

        horizontalLayout_3->addWidget(label_10);

        SpinBox_mu = new QDoubleSpinBox(groupBox_3);
        SpinBox_mu->setObjectName(QStringLiteral("SpinBox_mu"));
        SpinBox_mu->setMaximumSize(QSize(61, 16777215));
        SpinBox_mu->setMinimum(0);
        SpinBox_mu->setMaximum(1);
        SpinBox_mu->setSingleStep(0.05);
        SpinBox_mu->setValue(0.28);

        horizontalLayout_3->addWidget(SpinBox_mu);


        verticalLayout_3->addLayout(horizontalLayout_3);

        horizontalLayout_36 = new QHBoxLayout();
        horizontalLayout_36->setSpacing(6);
        horizontalLayout_36->setObjectName(QStringLiteral("horizontalLayout_36"));
        label_34 = new QLabel(groupBox_3);
        label_34->setObjectName(QStringLiteral("label_34"));
        label_34->setEnabled(true);

        horizontalLayout_36->addWidget(label_34);

        SpinBox_P = new QDoubleSpinBox(groupBox_3);
        SpinBox_P->setObjectName(QStringLiteral("SpinBox_P"));
        SpinBox_P->setEnabled(true);
        SpinBox_P->setMaximumSize(QSize(79, 16777215));
        SpinBox_P->setReadOnly(false);
        SpinBox_P->setDecimals(3);
        SpinBox_P->setMinimum(-1e+06);
        SpinBox_P->setMaximum(100000);
        SpinBox_P->setValue(0);

        horizontalLayout_36->addWidget(SpinBox_P);


        verticalLayout_3->addLayout(horizontalLayout_36);

        horizontalLayout_22 = new QHBoxLayout();
        horizontalLayout_22->setSpacing(6);
        horizontalLayout_22->setObjectName(QStringLiteral("horizontalLayout_22"));
        label_16 = new QLabel(groupBox_3);
        label_16->setObjectName(QStringLiteral("label_16"));

        horizontalLayout_22->addWidget(label_16);

        SpinBox_S = new QDoubleSpinBox(groupBox_3);
        SpinBox_S->setObjectName(QStringLiteral("SpinBox_S"));
        SpinBox_S->setMaximumSize(QSize(79, 16777215));
        SpinBox_S->setDecimals(3);
        SpinBox_S->setMinimum(-1e+06);
        SpinBox_S->setMaximum(100000);
        SpinBox_S->setValue(6.6);

        horizontalLayout_22->addWidget(SpinBox_S);


        verticalLayout_3->addLayout(horizontalLayout_22);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label_30 = new QLabel(groupBox_3);
        label_30->setObjectName(QStringLiteral("label_30"));

        horizontalLayout_4->addWidget(label_30);

        horizontalSpacer_6 = new QSpacerItem(17, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_6);

        SpinBox_kp = new QDoubleSpinBox(groupBox_3);
        SpinBox_kp->setObjectName(QStringLiteral("SpinBox_kp"));
        SpinBox_kp->setEnabled(true);
        SpinBox_kp->setMaximumSize(QSize(79, 16777215));
        SpinBox_kp->setDecimals(3);
        SpinBox_kp->setMinimum(0.001);
        SpinBox_kp->setMaximum(2);
        SpinBox_kp->setSingleStep(0.05);
        SpinBox_kp->setValue(1.8);

        horizontalLayout_4->addWidget(SpinBox_kp);


        verticalLayout_3->addLayout(horizontalLayout_4);

        horizontalLayout_27 = new QHBoxLayout();
        horizontalLayout_27->setSpacing(6);
        horizontalLayout_27->setObjectName(QStringLiteral("horizontalLayout_27"));
        label_19 = new QLabel(groupBox_3);
        label_19->setObjectName(QStringLiteral("label_19"));

        horizontalLayout_27->addWidget(label_19);

        SpinBox_ssl_c = new QDoubleSpinBox(groupBox_3);
        SpinBox_ssl_c->setObjectName(QStringLiteral("SpinBox_ssl_c"));
        SpinBox_ssl_c->setMaximumSize(QSize(79, 16777215));
        SpinBox_ssl_c->setDecimals(3);
        SpinBox_ssl_c->setMinimum(0.001);
        SpinBox_ssl_c->setMaximum(100000);
        SpinBox_ssl_c->setValue(72);

        horizontalLayout_27->addWidget(SpinBox_ssl_c);


        verticalLayout_3->addLayout(horizontalLayout_27);

        horizontalLayout_34 = new QHBoxLayout();
        horizontalLayout_34->setSpacing(6);
        horizontalLayout_34->setObjectName(QStringLiteral("horizontalLayout_34"));
        label_35 = new QLabel(groupBox_3);
        label_35->setObjectName(QStringLiteral("label_35"));
        label_35->setEnabled(true);

        horizontalLayout_34->addWidget(label_35);

        SpinBox_ssl_t = new QDoubleSpinBox(groupBox_3);
        SpinBox_ssl_t->setObjectName(QStringLiteral("SpinBox_ssl_t"));
        SpinBox_ssl_t->setEnabled(true);
        SpinBox_ssl_t->setMaximumSize(QSize(79, 16777215));
        SpinBox_ssl_t->setDecimals(3);
        SpinBox_ssl_t->setMinimum(0.001);
        SpinBox_ssl_t->setMaximum(100000);
        SpinBox_ssl_t->setValue(9.8);

        horizontalLayout_34->addWidget(SpinBox_ssl_t);


        verticalLayout_3->addLayout(horizontalLayout_34);

        horizontalLayout_28 = new QHBoxLayout();
        horizontalLayout_28->setSpacing(6);
        horizontalLayout_28->setObjectName(QStringLiteral("horizontalLayout_28"));
        label_20 = new QLabel(groupBox_3);
        label_20->setObjectName(QStringLiteral("label_20"));
        label_20->setEnabled(true);

        horizontalLayout_28->addWidget(label_20);

        SpinBox_ko = new QDoubleSpinBox(groupBox_3);
        SpinBox_ko->setObjectName(QStringLiteral("SpinBox_ko"));
        SpinBox_ko->setEnabled(true);
        SpinBox_ko->setMaximumSize(QSize(79, 16777215));
        SpinBox_ko->setDecimals(3);
        SpinBox_ko->setMinimum(0.001);
        SpinBox_ko->setMaximum(10);
        SpinBox_ko->setSingleStep(0.05);
        SpinBox_ko->setValue(0.2);

        horizontalLayout_28->addWidget(SpinBox_ko);


        verticalLayout_3->addLayout(horizontalLayout_28);

        verticalSpacer_6 = new QSpacerItem(305, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_6);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_2);


        verticalLayout_21->addWidget(groupBox_3);

        splitter = new QSplitter(elastic);
        splitter->setObjectName(QStringLiteral("splitter"));
        splitter->setOrientation(Qt::Vertical);
        layoutWidget = new QWidget(splitter);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        horizontalLayout = new QHBoxLayout(layoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(6, 6, 6, 6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        label_11 = new QLabel(layoutWidget);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setEnabled(true);

        horizontalLayout->addWidget(label_11);

        spinBox_eps_n = new QSpinBox(layoutWidget);
        spinBox_eps_n->setObjectName(QStringLiteral("spinBox_eps_n"));
        spinBox_eps_n->setEnabled(true);
        spinBox_eps_n->setMaximumSize(QSize(41, 16777215));
        spinBox_eps_n->setMaximum(10);
        spinBox_eps_n->setValue(6);

        horizontalLayout->addWidget(spinBox_eps_n);

        label_eps = new QLabel(layoutWidget);
        label_eps->setObjectName(QStringLiteral("label_eps"));
        label_eps->setEnabled(true);
        label_eps->setFont(font);

        horizontalLayout->addWidget(label_eps);

        horizontalSpacer_11 = new QSpacerItem(17, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_11);

        splitter->addWidget(layoutWidget);

        verticalLayout_21->addWidget(splitter);


        horizontalLayout_25->addLayout(verticalLayout_21);

        horizontalSpacer_15 = new QSpacerItem(1, 448, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_25->addItem(horizontalSpacer_15);

        tabWidget_params->addTab(elastic, QString());
        groupBox_2->raise();
        groupBox_6->raise();
        other = new QWidget();
        other->setObjectName(QStringLiteral("other"));
        horizontalLayout_8 = new QHBoxLayout(other);
        horizontalLayout_8->setSpacing(8);
        horizontalLayout_8->setContentsMargins(6, 6, 6, 6);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        verticalLayout_23 = new QVBoxLayout();
        verticalLayout_23->setSpacing(6);
        verticalLayout_23->setObjectName(QStringLiteral("verticalLayout_23"));
        horizontalLayout_33 = new QHBoxLayout();
        horizontalLayout_33->setSpacing(6);
        horizontalLayout_33->setObjectName(QStringLiteral("horizontalLayout_33"));
        label_13 = new QLabel(other);
        label_13->setObjectName(QStringLiteral("label_13"));

        horizontalLayout_33->addWidget(label_13);

        SpinBox_thetta = new QDoubleSpinBox(other);
        SpinBox_thetta->setObjectName(QStringLiteral("SpinBox_thetta"));
        SpinBox_thetta->setEnabled(true);
        SpinBox_thetta->setMaximumSize(QSize(110, 16777215));
        SpinBox_thetta->setDecimals(3);
        SpinBox_thetta->setMinimum(0);
        SpinBox_thetta->setMaximum(360);
        SpinBox_thetta->setValue(90);

        horizontalLayout_33->addWidget(SpinBox_thetta);


        verticalLayout_23->addLayout(horizontalLayout_33);

        horizontalLayout_31 = new QHBoxLayout();
        horizontalLayout_31->setSpacing(6);
        horizontalLayout_31->setObjectName(QStringLiteral("horizontalLayout_31"));
        label_15 = new QLabel(other);
        label_15->setObjectName(QStringLiteral("label_15"));

        horizontalLayout_31->addWidget(label_15);

        SpinBox_gg = new QDoubleSpinBox(other);
        SpinBox_gg->setObjectName(QStringLiteral("SpinBox_gg"));
        SpinBox_gg->setMaximumSize(QSize(79, 16777215));
        SpinBox_gg->setDecimals(3);
        SpinBox_gg->setMinimum(0.001);
        SpinBox_gg->setMaximum(100000);
        SpinBox_gg->setSingleStep(0.01);
        SpinBox_gg->setValue(2.394);

        horizontalLayout_31->addWidget(SpinBox_gg);


        verticalLayout_23->addLayout(horizontalLayout_31);

        checkBox_calcff = new QCheckBox(other);
        checkBox_calcff->setObjectName(QStringLiteral("checkBox_calcff"));
        checkBox_calcff->setEnabled(true);
        checkBox_calcff->setChecked(true);

        verticalLayout_23->addWidget(checkBox_calcff);

        horizontalLayout_30 = new QHBoxLayout();
        horizontalLayout_30->setSpacing(6);
        horizontalLayout_30->setObjectName(QStringLiteral("horizontalLayout_30"));
        label_18 = new QLabel(other);
        label_18->setObjectName(QStringLiteral("label_18"));
        label_18->setEnabled(false);

        horizontalLayout_30->addWidget(label_18);

        SpinBox_ff0 = new QDoubleSpinBox(other);
        SpinBox_ff0->setObjectName(QStringLiteral("SpinBox_ff0"));
        SpinBox_ff0->setEnabled(false);
        SpinBox_ff0->setMaximumSize(QSize(71, 16777215));
        SpinBox_ff0->setDecimals(3);
        SpinBox_ff0->setMinimum(-360);
        SpinBox_ff0->setMaximum(100000);
        SpinBox_ff0->setValue(37);

        horizontalLayout_30->addWidget(SpinBox_ff0);


        verticalLayout_23->addLayout(horizontalLayout_30);

        horizontalLayout_29 = new QHBoxLayout();
        horizontalLayout_29->setSpacing(6);
        horizontalLayout_29->setObjectName(QStringLiteral("horizontalLayout_29"));
        label_17 = new QLabel(other);
        label_17->setObjectName(QStringLiteral("label_17"));
        label_17->setEnabled(false);

        horizontalLayout_29->addWidget(label_17);

        SpinBox_ff1 = new QDoubleSpinBox(other);
        SpinBox_ff1->setObjectName(QStringLiteral("SpinBox_ff1"));
        SpinBox_ff1->setEnabled(false);
        SpinBox_ff1->setMaximumSize(QSize(71, 16777215));
        SpinBox_ff1->setDecimals(3);
        SpinBox_ff1->setMinimum(-360);
        SpinBox_ff1->setMaximum(100000);
        SpinBox_ff1->setValue(37);

        horizontalLayout_29->addWidget(SpinBox_ff1);


        verticalLayout_23->addLayout(horizontalLayout_29);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setSpacing(6);
        horizontalLayout_15->setObjectName(QStringLiteral("horizontalLayout_15"));
        label_23 = new QLabel(other);
        label_23->setObjectName(QStringLiteral("label_23"));

        horizontalLayout_15->addWidget(label_23);

        SpinBox_upr = new QDoubleSpinBox(other);
        SpinBox_upr->setObjectName(QStringLiteral("SpinBox_upr"));
        SpinBox_upr->setEnabled(true);
        SpinBox_upr->setMaximumSize(QSize(110, 16777215));
        SpinBox_upr->setDecimals(3);
        SpinBox_upr->setMinimum(0);
        SpinBox_upr->setMaximum(100000);
        SpinBox_upr->setValue(150);

        horizontalLayout_15->addWidget(SpinBox_upr);


        verticalLayout_23->addLayout(horizontalLayout_15);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        label_22 = new QLabel(other);
        label_22->setObjectName(QStringLiteral("label_22"));

        horizontalLayout_5->addWidget(label_22);

        SpinBox_ttk = new QDoubleSpinBox(other);
        SpinBox_ttk->setObjectName(QStringLiteral("SpinBox_ttk"));
        SpinBox_ttk->setEnabled(true);
        SpinBox_ttk->setDecimals(3);
        SpinBox_ttk->setMinimum(0.001);
        SpinBox_ttk->setMaximum(100000);
        SpinBox_ttk->setSingleStep(0.01);
        SpinBox_ttk->setValue(0.12);

        horizontalLayout_5->addWidget(SpinBox_ttk);


        verticalLayout_23->addLayout(horizontalLayout_5);

        horizontalLayout_21 = new QHBoxLayout();
        horizontalLayout_21->setSpacing(6);
        horizontalLayout_21->setObjectName(QStringLiteral("horizontalLayout_21"));
        label_26 = new QLabel(other);
        label_26->setObjectName(QStringLiteral("label_26"));

        horizontalLayout_21->addWidget(label_26);

        SpinBox_ssgp = new QDoubleSpinBox(other);
        SpinBox_ssgp->setObjectName(QStringLiteral("SpinBox_ssgp"));
        SpinBox_ssgp->setEnabled(true);
        SpinBox_ssgp->setMaximumSize(QSize(110, 16777215));
        SpinBox_ssgp->setDecimals(3);
        SpinBox_ssgp->setMinimum(0);
        SpinBox_ssgp->setMaximum(100000);
        SpinBox_ssgp->setSingleStep(0.05);
        SpinBox_ssgp->setValue(1);

        horizontalLayout_21->addWidget(SpinBox_ssgp);


        verticalLayout_23->addLayout(horizontalLayout_21);

        verticalSpacer = new QSpacerItem(20, 172, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_23->addItem(verticalSpacer);


        horizontalLayout_8->addLayout(verticalLayout_23);

        verticalLayout_16 = new QVBoxLayout();
        verticalLayout_16->setSpacing(6);
        verticalLayout_16->setObjectName(QStringLiteral("verticalLayout_16"));
        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setSpacing(6);
        horizontalLayout_16->setObjectName(QStringLiteral("horizontalLayout_16"));
        label_29 = new QLabel(other);
        label_29->setObjectName(QStringLiteral("label_29"));

        horizontalLayout_16->addWidget(label_29);

        SpinBox_ku = new QDoubleSpinBox(other);
        SpinBox_ku->setObjectName(QStringLiteral("SpinBox_ku"));
        SpinBox_ku->setEnabled(true);
        SpinBox_ku->setMaximumSize(QSize(110, 16777215));
        SpinBox_ku->setDecimals(3);
        SpinBox_ku->setMinimum(0.001);
        SpinBox_ku->setMaximum(100000);
        SpinBox_ku->setSingleStep(0.05);
        SpinBox_ku->setValue(0.5);

        horizontalLayout_16->addWidget(SpinBox_ku);


        verticalLayout_16->addLayout(horizontalLayout_16);

        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setSpacing(6);
        horizontalLayout_17->setObjectName(QStringLiteral("horizontalLayout_17"));
        label_28 = new QLabel(other);
        label_28->setObjectName(QStringLiteral("label_28"));

        horizontalLayout_17->addWidget(label_28);

        SpinBox_kd = new QDoubleSpinBox(other);
        SpinBox_kd->setObjectName(QStringLiteral("SpinBox_kd"));
        SpinBox_kd->setEnabled(true);
        SpinBox_kd->setMaximumSize(QSize(110, 16777215));
        SpinBox_kd->setDecimals(3);
        SpinBox_kd->setMinimum(0.001);
        SpinBox_kd->setMaximum(100000);
        SpinBox_kd->setSingleStep(0.05);
        SpinBox_kd->setValue(2);

        horizontalLayout_17->addWidget(SpinBox_kd);


        verticalLayout_16->addLayout(horizontalLayout_17);

        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setSpacing(6);
        horizontalLayout_18->setObjectName(QStringLiteral("horizontalLayout_18"));
        label_27 = new QLabel(other);
        label_27->setObjectName(QStringLiteral("label_27"));

        horizontalLayout_18->addWidget(label_27);

        SpinBox_ke = new QDoubleSpinBox(other);
        SpinBox_ke->setObjectName(QStringLiteral("SpinBox_ke"));
        SpinBox_ke->setEnabled(true);
        SpinBox_ke->setMaximumSize(QSize(110, 16777215));
        SpinBox_ke->setDecimals(3);
        SpinBox_ke->setMinimum(0.001);
        SpinBox_ke->setMaximum(100000);
        SpinBox_ke->setSingleStep(0.05);
        SpinBox_ke->setValue(0.7);

        horizontalLayout_18->addWidget(SpinBox_ke);


        verticalLayout_16->addLayout(horizontalLayout_18);

        horizontalLayout_23 = new QHBoxLayout();
        horizontalLayout_23->setSpacing(6);
        horizontalLayout_23->setObjectName(QStringLiteral("horizontalLayout_23"));
        label_25 = new QLabel(other);
        label_25->setObjectName(QStringLiteral("label_25"));

        horizontalLayout_23->addWidget(label_25);

        SpinBox_uw = new QDoubleSpinBox(other);
        SpinBox_uw->setObjectName(QStringLiteral("SpinBox_uw"));
        SpinBox_uw->setEnabled(true);
        SpinBox_uw->setMaximumSize(QSize(79, 16777215));
        SpinBox_uw->setDecimals(3);
        SpinBox_uw->setMinimum(0.001);
        SpinBox_uw->setMaximum(100000);
        SpinBox_uw->setValue(10);

        horizontalLayout_23->addWidget(SpinBox_uw);


        verticalLayout_16->addLayout(horizontalLayout_23);

        horizontalLayout_24 = new QHBoxLayout();
        horizontalLayout_24->setSpacing(6);
        horizontalLayout_24->setObjectName(QStringLiteral("horizontalLayout_24"));
        label_24 = new QLabel(other);
        label_24->setObjectName(QStringLiteral("label_24"));
        label_24->setEnabled(false);

        horizontalLayout_24->addWidget(label_24);

        SpinBox_Pn = new QDoubleSpinBox(other);
        SpinBox_Pn->setObjectName(QStringLiteral("SpinBox_Pn"));
        SpinBox_Pn->setEnabled(false);
        SpinBox_Pn->setMaximumSize(QSize(79, 16777215));
        SpinBox_Pn->setDecimals(3);
        SpinBox_Pn->setMinimum(0);
        SpinBox_Pn->setMaximum(100000);
        SpinBox_Pn->setSingleStep(0.001);
        SpinBox_Pn->setValue(110);

        horizontalLayout_24->addWidget(SpinBox_Pn);


        verticalLayout_16->addLayout(horizontalLayout_24);

        horizontalLayout_26 = new QHBoxLayout();
        horizontalLayout_26->setSpacing(6);
        horizontalLayout_26->setObjectName(QStringLiteral("horizontalLayout_26"));
        label_21 = new QLabel(other);
        label_21->setObjectName(QStringLiteral("label_21"));

        horizontalLayout_26->addWidget(label_21);

        SpinBox_km = new QDoubleSpinBox(other);
        SpinBox_km->setObjectName(QStringLiteral("SpinBox_km"));
        SpinBox_km->setEnabled(true);
        SpinBox_km->setMaximumSize(QSize(71, 16777215));
        SpinBox_km->setDecimals(3);
        SpinBox_km->setMinimum(0.001);
        SpinBox_km->setMaximum(100000);
        SpinBox_km->setSingleStep(0.05);
        SpinBox_km->setValue(0.85);

        horizontalLayout_26->addWidget(SpinBox_km);


        verticalLayout_16->addLayout(horizontalLayout_26);

        horizontalLayout_32 = new QHBoxLayout();
        horizontalLayout_32->setSpacing(6);
        horizontalLayout_32->setObjectName(QStringLiteral("horizontalLayout_32"));
        label_14 = new QLabel(other);
        label_14->setObjectName(QStringLiteral("label_14"));

        horizontalLayout_32->addWidget(label_14);

        SpinBox_Br = new QDoubleSpinBox(other);
        SpinBox_Br->setObjectName(QStringLiteral("SpinBox_Br"));
        SpinBox_Br->setEnabled(true);
        SpinBox_Br->setMaximumSize(QSize(110, 16777215));
        SpinBox_Br->setDecimals(3);
        SpinBox_Br->setMinimum(0.001);
        SpinBox_Br->setMaximum(100000);
        SpinBox_Br->setValue(500);

        horizontalLayout_32->addWidget(SpinBox_Br);


        verticalLayout_16->addLayout(horizontalLayout_32);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_16->addItem(verticalSpacer_4);


        horizontalLayout_8->addLayout(verticalLayout_16);

        horizontalSpacer = new QSpacerItem(0, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer);

        tabWidget_params->addTab(other, QString());

        verticalLayout_17->addWidget(tabWidget_params);

        tabWidget->addTab(tab_param, QString());
        tab_graphics = new QWidget();
        tab_graphics->setObjectName(QStringLiteral("tab_graphics"));
        horizontalLayout_2 = new QHBoxLayout(tab_graphics);
        horizontalLayout_2->setSpacing(2);
        horizontalLayout_2->setContentsMargins(6, 6, 6, 6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(2, 2, 2, 2);
        tabWidget_graphics = new QTabWidget(tab_graphics);
        tabWidget_graphics->setObjectName(QStringLiteral("tabWidget_graphics"));
        tabWidget_graphics->setEnabled(true);
        QFont font3;
        font3.setFamily(QStringLiteral("Helvetica"));
        font3.setPointSize(8);
        font3.setBold(false);
        font3.setItalic(false);
        font3.setWeight(50);
        tabWidget_graphics->setFont(font3);
        tabWidget_graphics->setStyleSheet(QStringLiteral("font: 8pt \"Helvetica\";"));
        tab_e = new QWidget();
        tab_e->setObjectName(QStringLiteral("tab_e"));
        tab_e->setFont(font3);
        verticalLayout_11 = new QVBoxLayout(tab_e);
        verticalLayout_11->setSpacing(6);
        verticalLayout_11->setContentsMargins(6, 6, 6, 6);
        verticalLayout_11->setObjectName(QStringLiteral("verticalLayout_11"));
        tabWidget_3 = new QTabWidget(tab_e);
        tabWidget_3->setObjectName(QStringLiteral("tabWidget_3"));
        tabWidget_3->setEnabled(true);
        tab_eu = new QWidget();
        tab_eu->setObjectName(QStringLiteral("tab_eu"));
        verticalLayout_19 = new QVBoxLayout(tab_eu);
        verticalLayout_19->setSpacing(6);
        verticalLayout_19->setContentsMargins(6, 6, 6, 6);
        verticalLayout_19->setObjectName(QStringLiteral("verticalLayout_19"));
        plot_u = new QwtPlot(tab_eu);
        plot_u->setObjectName(QStringLiteral("plot_u"));

        verticalLayout_19->addWidget(plot_u);

        tabWidget_3->addTab(tab_eu, QString());
        tab_essr = new QWidget();
        tab_essr->setObjectName(QStringLiteral("tab_essr"));
        verticalLayout_15 = new QVBoxLayout(tab_essr);
        verticalLayout_15->setSpacing(6);
        verticalLayout_15->setContentsMargins(6, 6, 6, 6);
        verticalLayout_15->setObjectName(QStringLiteral("verticalLayout_15"));
        plot_ssr = new QwtPlot(tab_essr);
        plot_ssr->setObjectName(QStringLiteral("plot_ssr"));

        verticalLayout_15->addWidget(plot_ssr);

        tabWidget_3->addTab(tab_essr, QString());
        tab_esst = new QWidget();
        tab_esst->setObjectName(QStringLiteral("tab_esst"));
        verticalLayout_14 = new QVBoxLayout(tab_esst);
        verticalLayout_14->setSpacing(6);
        verticalLayout_14->setContentsMargins(6, 6, 6, 6);
        verticalLayout_14->setObjectName(QStringLiteral("verticalLayout_14"));
        plot_sst = new QwtPlot(tab_esst);
        plot_sst->setObjectName(QStringLiteral("plot_sst"));

        verticalLayout_14->addWidget(plot_sst);

        tabWidget_3->addTab(tab_esst, QString());
        tab_eFoS = new QWidget();
        tab_eFoS->setObjectName(QStringLiteral("tab_eFoS"));
        tab_eFoS->setEnabled(false);
        verticalLayout_13 = new QVBoxLayout(tab_eFoS);
        verticalLayout_13->setSpacing(6);
        verticalLayout_13->setContentsMargins(6, 6, 6, 6);
        verticalLayout_13->setObjectName(QStringLiteral("verticalLayout_13"));
        plot_sf = new QwtPlot(tab_eFoS);
        plot_sf->setObjectName(QStringLiteral("plot_sf"));

        verticalLayout_13->addWidget(plot_sf);

        tabWidget_3->addTab(tab_eFoS, QString());
        tab_k = new QWidget();
        tab_k->setObjectName(QStringLiteral("tab_k"));
        verticalLayout = new QVBoxLayout(tab_k);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(6, 6, 6, 6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        plot_k = new QwtPlot(tab_k);
        plot_k->setObjectName(QStringLiteral("plot_k"));

        verticalLayout->addWidget(plot_k);

        tabWidget_3->addTab(tab_k, QString());

        verticalLayout_11->addWidget(tabWidget_3);

        horizontalLayout_43 = new QHBoxLayout();
        horizontalLayout_43->setSpacing(2);
        horizontalLayout_43->setObjectName(QStringLiteral("horizontalLayout_43"));
        label_ff = new QLabel(tab_e);
        label_ff->setObjectName(QStringLiteral("label_ff"));

        horizontalLayout_43->addWidget(label_ff);

        SpinBox_ff_2 = new QDoubleSpinBox(tab_e);
        SpinBox_ff_2->setObjectName(QStringLiteral("SpinBox_ff_2"));
        SpinBox_ff_2->setReadOnly(true);
        SpinBox_ff_2->setDecimals(12);
        SpinBox_ff_2->setMaximum(1e+07);
        SpinBox_ff_2->setValue(0);

        horizontalLayout_43->addWidget(SpinBox_ff_2);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_43->addItem(horizontalSpacer_9);


        verticalLayout_11->addLayout(horizontalLayout_43);

        tabWidget_graphics->addTab(tab_e, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        verticalLayout_10 = new QVBoxLayout(tab_3);
        verticalLayout_10->setSpacing(6);
        verticalLayout_10->setContentsMargins(6, 6, 6, 6);
        verticalLayout_10->setObjectName(QStringLiteral("verticalLayout_10"));
        plot_P = new QwtPlot(tab_3);
        plot_P->setObjectName(QStringLiteral("plot_P"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(plot_P->sizePolicy().hasHeightForWidth());
        plot_P->setSizePolicy(sizePolicy1);

        verticalLayout_10->addWidget(plot_P);

        horizontalLayout_46 = new QHBoxLayout();
        horizontalLayout_46->setSpacing(8);
        horizontalLayout_46->setObjectName(QStringLiteral("horizontalLayout_46"));
        horizontalLayout_46->setContentsMargins(3, 1, 1, 0);
        label_shoring_c = new QLabel(tab_3);
        label_shoring_c->setObjectName(QStringLiteral("label_shoring_c"));
        label_shoring_c->setFont(font3);
        label_shoring_c->setTextFormat(Qt::RichText);

        horizontalLayout_46->addWidget(label_shoring_c);

        label_shoring_c_2 = new QLabel(tab_3);
        label_shoring_c_2->setObjectName(QStringLiteral("label_shoring_c_2"));
        label_shoring_c_2->setTextFormat(Qt::RichText);

        horizontalLayout_46->addWidget(label_shoring_c_2);

        line_2 = new QFrame(tab_3);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);

        horizontalLayout_46->addWidget(line_2);

        horizontalLayout_45 = new QHBoxLayout();
        horizontalLayout_45->setSpacing(6);
        horizontalLayout_45->setObjectName(QStringLiteral("horizontalLayout_45"));
        label_pmax = new QLabel(tab_3);
        label_pmax->setObjectName(QStringLiteral("label_pmax"));
        label_pmax->setFont(font3);

        horizontalLayout_45->addWidget(label_pmax);

        label_pmax_val = new QLabel(tab_3);
        label_pmax_val->setObjectName(QStringLiteral("label_pmax_val"));
        label_pmax_val->setFont(font3);
        label_pmax_val->setTextFormat(Qt::RichText);
        label_pmax_val->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        horizontalLayout_45->addWidget(label_pmax_val);


        horizontalLayout_46->addLayout(horizontalLayout_45);

        horizontalLayout_44 = new QHBoxLayout();
        horizontalLayout_44->setSpacing(6);
        horizontalLayout_44->setObjectName(QStringLiteral("horizontalLayout_44"));
        label_pmin = new QLabel(tab_3);
        label_pmin->setObjectName(QStringLiteral("label_pmin"));

        horizontalLayout_44->addWidget(label_pmin);

        label_pmin_val = new QLabel(tab_3);
        label_pmin_val->setObjectName(QStringLiteral("label_pmin_val"));
        label_pmin_val->setTextFormat(Qt::RichText);
        label_pmin_val->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        horizontalLayout_44->addWidget(label_pmin_val);


        horizontalLayout_46->addLayout(horizontalLayout_44);

        horizontalLayout_381 = new QHBoxLayout();
        horizontalLayout_381->setSpacing(6);
        horizontalLayout_381->setObjectName(QStringLiteral("horizontalLayout_381"));
        label_p_info_3 = new QLabel(tab_3);
        label_p_info_3->setObjectName(QStringLiteral("label_p_info_3"));
        label_p_info_3->setTextFormat(Qt::AutoText);

        horizontalLayout_381->addWidget(label_p_info_3);

        label_pn_val = new QLabel(tab_3);
        label_pn_val->setObjectName(QStringLiteral("label_pn_val"));
        label_pn_val->setTextFormat(Qt::RichText);
        label_pn_val->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        horizontalLayout_381->addWidget(label_pn_val);


        horizontalLayout_46->addLayout(horizontalLayout_381);

        label_7 = new QLabel(tab_3);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setFont(font3);

        horizontalLayout_46->addWidget(label_7);

        horizontalSpacer_13 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_46->addItem(horizontalSpacer_13);


        verticalLayout_10->addLayout(horizontalLayout_46);

        tabWidget_graphics->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QStringLiteral("tab_4"));
        verticalLayout_9 = new QVBoxLayout(tab_4);
        verticalLayout_9->setSpacing(6);
        verticalLayout_9->setContentsMargins(6, 6, 6, 6);
        verticalLayout_9->setObjectName(QStringLiteral("verticalLayout_9"));
        verticalLayout_9->setContentsMargins(2, 2, 2, 2);
        plot_R = new QwtPlot(tab_4);
        plot_R->setObjectName(QStringLiteral("plot_R"));

        verticalLayout_9->addWidget(plot_R);

        tabWidget_graphics->addTab(tab_4, QString());
        tab_legend = new QWidget();
        tab_legend->setObjectName(QStringLiteral("tab_legend"));
        horizontalLayout_41 = new QHBoxLayout(tab_legend);
        horizontalLayout_41->setSpacing(2);
        horizontalLayout_41->setContentsMargins(6, 6, 6, 6);
        horizontalLayout_41->setObjectName(QStringLiteral("horizontalLayout_41"));
        horizontalLayout_41->setContentsMargins(2, 2, 2, 2);
        textBrowser = new QTextBrowser(tab_legend);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));

        horizontalLayout_41->addWidget(textBrowser);

        tabWidget_graphics->addTab(tab_legend, QString());

        horizontalLayout_2->addWidget(tabWidget_graphics);

        tabWidget->addTab(tab_graphics, QString());
        tab_data = new QWidget();
        tab_data->setObjectName(QStringLiteral("tab_data"));
        verticalLayout_5 = new QVBoxLayout(tab_data);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(6, 6, 6, 6);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        tabWidget_tables = new QTabWidget(tab_data);
        tabWidget_tables->setObjectName(QStringLiteral("tabWidget_tables"));
        tabWidget_tables->setEnabled(true);
        tab_RPz_table = new QWidget();
        tab_RPz_table->setObjectName(QStringLiteral("tab_RPz_table"));
        tab_RPz_table->setEnabled(false);
        verticalLayout_25 = new QVBoxLayout(tab_RPz_table);
        verticalLayout_25->setSpacing(6);
        verticalLayout_25->setContentsMargins(6, 6, 6, 6);
        verticalLayout_25->setObjectName(QStringLiteral("verticalLayout_25"));
        tw_radius = new QTableWidget(tab_RPz_table);
        if (tw_radius->columnCount() < 8)
            tw_radius->setColumnCount(8);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tw_radius->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tw_radius->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tw_radius->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tw_radius->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tw_radius->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        tw_radius->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        tw_radius->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        tw_radius->setHorizontalHeaderItem(7, __qtablewidgetitem7);
        tw_radius->setObjectName(QStringLiteral("tw_radius"));
        tw_radius->setEnabled(false);

        verticalLayout_25->addWidget(tw_radius);

        tabWidget_tables->addTab(tab_RPz_table, QString());
        tab_RP_table = new QWidget();
        tab_RP_table->setObjectName(QStringLiteral("tab_RP_table"));
        verticalLayout_26 = new QVBoxLayout(tab_RP_table);
        verticalLayout_26->setSpacing(6);
        verticalLayout_26->setContentsMargins(6, 6, 6, 6);
        verticalLayout_26->setObjectName(QStringLiteral("verticalLayout_26"));
        tw_RP = new QTableWidget(tab_RP_table);
        if (tw_RP->columnCount() < 7)
            tw_RP->setColumnCount(7);
        QTableWidgetItem *__qtablewidgetitem8 = new QTableWidgetItem();
        tw_RP->setHorizontalHeaderItem(0, __qtablewidgetitem8);
        QTableWidgetItem *__qtablewidgetitem9 = new QTableWidgetItem();
        tw_RP->setHorizontalHeaderItem(1, __qtablewidgetitem9);
        QTableWidgetItem *__qtablewidgetitem10 = new QTableWidgetItem();
        tw_RP->setHorizontalHeaderItem(2, __qtablewidgetitem10);
        QTableWidgetItem *__qtablewidgetitem11 = new QTableWidgetItem();
        tw_RP->setHorizontalHeaderItem(3, __qtablewidgetitem11);
        QTableWidgetItem *__qtablewidgetitem12 = new QTableWidgetItem();
        tw_RP->setHorizontalHeaderItem(4, __qtablewidgetitem12);
        QTableWidgetItem *__qtablewidgetitem13 = new QTableWidgetItem();
        tw_RP->setHorizontalHeaderItem(5, __qtablewidgetitem13);
        QTableWidgetItem *__qtablewidgetitem14 = new QTableWidgetItem();
        tw_RP->setHorizontalHeaderItem(6, __qtablewidgetitem14);
        tw_RP->setObjectName(QStringLiteral("tw_RP"));

        verticalLayout_26->addWidget(tw_RP);

        tabWidget_tables->addTab(tab_RP_table, QString());

        verticalLayout_5->addWidget(tabWidget_tables);

        horizontalLayout_37 = new QHBoxLayout();
        horizontalLayout_37->setSpacing(6);
        horizontalLayout_37->setObjectName(QStringLiteral("horizontalLayout_37"));
        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_37->addItem(horizontalSpacer_12);

        pushButton_clear_table = new QPushButton(tab_data);
        pushButton_clear_table->setObjectName(QStringLiteral("pushButton_clear_table"));

        horizontalLayout_37->addWidget(pushButton_clear_table);

        pushButton_delta = new QPushButton(tab_data);
        pushButton_delta->setObjectName(QStringLiteral("pushButton_delta"));

        horizontalLayout_37->addWidget(pushButton_delta);


        verticalLayout_5->addLayout(horizontalLayout_37);

        tabWidget->addTab(tab_data, QString());
        tab_shoring = new QWidget();
        tab_shoring->setObjectName(QStringLiteral("tab_shoring"));
        verticalLayout_27 = new QVBoxLayout(tab_shoring);
        verticalLayout_27->setSpacing(6);
        verticalLayout_27->setContentsMargins(6, 6, 6, 6);
        verticalLayout_27->setObjectName(QStringLiteral("verticalLayout_27"));
        tableWidget_shoring = new QTableWidget(tab_shoring);
        if (tableWidget_shoring->columnCount() < 6)
            tableWidget_shoring->setColumnCount(6);
        QTableWidgetItem *__qtablewidgetitem15 = new QTableWidgetItem();
        tableWidget_shoring->setHorizontalHeaderItem(0, __qtablewidgetitem15);
        QTableWidgetItem *__qtablewidgetitem16 = new QTableWidgetItem();
        tableWidget_shoring->setHorizontalHeaderItem(1, __qtablewidgetitem16);
        QTableWidgetItem *__qtablewidgetitem17 = new QTableWidgetItem();
        tableWidget_shoring->setHorizontalHeaderItem(2, __qtablewidgetitem17);
        QTableWidgetItem *__qtablewidgetitem18 = new QTableWidgetItem();
        tableWidget_shoring->setHorizontalHeaderItem(3, __qtablewidgetitem18);
        QTableWidgetItem *__qtablewidgetitem19 = new QTableWidgetItem();
        tableWidget_shoring->setHorizontalHeaderItem(4, __qtablewidgetitem19);
        QTableWidgetItem *__qtablewidgetitem20 = new QTableWidgetItem();
        tableWidget_shoring->setHorizontalHeaderItem(5, __qtablewidgetitem20);
        tableWidget_shoring->setObjectName(QStringLiteral("tableWidget_shoring"));

        verticalLayout_27->addWidget(tableWidget_shoring);

        textBrowser_shoring = new QTextBrowser(tab_shoring);
        textBrowser_shoring->setObjectName(QStringLiteral("textBrowser_shoring"));

        verticalLayout_27->addWidget(textBrowser_shoring);

        tabWidget->addTab(tab_shoring, QString());

        gridLayout_2->addWidget(tabWidget, 0, 0, 1, 1);

        horizontalLayout_40 = new QHBoxLayout();
        horizontalLayout_40->setSpacing(2);
        horizontalLayout_40->setObjectName(QStringLiteral("horizontalLayout_40"));
        label_param = new QLabel(centralWidget);
        label_param->setObjectName(QStringLiteral("label_param"));

        horizontalLayout_40->addWidget(label_param);

        label_param_file = new QLabel(centralWidget);
        label_param_file->setObjectName(QStringLiteral("label_param_file"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(label_param_file->sizePolicy().hasHeightForWidth());
        label_param_file->setSizePolicy(sizePolicy2);
        QFont font4;
        font4.setItalic(true);
        label_param_file->setFont(font4);
        label_param_file->setWordWrap(true);

        horizontalLayout_40->addWidget(label_param_file);

        horizontalSpacer_4 = new QSpacerItem(10, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_40->addItem(horizontalSpacer_4);

        groupBox_5 = new QGroupBox(centralWidget);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        horizontalLayout_39 = new QHBoxLayout(groupBox_5);
        horizontalLayout_39->setSpacing(4);
        horizontalLayout_39->setContentsMargins(6, 6, 6, 6);
        horizontalLayout_39->setObjectName(QStringLiteral("horizontalLayout_39"));
        horizontalLayout_39->setContentsMargins(0, 5, 0, 0);
        button_calc_ep_cem = new QPushButton(groupBox_5);
        button_calc_ep_cem->setObjectName(QStringLiteral("button_calc_ep_cem"));
        button_calc_ep_cem->setEnabled(true);

        horizontalLayout_39->addWidget(button_calc_ep_cem);

        button_calc_rp = new QPushButton(groupBox_5);
        button_calc_rp->setObjectName(QStringLiteral("button_calc_rp"));

        horizontalLayout_39->addWidget(button_calc_rp);

        button_calc_ep = new QPushButton(groupBox_5);
        button_calc_ep->setObjectName(QStringLiteral("button_calc_ep"));
        button_calc_ep->setEnabled(true);

        horizontalLayout_39->addWidget(button_calc_ep);

        button_calc_e = new QPushButton(groupBox_5);
        button_calc_e->setObjectName(QStringLiteral("button_calc_e"));
        button_calc_e->setEnabled(true);

        horizontalLayout_39->addWidget(button_calc_e);


        horizontalLayout_40->addWidget(groupBox_5);


        gridLayout_2->addLayout(horizontalLayout_40, 2, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1036, 30));
        menu_params = new QMenu(menuBar);
        menu_params->setObjectName(QStringLiteral("menu_params"));
        menu_2 = new QMenu(menuBar);
        menu_2->setObjectName(QStringLiteral("menu_2"));
        menu_2->setEnabled(true);
        menu = new QMenu(menuBar);
        menu->setObjectName(QStringLiteral("menu"));
        menu_shorings = new QMenu(menuBar);
        menu_shorings->setObjectName(QStringLiteral("menu_shorings"));
        MainWindow->setMenuBar(menuBar);

        menuBar->addAction(menu_params->menuAction());
        menuBar->addAction(menu_2->menuAction());
        menuBar->addAction(menu_shorings->menuAction());
        menuBar->addAction(menu->menuAction());
        menu_params->addAction(action_paramload);
        menu_params->addAction(action_paramsave);
        menu_params->addAction(action_more_param);
        menu_2->addAction(action_openel_report);
        menu_2->addAction(action_open_ep_report);
        menu_2->addAction(action_html_report);
        menu_2->addAction(action_datafile);
        menu_2->addAction(actionGnuplot);
        menu->addAction(action_manual);
        menu->addAction(action_src);
        menu->addAction(action_pub);
        menu->addAction(action);
        menu_shorings->addAction(action_open_shorings);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);
        tabWidget_params->setCurrentIndex(0);
        comboBox_strength->setCurrentIndex(0);
        comboBox_calc_method->setCurrentIndex(0);
        comboBox_el_method->setCurrentIndex(0);
        tabWidget_graphics->setCurrentIndex(1);
        tabWidget_3->setCurrentIndex(0);
        tabWidget_tables->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "\320\223\320\224-4", 0));
        action_paramload->setText(QApplication::translate("MainWindow", "\320\227\320\260\320\263\321\200\321\203\320\267\320\270\321\202\321\214", 0));
        action_paramload->setShortcut(QApplication::translate("MainWindow", "Ctrl+O", 0));
        action_paramsave->setText(QApplication::translate("MainWindow", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214", 0));
        action_paramsave->setShortcut(QApplication::translate("MainWindow", "Ctrl+S", 0));
        action_openel_report->setText(QApplication::translate("MainWindow", "\320\264\320\273\321\217 \321\203\320\277\321\200\321\203\320\263\320\276\320\271 \320\267\320\276\320\275\321\213\\\320\267\320\260\320\264\320\260\321\207\320\270 (html)", 0));
        action_open_ep_report->setText(QApplication::translate("MainWindow", "\320\264\320\273\321\217 \320\263\320\276\321\200\320\275\320\276\320\263\320\276 \320\264\320\260\320\262\320\273\320\265\320\275\320\270\321\217 (html)", 0));
        action_html_report->setText(QApplication::translate("MainWindow", "\321\201\320\276\320\267\320\264\320\260\320\262\320\260\321\202\321\214 \320\276\321\202\321\207\321\221\321\202 \320\262 html", 0));
        action_datafile->setText(QApplication::translate("MainWindow", "\321\201\320\276\321\205\321\200\320\260\320\275\321\217\321\202\321\214 \320\264\320\260\320\275\320\275\321\213\320\265 \320\262 \321\204\320\260\320\271\320\273", 0));
        action111->setText(QApplication::translate("MainWindow", "111", 0));
        action_src->setText(QApplication::translate("MainWindow", "\320\230\321\201\321\205\320\276\320\264\320\275\321\213\320\271 \320\272\320\276\320\264", 0));
        action_pub->setText(QApplication::translate("MainWindow", "\320\237\321\203\320\261\320\273\320\270\320\272\320\260\321\206\320\270\320\270", 0));
        action->setText(QApplication::translate("MainWindow", "O \320\277\321\200\320\276\320\263\321\200\320\260\320\274\320\274\320\265", 0));
        action_manual->setText(QApplication::translate("MainWindow", "P\321\203\320\272\320\276\320\262\320\276\320\264\321\201\321\202\320\262\320\276 \320\277\320\276\320\273\321\214\320\267\320\276\320\262\320\260\321\202\320\265\320\273\321\217 (pdf)", 0));
        action_manual->setShortcut(QApplication::translate("MainWindow", "F1", 0));
        action_more_param->setText(QApplication::translate("MainWindow", "\320\264\320\276\320\277. \320\277\320\260\321\200\320\260\320\274\320\265\321\202\321\200\321\213", 0));
        action_open_shorings->setText(QApplication::translate("MainWindow", "\320\277\321\200\320\276\321\201\320\274\320\276\321\202\321\200\320\265\321\202\321\214 \320\264\320\276\321\201\321\202\321\203\320\277\320\275\321\213\320\265", 0));
        actionGnuplot->setText(QApplication::translate("MainWindow", "\320\264\320\260\320\275\320\275\321\213\320\265 \320\264\320\273\321\217 gnuplot (txt)", 0));
        groupBox->setTitle(QApplication::translate("MainWindow", "\321\201\320\265\321\202\320\272\320\260 \320\270 \321\200\320\260\320\267\320\274\320\265\321\200\321\213 [\320\234\320\232\320\240]", 0));
        label->setText(QApplication::translate("MainWindow", "\321\207\320\270\321\201\320\273\320\276 \321\202\320\276\321\207\320\265\320\272", 0));
        label_5->setText(QApplication::translate("MainWindow", "   \321\210\320\260\320\263:", 0));
        label_step->setText(QApplication::translate("MainWindow", "00000 \320\274\320\274", 0));
        label_2->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt; font-weight:600;\">b</span><span style=\" font-family:'MS Shell Dlg 2'; font-size:8pt; font-weight:600; vertical-align:sub;\"> </span><span style=\" font-family:'MS Shell Dlg 2'; font-size:8pt;\"> \321\210\320\270\321\200\320\270\320\275\320\260 \320\262\321\213\321\200\320\260\320\261\320\276\321\202\320\272\320\270 (\320\274)</span></p></body></html>", 0));
#ifndef QT_NO_TOOLTIP
        label_3->setToolTip(QApplication::translate("MainWindow", "<html><head/><body><p>\321\200\320\260\321\201\321\201\321\202\320\276\321\217\320\275\320\270\320\265 \320\276\321\202 \321\206\320\265\320\275\321\202\321\200\320\260 \320\262\321\213\321\200\320\260\320\261\320\276\321\202\320\272\320\270, \320\275\320\260 \320\272\320\276\321\202\320\276\321\200\320\276\320\274 \320\265\321\221 \320\262\320\273\320\270\321\217\320\275\320\270\320\265 \320\275\320\265 \321\201\321\203\321\211\320\265\321\201\321\202\320\262\320\265\320\275\320\275\320\276</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        label_3->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt; font-weight:600;\">r</span><span style=\" font-size:12pt; font-weight:600; vertical-align:sub;\">b </span>(\320\265\320\264. <span style=\" font-size:12pt;\">r</span><span style=\" font-size:12pt; vertical-align:sub;\">a</span>)</p></body></html>", 0));
        label_32->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt; font-weight:600;\">r</span><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt; font-weight:600; vertical-align:sub;\">b </span>(\320\274\320\274)</p></body></html>", 0));
        label_rb_mm->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        label_8->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt; font-weight:600;\">r</span><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt; font-weight:600; vertical-align:sub;\">a</span><span style=\" font-family:'MS Shell Dlg 2'; font-size:8pt; font-weight:600; vertical-align:sub;\"> </span><span style=\" font-family:'MS Shell Dlg 2'; font-size:8pt;\"> \321\200\320\260\320\264\320\270\321\203\321\201 \320\262\321\213\321\200\320\260\320\261\320\276\321\202\320\272\320\270 (\320\274\320\274)</span></p></body></html>", 0));
        label_33->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt; font-weight:600;\">r*</span><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt;\"> </span><span style=\" font-family:'MS Shell Dlg 2'; font-size:8pt;\">(\320\265\320\264. </span><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt;\">r</span><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt; vertical-align:sub;\">a, </span><span style=\" font-family:'MS Shell Dlg 2';\">\320\274\320\274</span><span style=\" font-family:'MS Shell Dlg 2'; font-size:8pt;\">)</span></p></body></h"
                        "tml>", 0));
        label_rzval->setText(QApplication::translate("MainWindow", "2234", 0));
        label_700->setText(QApplication::translate("MainWindow", "r \321\206\320\265\320\274\320\265\320\275\321\202\320\260\321\206\320\270\320\270 (\320\274\320\274)", 0));
#ifndef QT_NO_TOOLTIP
        checkBox_elproblem->setToolTip(QApplication::translate("MainWindow", "<html><head/><body><p>\320\262\320\274\320\265\321\201\321\202\320\276 \321\203\321\201\320\273\320\276\320\262\320\270\321\217 \320\277\321\200\320\276\321\207\320\275\320\276\321\201\321\202\320\270</p><p>\320\270\321\201\320\277\320\276\320\273\321\214\320\267\320\276\320\262\320\260\321\202\321\214: <span style=\" font-weight:600;\">\317\203</span><span style=\" font-weight:600; vertical-align:sub;\">r</span><span style=\" font-weight:600;\">=P</span></p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        checkBox_elproblem->setText(QApplication::translate("MainWindow", "\321\203\320\277\321\200\321\203\320\263\320\260\321\217 \320\267\320\260\320\264\320\260\321\207\320\260", 0));
        groupBox_sc->setTitle(QApplication::translate("MainWindow", "\321\203\321\201\320\273\320\276\320\262\320\270\320\265 \320\277\321\200\320\276\321\207\320\275\320\276\321\201\321\202\320\270", 0));
        comboBox_strength->clear();
        comboBox_strength->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "\317\203_\316\270 = \316\262*\317\203_r + \317\203_\321\201\320\266", 0)
         << QApplication::translate("MainWindow", "a + b*\317\203_r + c * (\317\203_r)^2 - \317\203_\316\270 = 0", 0)
        );
        groupBox_6->setTitle(QApplication::translate("MainWindow", "a, b, c", 0));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "\320\264\320\273\321\217 \320\267\320\260\320\264\320\260\321\207\320\270 \320\263\320\276\321\200\320\275\320\276\320\263\320\276 \320\264\320\260\320\262\320\273\320\265\320\275\320\270\321\217", 0));
        comboBox_calc_method->clear();
        comboBox_calc_method->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "\320\274\320\265\321\202\320\276\320\264 1 [T]", 0)
         << QApplication::translate("MainWindow", "\320\274\320\265\321\202\320\276\320\264 2 (\321\201 \320\264\320\270\320\273\320\260\321\202.)", 0)
        );
        label_37->setText(QApplication::translate("MainWindow", "\321\200\320\265\321\210\320\265\320\275\320\270\320\265 \320\264\320\273\321\217 \321\203\320\277\321\200\321\203\320\263\320\276\320\271 \320\267\320\276\320\275\321\213", 0));
        comboBox_el_method->clear();
        comboBox_el_method->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "\321\207\320\270\321\201\320\273. \321\200\320\265\321\210\320\265\320\275\320\270\320\265", 0)
         << QApplication::translate("MainWindow", "\320\260\320\275\320\260\320\273\320\270\321\202. \321\200\320\265\321\210\320\265\320\275\320\270\320\265 \320\264\320\273\321\217 R*", 0)
        );
        groupBox_4->setTitle(QApplication::translate("MainWindow", "\320\264\320\270\320\260\320\277\320\260\320\267\320\276\320\275 \321\200\320\260\320\264\320\270\321\203\321\201\320\260 \320\267\320\260\320\277\321\200\320\265\320\264\320\265\320\273\321\214\320\275\320\276\320\271 \320\267\320\276\320\275\321\213 (r*/ra)", 0));
        label_6->setText(QApplication::translate("MainWindow", "\320\276\321\202", 0));
        label_12->setText(QApplication::translate("MainWindow", "\320\264\320\276", 0));
        label_31->setText(QApplication::translate("MainWindow", "\321\207\320\270\321\201\320\273\320\276 \321\202\320\276\321\207\320\265\320\272", 0));
        groupBox_3->setTitle(QApplication::translate("MainWindow", "\320\277\320\260\321\200\320\260\320\274\320\265\321\202\321\200\321\213 \321\201\321\200\320\265\320\264\321\213", 0));
        checkBox_burr->setText(QApplication::translate("MainWindow", "\321\203\321\207\320\270\321\202\321\213\320\262\320\260\321\202\321\214 \320\262\320\273\320\270\321\217\320\275\320\270\320\265 \320\261\321\203\321\200\320\276\320\262\320\267\321\200\321\213\320\262\320\275\321\213\321\205 \321\200\320\260\320\261\320\276\321\202", 0));
        label_Er->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">E(r) =Em * (1 - a*r</span><span style=\" font-size:12pt; vertical-align:super;\">k</span><span style=\" font-size:12pt;\">)</span></p></body></html>", 0));
        label_4->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-weight:600;\">E</span><span style=\" font-family:'MS Shell Dlg 2'; font-weight:600; vertical-align:sub;\">lab</span><span style=\" font-family:'MS Shell Dlg 2'; vertical-align:sub;\">.</span><span style=\" font-family:'MS Shell Dlg 2'; font-weight:600; vertical-align:sub;\"> </span><span style=\" font-family:'MS Shell Dlg 2';\">\320\273\320\260\320\261\320\276\321\200\320\260\321\202\320\276\321\200\320\275\321\213\320\271 </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:"
                        "0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\320\274\320\276\320\264\321\203\320\273\321\214 \320\256\320\275\320\263\320\260 (\320\234\320\237\320\260)</span></p></body></html>", 0));
        label_10->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-weight:600;\">\316\274</span><span style=\" font-family:'MS Shell Dlg 2';\"> \320\272\320\276\321\215\321\204\321\204\320\270\321\206\320\270\320\265\320\275\321\202 \320\237\321\203\320\260\321\201\321\201\320\276\320\275\320\260</span></p></body></html>", 0));
#ifndef QT_NO_TOOLTIP
        label_34->setToolTip(QString());
#endif // QT_NO_TOOLTIP
        label_34->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt; font-weight:600;\">P </span><span style=\" font-family:'MS Shell Dlg 2'; font-size:8pt;\">\320\264\320\260\320\262\320\273\320\265\320\275\320\270\320\265 \320\275\320\260 \320\272\320\276\320\275\321\202\321\203\321\200 \320\262\321\213\321\200\320\260\320\261\320\276\321\202\320\272\320\270 (\320\234\320\237\320\260)</span></p></body></html>", 0));
        label_16->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-weight:600;\">S </span><span style=\" font-family:'MS Shell Dlg 2';\">\320\265\321\201\321\202\320\265\321\201\321\202\320\262\320\265\320\275\320\275\320\276\320\265 \320\264\320\260\320\262\320\273\320\265\320\275\320\270\320\265 (\320\234\320\237\320\260)</span></p></body></html>", 0));
        label_30->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\320\272\320\276\321\215\321\204-\321\202 \320\277\320\265\321\200\320\265\320\263\321\200\321\203\320\267\320\272\320\270, \321\203\320\262\320\265\320\273\320\270\321\207\320\270\320\262\321\216\321\211\320\270\320\271</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\320\265\321\201\321\202\320\265\321\201\321\202\320\262\320\265\320\275\320\275\320\276\320\265 \320\264\320\260\320\262"
                        "\320\273\320\265\320\275\320\270\320\265 \320\262 \320\274\320\260\321\201\321\201\320\270\320\262\320\265</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">(1&lt;</span><span style=\" font-family:'MS Shell Dlg 2'; font-weight:600;\">k</span><span style=\" font-family:'MS Shell Dlg 2'; font-weight:600; vertical-align:sub;\">p</span><span style=\" font-family:'MS Shell Dlg 2';\">&lt;2)</span></p></body></html>", 0));
        label_19->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-weight:600;\">\317\203</span><span style=\" font-family:'MS Shell Dlg 2'; font-weight:600; vertical-align:sub;\">lab</span><span style=\" font-family:'MS Shell Dlg 2'; font-weight:600; vertical-align:super;\">\321\201\320\266</span><span style=\" font-family:'MS Shell Dlg 2'; font-weight:600; vertical-align:sub;\"> </span><span style=\" font-family:'MS Shell Dlg 2';\">\320\277\321\200\320\265\320\264\320\265\320\273 \320\277\321\200\320\276\321\207\320\275\320\276\321\201\321\202\320\270 \320\275\320\260 \321\201\320"
                        "\266\320\260\321\202\320\270\320\265 </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\320\273\320\260\320\261\320\276\321\200\320\260\321\202\320\276\321\200\320\275\320\276\320\263\320\276 \320\276\320\261\321\200\320\260\320\267\321\206\320\260 (\320\234\320\237\320\260)</span></p></body></html>", 0));
        label_35->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-weight:600;\">\317\203</span><span style=\" font-family:'MS Shell Dlg 2'; font-weight:600; vertical-align:sub;\">lab</span><span style=\" font-family:'MS Shell Dlg 2'; font-weight:600; vertical-align:super;\">\321\200</span><span style=\" font-family:'MS Shell Dlg 2'; font-weight:600; vertical-align:sub;\"> </span><span style=\" font-family:'MS Shell Dlg 2';\">\320\277\321\200\320\265\320\264\320\265\320\273 \320\277\321\200\320\276\321\207\320\275\320\276\321\201\321\202\320\270 \320\275\320\260 \321\200\320\260\321"
                        "\201\321\202\321\217\320\266\320\265\320\275\320\270\320\265 </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\320\273\320\260\320\261\320\276\321\200\320\260\321\202\320\276\321\200\320\275\320\276\320\263\320\276 \320\276\320\261\321\200\320\260\320\267\321\206\320\260 (\320\234\320\237\320\260)</span></p></body></html>", 0));
        label_20->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-weight:600;\">k</span><span style=\" font-family:'MS Shell Dlg 2'; font-weight:600; vertical-align:sub;\">o </span><span style=\" font-family:'MS Shell Dlg 2';\">\320\272\320\276\321\215\321\204-\321\202 \321\201\321\202\321\200\321\203\320\272\321\202\321\203\321\200\320\275\320\276\320\263\320\276 </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\320\276\321\201\320\273\320\260\320"
                        "\261\320\273\320\265\320\275\320\270\321\217 \320\274\320\260\321\201\321\201\320\270\320\262\320\260</span></p></body></html>", 0));
#ifndef QT_NO_TOOLTIP
        label_11->setToolTip(QApplication::translate("MainWindow", "<html><head/><body><p>\320\275\320\260\320\270\320\274\320\265\320\275\321\214\321\210\320\265\320\265 \321\200\320\260\320\267\320\273\320\270\321\207\320\270\320\274\320\276\320\265 \320\267\320\275\320\260\321\207\320\265\320\275\320\270\320\265 \320\270\320\267\320\274\320\265\320\275\320\265\320\275\320\270\321\217 \320\267\320\275\320\260\321\207\320\265\320\275\320\270\321\217 \321\206\320\265\320\273\320\265\320\262\320\276\320\271 \321\204\321\203\320\275\320\272\321\206\320\270\320\270 \320\275\320\265\320\273\320\270\320\275\320\265\320\271\320\275\320\276\320\263\320\276 \320\277\321\200\320\276\320\263\321\200\320\260\320\274\320\274\320\270\321\200\320\276\320\262\320\260\320\275\320\270\321\217</p><p><br/></p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        label_11->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\321\202\320\276\321\207\320\275\320\276\321\201\321\202\321\214 (\320\267\320\275\320\260\320\272\320\276\320\262 \320\277\320\276\321\201\320\273\320\265 \320\267\320\260\320\277\321\217\321\202\320\276\320\271)</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">[\320\275\320\265\320\273\320\270\320\275\320\265\320\271\320\275\320\276\320\265 \320\277\321\200\320\276\320\263\321\200\320"
                        "\260\320\274\320\274\320\270\321\200\320\276\320\262\320\260\320\275\320\270\320\265]</span></p></body></html>", 0));
        label_eps->setText(QApplication::translate("MainWindow", "0,000000", 0));
        tabWidget_params->setTabText(tabWidget_params->indexOf(elastic), QApplication::translate("MainWindow", "1", 0));
        label_13->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\320\277\320\276\320\273\321\217\321\200\320\275\321\213\320\271 \321\203\320\263\320\276\320\273 </span><span style=\" font-family:'MS Shell Dlg 2'; font-weight:600;\">\316\251</span><span style=\" font-family:'MS Shell Dlg 2';\"> (\320\263\321\200\320\260\320\264\321\203\321\201\321\213), </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\320\272\321\200\320\276\320\262\320\273\321\217"
                        " \320\262\321\213\321\200\320\260\320\261\320\276\321\202\320\272\320\270 = 90</span><span style=\" font-family:'MS Shell Dlg 2'; vertical-align:super;\">o</span></p></body></html>", 0));
        label_15->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\320\277\320\273\320\276\321\202\320\275\320\276\321\201\321\202\321\214 \320\277\320\276\321\200\320\276\320\264 (\321\202\320\276\320\275\320\275/\320\274</span><span style=\" font-family:'MS Shell Dlg 2'; vertical-align:super;\">3</span><span style=\" font-family:'MS Shell Dlg 2';\">)</span></p></body></html>", 0));
        checkBox_calcff->setText(QApplication::translate("MainWindow", "\320\222\321\213\321\207\320\270\321\201\320\273\321\217\321\202\321\214 \321\203\320\263\320\273\321\213 \320\262\320\275\321\203\321\202\321\200\320\265\320\275\320\275\320\265\320\263\320\276 \321\202\321\200\320\265\320\275\320\270\321\217", 0));
        label_18->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt; font-weight:600;\">\317\206</span><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt; font-weight:600; vertical-align:sub;\">0 </span><span style=\" font-family:'MS Shell Dlg 2'; font-size:8pt;\">\321\203\320\263\320\276\320\273 \320\262\320\275\321\203\321\202\321\200\320\265\320\275\320\275\320\265\320\263\320\276 \321\202\321\200\320\265\320\275\320\270\321\217 (\320\263\321\200\320\260\320\264\321\203\321\201\321\213),</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-"
                        "left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-size:8pt;\">\320\262\321\205\320\276\320\264\321\217\321\210\320\270\320\271 \320\262 \321\203\321\201\320\273\320\276\320\262\320\270\320\265 \320\277\321\200\320\276\321\207\320\275\320\276\321\201\321\202\320\270</span></p></body></html>", 0));
        label_17->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt; font-weight:600;\">\317\206</span><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt; font-weight:600; vertical-align:sub;\">1 </span><span style=\" font-family:'MS Shell Dlg 2'; font-size:8pt;\">\321\203\320\263\320\276\320\273 \320\262\320\275\321\203\321\202\321\200\320\265\320\275\320\275\320\265\320\263\320\276 \321\202\321\200\320\265\320\275\320\270\321\217 (\320\263\321\200\320\260\320\264\321\203\321\201\321\213),</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-"
                        "left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-size:8pt;\">\320\262\321\205\320\276\320\264\321\217\321\210\320\270\320\271 \320\262 \320\264\320\270\320\273\320\260\321\202\320\260\320\275\321\201\320\270\320\276\320\275\320\275\320\276\320\265 \321\201\320\276\320\276\321\202-\320\265</span></p></body></html>", 0));
        label_23->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\320\277\321\200\320\265\320\264\320\265\320\273\321\214\320\275\320\276\320\265 \320\277\320\265\321\200\320\265\320\274\320\265\321\211\320\265\320\275\320\270\320\265 </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\320\272\320\276\320\275\321\202\321\203\321\200\320\260 \320\262\321\213\321\200\320\260\320\261\320\276\321\202\320\272\320\270, </span><span style=\" font-family:'MS "
                        "Shell Dlg 2'; font-weight:600;\">upr</span><span style=\" font-family:'MS Shell Dlg 2';\"> (\321\201\320\274)</span></p></body></html>", 0));
        label_22->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\320\277\321\200\320\265\320\264\320\265\320\273\321\214\320\275\320\276\320\265 \320\267\320\275\320\260\321\207. \320\276\321\202\320\275\320\276\321\201-\320\263\320\276 \320\276\320\261\321\212\321\221\320\274\320\275\320\276\320\263\320\276</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\321\200\320\260\321\201\321\210\320\270\321\200\320\265\320\275\320\270\321\217 \320\262 \320"
                        "\274\320\260\321\201\321\201\320\270\320\262\320\265 </span><span style=\" font-family:'MS Shell Dlg 2'; font-weight:600;\">ttk</span><span style=\" font-family:'MS Shell Dlg 2';\"> (~ 0.12)</span></p></body></html>", 0));
        label_26->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\320\277\321\200\320\265\320\264\320\265\320\273\321\214\320\275\320\260\321\217 \320\276\321\201\321\202\320\260\321\202\320\276\321\207\320\275\320\260\321\217</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\320\277\321\200\320\276\321\207\320\275\320\276\321\201\321\202\321\214 </span><span style=\" font-family:'MS Shell Dlg 2'; font-weight:600;\">ssgp</span><span style=\" font-fam"
                        "ily:'MS Shell Dlg 2';\">, (%)</span></p></body></html>", 0));
        label_29->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\320\272\320\276\321\215\321\204-\321\202, \321\203\320\262\320\265\320\273\320\270\321\207\320\270\320\262\320\260\321\216\321\211\320\270\320\271 \320\277\320\260\321\200\320\260\320\274\320\265\321\202\321\200 \320\276\321\202 </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\320\272\320\276\321\202\320\276\321\200\320\276\320\263\320\276 \320\267\320\260\320\262\320\270\321\201\320"
                        "\270\321\202 \321\201\320\275\320\270\320\266\320\265\320\275\320\270\320\265 \320\277\321\200\320\276\321\207\320\275\320\276\321\201\321\202\320\270 </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\320\262 \320\277\321\200\320\276\321\206\320\265\321\201\321\201\320\265 \321\200\320\260\320\267\321\200\321\213\321\205\320\273\320\265\320\275\320\270\321\217 (0&lt;</span><span style=\" font-family:'MS Shell Dlg 2'; font-weight:600;\">ku</span><span style=\" font-family:'MS Shell Dlg 2';\">&lt;1)</span></p></body></html>", 0));
        label_28->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\320\272\320\276\321\215\321\204-\321\202 \320\264\320\270\320\275\320\260\320\274\320\270\321\207\320\275\320\276\321\201\321\202\320\270 </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\320\277\321\200\320\270 \320\262\321\213\320\262\320\260\320\273\320\265 \320\277\320\276\321\200\320\276\320\264 \320\262 \320\272\321\200\320\276\320\262\320\273\320\265, </span><span style=\" font"
                        "-family:'MS Shell Dlg 2'; font-weight:600;\">kd</span></p></body></html>", 0));
        label_27->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\320\272\320\276\321\215\321\204-\321\202 \320\264\320\273\320\270\321\202\320\265\320\273\321\214\320\275\320\276\320\271 \320\277\321\200\320\276\321\207\320\275\320\276\321\201\321\202\320\270 </span><span style=\" font-family:'MS Shell Dlg 2'; font-weight:600;\">ke</span></p></body></html>", 0));
        label_25->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\320\274\320\260\320\272\321\201. \320\277\320\265\321\200\320\265\320\274\320\265\321\211\320\265\320\275\320\270\320\265, \320\277\321\200\320\270 \320\272\320\276\321\202\320\276\321\200\320\276\320\274 \320\272\321\200\320\265\320\277\321\214</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\320\277\320\276\320\273\320\275\320\276\321\201\321\202\321\214\321\216 \320\262\321\201\321"
                        "\202\321\203\320\277\320\260\320\265\321\202 \320\262 \321\200\320\260\320\261\320\276\321\202\321\203 </span><span style=\" font-family:'MS Shell Dlg 2'; font-weight:600;\">uw</span><span style=\" font-family:'MS Shell Dlg 2';\"> (\321\201\320\274)</span></p></body></html>", 0));
        label_24->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-size:8pt;\">\320\275\320\265\321\201\321\203\321\211\320\260\321\217 \321\201\320\277\320\276\321\201\320\276\320\261\320\275\320\276\321\201\321\202\321\214</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-size:8pt;\">\320\272\321\200\320\265\320\277\320\270 </span><span style=\" font-family:'MS Shell Dlg 2'; font-size:8pt; font-weight:600;\">\320\240\320\275</span><span style=\""
                        " font-family:'MS Shell Dlg 2'; font-size:8pt;\"> (\320\272\320\237\320\260)</span></p></body></html>", 0));
        label_21->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\320\272\320\276\321\215\321\204-\321\202 \321\203\321\201\320\273\320\276\320\262\320\270\321\217 \321\200\320\260\320\261\320\276\321\202\321\213 \320\272\321\200\320\265\320\277\320\270 </span><span style=\" font-family:'MS Shell Dlg 2'; font-weight:600;\">km</span></p></body></html>", 0));
        label_14->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\321\200\320\260\321\201\321\201\321\202\320\276\321\217\320\275\320\270\320\265 \320\274\320\265\320\266\320\264\321\203 \320\276\320\272\320\276\320\275\321\202\321\203\321\200\320\270-</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">\320\262\320\260\321\216\321\211\320\265\320\274\320\270 \321\210\320\277\321\203\321\200\320\260\320\274\320\270 </span><span style=\" font-family:'MS "
                        "Shell Dlg 2'; font-weight:600;\">br</span><span style=\" font-family:'MS Shell Dlg 2';\"> (\320\274\320\274)</span></p></body></html>", 0));
        tabWidget_params->setTabText(tabWidget_params->indexOf(other), QApplication::translate("MainWindow", "2", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_param), QApplication::translate("MainWindow", "\320\277\320\260\321\200\320\260\320\274\320\265\321\202\321\200\321\213", 0));
        tabWidget_3->setTabText(tabWidget_3->indexOf(tab_eu), QApplication::translate("MainWindow", "u", 0));
        tabWidget_3->setTabText(tabWidget_3->indexOf(tab_essr), QApplication::translate("MainWindow", "ssr", 0));
        tabWidget_3->setTabText(tabWidget_3->indexOf(tab_esst), QApplication::translate("MainWindow", "sst", 0));
        tabWidget_3->setTabText(tabWidget_3->indexOf(tab_eFoS), QApplication::translate("MainWindow", "FoS", 0));
        tabWidget_3->setTabText(tabWidget_3->indexOf(tab_k), QApplication::translate("MainWindow", "k(\320\262\321\213\320\277\320\276\320\273\320\275\321\217\320\265\321\202\321\201\321\217 \320\273\320\270 \321\203\321\201\320\273\320\276\320\262\320\270\320\265 \320\277\321\200\320\276\321\207\320\275\320\276\321\201\321\202\320\270)", 0));
        label_ff->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600;\">f = </span></p></body></html>", 0));
        tabWidget_graphics->setTabText(tabWidget_graphics->indexOf(tab_e), QApplication::translate("MainWindow", "\321\200\320\260\320\264\320\270\320\260\320\273\321\214\320\275\321\213\320\265", 0));
        label_shoring_c->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt;\">\320\241\320\276\321\201\321\202\320\276\321\217\320\275\320\270\320\265 \320\272\321\200\320\265\320\277\320\270: </span></p></body></html>", 0));
        label_shoring_c_2->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt;\">\320\275\320\265\321\202 (\320\275\320\260\320\266\320\274\320\270\321\202\320\265 \320\272\320\275\320\276\320\277\320\272\321\203 </span><span style=\" font-size:12pt; font-weight:600;\">\320\223\320\224</span><span style=\" font-size:12pt;\">)</span></p></body></html>", 0));
        label_pmax->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt;\">P</span><span style=\" font-size:12pt; vertical-align:sub;\">max</span><span style=\" font-size:12pt;\">:</span></p></body></html>", 0));
        label_pmax_val->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt;\">-</span></p></body></html>", 0));
        label_pmin->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt;\">P</span><span style=\" font-size:12pt; vertical-align:sub;\">min</span><span style=\" font-size:12pt;\">:</span></p></body></html>", 0));
        label_pmin_val->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt;\">-</span></p></body></html>", 0));
        label_p_info_3->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt;\">P</span><span style=\" font-size:12pt; vertical-align:sub;\">\320\275</span><span style=\" font-size:12pt;\">:</span></p></body></html>", 0));
        label_pn_val->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt;\">-</span></p></body></html>", 0));
        label_7->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt;\"> [\320\272\320\237\320\260]</span></p></body></html>", 0));
        tabWidget_graphics->setTabText(tabWidget_graphics->indexOf(tab_3), QApplication::translate("MainWindow", "\320\223\320\224. {P, P_ob (u_a)}", 0));
        tabWidget_graphics->setTabText(tabWidget_graphics->indexOf(tab_4), QApplication::translate("MainWindow", "rk/ra, rz/ra, rp/ra, (u_a)", 0));
        textBrowser->setHtml(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Helvetica'; font-size:8pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt; font-weight:600;\">u_a</span><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt;\"> - \320\262\320\270\320\264\320\270\320\274\321\213\320\265 \321\200\320\260\320\264\320\270\320\260\320\273\321\214\320\275\321\213\320\265 \320\277\320\265\321\200\320\265\320\274\320\265\321\211\320\265\320\275\320\270\321\217 \320\275\320\260 \320\272\320\276\320\275\321\202\321\203\321\200\320\265 \320\262\321\213\321\200\320\260\320\261\320\276\321\202\320\272\320\270</span></p>\n"
"<p style=\" margin-top:"
                        "0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt; font-weight:600;\">ssr - \317\203 </span><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt; font-weight:600; vertical-align:sub;\">r</span><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt;\"> - \321\200\320\260\320\264\320\270\320\260\320\273\321\214\320\275\321\213\320\265 \320\275\320\260\320\277\321\200\321\217\320\266\320\265\320\275\320\270\321\217</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt; font-weight:600;\">sst - \317\203</span><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt; font-weight:600; vertical-align:sub;\">\316\270</span><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt;\"> - \320\276\320\272\321\200\321\203\320\266\320\275\321\213\320\265 \320\275"
                        "\320\260\320\277\321\200\321\217\320\266\320\265\320\275\320\270\321\217</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt; font-weight:600;\">FoS (factor of safety)</span><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt;\">- \320\272\320\276\321\215\321\204\321\204\320\270\321\206\320\270\320\265\320\275\321\202 \320\267\320\260\320\277\320\260\321\201\320\260</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt; font-weight:600;\">P\320\276b</span><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt;\"> - \320\264\320\260\320\262\320\273\320\265\320\275\320\270\320\265 \320\276\320\261\321\200\321\203\321\210\320\265\320\275\320\270\321\217</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margi"
                        "n-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt; font-weight:600;\">P</span><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt;\"> - \320\264\320\260\320\262\320\273\320\265\320\275\320\270\320\265 \320\275\320\260 \320\272\320\276\320\275\321\202\321\203\321\200\320\265 \320\262\321\213\321\200\320\260\320\261\320\276\321\202\320\272\320\270</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt; font-weight:600;\">rz </span><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt;\">- </span><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt; font-weight:600;\">r* -</span><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt;\"> \320\262\320\275\320\265\321\210\320\275\321\217\321\217 \320\263\321\200\320\260\320\275\320\270\321\206\320\260 \320\267\320\276\320\275"
                        "\321\213 \320\275\320\265\321\203\320\277\321\200\321\203\320\263\320\270\321\205 \320\264\320\265\321\204\320\276\321\200\320\274\320\260\321\206\320\270\320\271</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt; font-weight:600;\">rp </span><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt;\">- \320\262\320\275\320\265\321\210\320\275\320\270\320\271 \321\200\320\260\320\264\320\270\321\203\321\201 \320\263\321\200\320\260\320\275\320\270\321\206\321\213 \320\277\321\200\320\265\320\264\320\265\320\273\321\214\320\275\320\276 \321\200\320\260\320\267\321\200\321\203\321\210\320\265\320\275\320\275\320\276\320\271 \320\267\320\276\320\275\321\213</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt; font-weight:600;"
                        "\">rk </span><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt;\">- \320\262\320\275\320\265\321\210\320\275\320\270\320\271 \321\200\320\260\320\264\320\270\321\203\321\201 \320\263\321\200\320\260\320\275\320\270\321\206\321\213 \320\277\321\200\320\265\320\264\320\265\320\273\321\214\320\275\320\276 \321\200\320\260\320\267\321\200\321\213\321\205\320\273\320\265\320\275\320\275\320\276\320\271 \320\267\320\276\320\275\321\213</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:'MS Shell Dlg 2'; font-size:12pt;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt;\"><br /></span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS S"
                        "hell Dlg 2'; font-size:12pt;\"><br /></span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt;\"><br /></span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2'; font-size:12pt;\"><br /></span></p></body></html>", 0));
        tabWidget_graphics->setTabText(tabWidget_graphics->indexOf(tab_legend), QApplication::translate("MainWindow", "\320\276\320\261\320\276\320\267\320\275\320\260\321\207\320\265\320\275\320\270\321\217", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_graphics), QApplication::translate("MainWindow", "\320\263\321\200\320\260\321\204\320\270\320\272\320\270", 0));
        QTableWidgetItem *___qtablewidgetitem = tw_radius->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("MainWindow", "r*", 0));
        QTableWidgetItem *___qtablewidgetitem1 = tw_radius->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("MainWindow", "u_a (\320\274\320\274)", 0));
        QTableWidgetItem *___qtablewidgetitem2 = tw_radius->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("MainWindow", "u* (\320\274\320\274)", 0));
        QTableWidgetItem *___qtablewidgetitem3 = tw_radius->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QApplication::translate("MainWindow", "ssr_a / S", 0));
        QTableWidgetItem *___qtablewidgetitem4 = tw_radius->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QApplication::translate("MainWindow", "ssr* / S", 0));
        QTableWidgetItem *___qtablewidgetitem5 = tw_radius->horizontalHeaderItem(5);
        ___qtablewidgetitem5->setText(QApplication::translate("MainWindow", "sst_a / S", 0));
        QTableWidgetItem *___qtablewidgetitem6 = tw_radius->horizontalHeaderItem(6);
        ___qtablewidgetitem6->setText(QApplication::translate("MainWindow", "sst* / S", 0));
        QTableWidgetItem *___qtablewidgetitem7 = tw_radius->horizontalHeaderItem(7);
        ___qtablewidgetitem7->setText(QApplication::translate("MainWindow", "note", 0));
        tabWidget_tables->setTabText(tabWidget_tables->indexOf(tab_RPz_table), QApplication::translate("MainWindow", "\320\223\320\224*", 0));
        QTableWidgetItem *___qtablewidgetitem8 = tw_RP->horizontalHeaderItem(0);
        ___qtablewidgetitem8->setText(QApplication::translate("MainWindow", "ua_0 (\320\274\320\274)", 0));
        QTableWidgetItem *___qtablewidgetitem9 = tw_RP->horizontalHeaderItem(1);
        ___qtablewidgetitem9->setText(QApplication::translate("MainWindow", "P (\320\234\320\237\320\260)", 0));
        QTableWidgetItem *___qtablewidgetitem10 = tw_RP->horizontalHeaderItem(2);
        ___qtablewidgetitem10->setText(QApplication::translate("MainWindow", "P x Pob (\320\234\320\237\320\260)", 0));
        QTableWidgetItem *___qtablewidgetitem11 = tw_RP->horizontalHeaderItem(3);
        ___qtablewidgetitem11->setText(QApplication::translate("MainWindow", "P x Pob (\320\274\320\274)", 0));
        QTableWidgetItem *___qtablewidgetitem12 = tw_RP->horizontalHeaderItem(4);
        ___qtablewidgetitem12->setText(QApplication::translate("MainWindow", "P_max (\320\234\320\237\320\260)", 0));
        QTableWidgetItem *___qtablewidgetitem13 = tw_RP->horizontalHeaderItem(5);
        ___qtablewidgetitem13->setText(QApplication::translate("MainWindow", "P_min (\320\234\320\237\320\260)", 0));
        QTableWidgetItem *___qtablewidgetitem14 = tw_RP->horizontalHeaderItem(6);
        ___qtablewidgetitem14->setText(QApplication::translate("MainWindow", "note", 0));
        tabWidget_tables->setTabText(tabWidget_tables->indexOf(tab_RP_table), QApplication::translate("MainWindow", "\320\223\320\224", 0));
        pushButton_clear_table->setText(QApplication::translate("MainWindow", "clear", 0));
        pushButton_delta->setText(QApplication::translate("MainWindow", "delta", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_data), QApplication::translate("MainWindow", "\320\270\321\201\321\202\320\276\321\200\320\270\321\217", 0));
        QTableWidgetItem *___qtablewidgetitem15 = tableWidget_shoring->horizontalHeaderItem(0);
        ___qtablewidgetitem15->setText(QApplication::translate("MainWindow", "n", 0));
        QTableWidgetItem *___qtablewidgetitem16 = tableWidget_shoring->horizontalHeaderItem(1);
        ___qtablewidgetitem16->setText(QApplication::translate("MainWindow", "\320\272\321\200\320\265\320\277\321\214", 0));
        QTableWidgetItem *___qtablewidgetitem17 = tableWidget_shoring->horizontalHeaderItem(2);
        ___qtablewidgetitem17->setText(QApplication::translate("MainWindow", "\321\210\320\270\321\200\320\270\320\275\320\260 [\320\274]", 0));
        QTableWidgetItem *___qtablewidgetitem18 = tableWidget_shoring->horizontalHeaderItem(3);
        ___qtablewidgetitem18->setText(QApplication::translate("MainWindow", "\320\275\320\265\321\201\321\203\321\211\320\260\321\217 \321\201\320\277\320\276\321\201\320\276\320\261\320\275\320\276\321\201\321\202\321\214 [kH]", 0));
        QTableWidgetItem *___qtablewidgetitem19 = tableWidget_shoring->horizontalHeaderItem(4);
        ___qtablewidgetitem19->setText(QApplication::translate("MainWindow", "\320\277\320\273\320\276\321\202\320\275\320\276\321\201\321\202\321\214 [1/\320\274]", 0));
        QTableWidgetItem *___qtablewidgetitem20 = tableWidget_shoring->horizontalHeaderItem(5);
        ___qtablewidgetitem20->setText(QApplication::translate("MainWindow", "\321\210\320\260\320\263 [\320\274]", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_shoring), QApplication::translate("MainWindow", "\320\272\321\200\320\265\320\277\320\270", 0));
        label_param->setText(QApplication::translate("MainWindow", "\320\237\320\260\321\200\320\260\320\274\320\265\321\202\321\200\321\213: ", 0));
        label_param_file->setText(QApplication::translate("MainWindow", "default", 0));
        groupBox_5->setTitle(QApplication::translate("MainWindow", "\321\201\321\207\320\270\321\202\320\260\321\202\321\214", 0));
        button_calc_ep_cem->setText(QApplication::translate("MainWindow", "\321\206\320\265\320\274\320\265\320\275\321\202\320\260\321\206\320\270\321\217", 0));
#ifndef QT_NO_TOOLTIP
        button_calc_rp->setToolTip(QApplication::translate("MainWindow", "\320\263\320\276\321\200\320\275\320\276\320\265 \320\264\320\260\320\262\320\273\320\265\320\275\320\270\320\265 \320\264\320\273\321\217 \320\264\320\270\320\260\320\277\320\260\320\267\320\276\320\275\320\260 \320\267\320\275\320\260\321\207\320\265\320\275\320\270\320\271 *", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_ACCESSIBILITY
        button_calc_rp->setAccessibleName(QApplication::translate("MainWindow", "button_calculate", 0));
#endif // QT_NO_ACCESSIBILITY
        button_calc_rp->setText(QApplication::translate("MainWindow", "\320\223\320\224", 0));
#ifndef QT_NO_TOOLTIP
        button_calc_ep->setToolTip(QApplication::translate("MainWindow", "<html><head/><body><p>\320\263\320\276\321\200\320\275\320\276\320\265 \320\264\320\260\320\273\320\265\320\275\320\270\320\265 \320\264\320\273\321\217 \320\267\320\260\320\264\320\260\320\275\320\275\320\276\320\263\320\276 r*</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        button_calc_ep->setText(QApplication::translate("MainWindow", "\320\223\320\224*", 0));
#ifndef QT_NO_ACCESSIBILITY
        button_calc_e->setAccessibleName(QApplication::translate("MainWindow", "button_calculate", 0));
#endif // QT_NO_ACCESSIBILITY
        button_calc_e->setText(QApplication::translate("MainWindow", "\321\203\320\277\321\200\321\203\320\263\321\203\321\216 \320\267\320\276\320\275\321\203/\320\267\320\260\320\264\320\260\321\207\321\203", 0));
        menu_params->setTitle(QApplication::translate("MainWindow", "\320\237\320\260\321\200\320\260\320\274\320\265\321\202\321\200\321\213", 0));
        menu_2->setTitle(QApplication::translate("MainWindow", "\320\236\321\202\321\207\321\221\321\202", 0));
        menu->setTitle(QApplication::translate("MainWindow", "C\320\277\321\200\320\260\320\262\320\272\320\260", 0));
        menu_shorings->setTitle(QApplication::translate("MainWindow", "\320\232\321\200\320\265\320\277\320\270", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
