#!/bin/sh
DoExitAsm ()
{ echo "An error occurred while assembling $1"; exit 1; }
DoExitLink ()
{ echo "An error occurred while linking $1"; exit 1; }
echo Linking libfflib.so
OFS=$IFS
IFS="
"
/usr/bin/ld.bfd -b elf64-x86-64 -m elf_x86_64  -init FPC_SHARED_LIB_START -fini FPC_LIB_EXIT -soname libfflib.so -shared -L. -o libfflib.so link.res
if [ $? != 0 ]; then DoExitLink libfflib.so; fi
IFS=$OFS
echo Linking libfflib.so
OFS=$IFS
IFS="
"
/usr/bin/strip --discard-all --strip-debug libfflib.so
if [ $? != 0 ]; then DoExitLink libfflib.so; fi
IFS=$OFS
