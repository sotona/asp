library fflib;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
   pasp13i;

var
  ssrl_g,sssl_g,ko_g,kp_g,q_g,nt_g,k0_g,ff0_g,k1_g,ff1_g:double;

procedure ff_calc;
var f: TextFile;
begin
{AssignFile(F, 'log.txt');
Rewrite(f);
Writeln(f,ssrl_g:5:2);
Writeln(f,sssl_g:5:2);
Writeln(f,ko_g:5:2);
Writeln(f,kp_g:5:2);
Writeln(f,q_g:5:2);
Writeln(f,nt_g:5:2);}

ssrl_:=ssrl_g;
sssl_:=sssl_g;
ko_:=ko_g;
kp_:=kp_g;
q_:=q_g;
nt_:=nt_g;
sets;
k0_g:=K__;
ff0_g:=fi__;
k1_g:=K_;
ff1_g:=fi_;
{Writeln(f,k0_g:5:2);
Writeln(f,ff0_g:5:2);
Writeln(f,k1_g:5:2);
Writeln(f,ff1_g:5:2);
CloseFile(F);}
end;

begin
end.

exports
ff_calc,ssrl_g,sssl_g,ko_g,kp_g,q_g,nt_g,k0_g,ff0_g,k1_g,ff1_g;
